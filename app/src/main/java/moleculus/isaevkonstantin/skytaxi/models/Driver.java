package moleculus.isaevkonstantin.skytaxi.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Isaev Konstantin on 28.05.16.
 */
public class Driver {

    private transient long id;

    @SerializedName("id")
    private String serverId;
    @SerializedName("name")
    private String name;
    @SerializedName("phone")
    private String phone;
    @SerializedName("car")
    private String car;
    @SerializedName("driver_license")
    private String driverLicense;
    @SerializedName("number")
    private String number;
    @SerializedName("car_license")
    private String carLicense;
    @SerializedName("driver_raiting")
    private String driverRaiting;

    @SerializedName("point")
    private Point point;
    private transient String pointJson;
    @SerializedName("service")
    private TaxiService taxiService;
    private String taxiServiceJson;
    @SerializedName("car_details")
    private CarDetails carDetails;
    private String carDetailsJson;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

    public String getPointJson() {
        return pointJson;
    }

    public void setPointJson(String pointJson) {
        this.pointJson = pointJson;
    }

    public TaxiService getTaxiService() {
        return taxiService;
    }

    public void setTaxiService(TaxiService taxiService) {
        this.taxiService = taxiService;
    }

    public String getTaxiServiceJson() {
        return taxiServiceJson;
    }

    public void setTaxiServiceJson(String taxiServiceJson) {
        this.taxiServiceJson = taxiServiceJson;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public String getDriverLicense() {
        return driverLicense;
    }

    public void setDriverLicense(String driverLicense) {
        this.driverLicense = driverLicense;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCarLicense() {
        return carLicense;
    }

    public void setCarLicense(String carLicense) {
        this.carLicense = carLicense;
    }

    public String getDriverRaiting() {
        return driverRaiting;
    }

    public void setDriverRaiting(String driverRaiting) {
        this.driverRaiting = driverRaiting;
    }

    public CarDetails getCarDetails() {
        return carDetails;
    }

    public void setCarDetails(CarDetails carDetails) {
        this.carDetails = carDetails;
    }

    public String getCarDetailsJson() {
        return carDetailsJson;
    }

    public void setCarDetailsJson(String carDetailsJson) {
        this.carDetailsJson = carDetailsJson;
    }

    public class Point{
       @SerializedName("time")
       private String time;
       @SerializedName("latitude")
       private double latitude;
       @SerializedName("longitude")
       private double longitude;

       public String getTime() {
           return time;
       }

       public void setTime(String time) {
           this.time = time;
       }

       public double getLatitude() {
           return latitude;
       }

       public void setLatitude(double latitude) {
           this.latitude = latitude;
       }

       public double getLongitude() {
           return longitude;
       }

       public void setLongitude(double longitude) {
           this.longitude = longitude;
       }
   }

    public class TaxiService{
        @SerializedName("name")
        private String name;
        @SerializedName("phone")
        private String phone;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }

    public class CarDetails{
        private String brand;
        private String model;
        private String number;
        private String license;
        private String color;
        private String age;

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getLicense() {
            return license;
        }

        public void setLicense(String license) {
            this.license = license;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }
    }
}
