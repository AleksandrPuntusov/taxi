package moleculus.isaevkonstantin.skytaxi.models;

import com.google.gson.annotations.SerializedName;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import moleculus.isaevkonstantin.skytaxi.database.tables.OrderOptionsTable;

/**
 * Created by Isaev Konstantin on 26.05.16.
 */
@StorIOSQLiteType(table = OrderOptionsTable.ORDER_OPTIONS_TABLE)
public class OrderOptions {
    public static final int COMFORT_CAR = 1;
    public static final int BUSSINESS_CAR = 2;
    public static final int MINIVAN_CAR = 3;
    public static final int UNIVERSAL_CAR = 4;

    @StorIOSQLiteColumn(name = OrderOptionsTable.ORDER_OPTIONS_ID, key = true)
    private transient long id;

    @StorIOSQLiteColumn(name = OrderOptionsTable.ORDER_BAG_COUNTER)
    @SerializedName("luggage_count")
    private int bagCount;
    @StorIOSQLiteColumn(name = OrderOptionsTable.ORDER_CAR_CLASS)
    @SerializedName("car_class")
    private int carClass;
    @StorIOSQLiteColumn(name = OrderOptionsTable.ORDER_COMMENT)
    @SerializedName("comments")
    private String comment;
    @StorIOSQLiteColumn(name = OrderOptionsTable.ORDER_NO_SMOKING_DRIVER)
    @SerializedName("no_smoking")
    private boolean noSmokingDriver;
    @StorIOSQLiteColumn(name = OrderOptionsTable.ORDER_AIR_BUS_NUMBER)
    @SerializedName("flight_number")
    private String  flightNumber;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getBagCount() {
        return bagCount;
    }

    public void setBagCount(int bagCount) {
        this.bagCount = bagCount;
    }

    public int getCarClass() {
        return carClass;
    }

    public void setCarClass(int carClass) {
        this.carClass = carClass;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isNoSmokingDriver() {
        return noSmokingDriver;
    }

    public void setNoSmokingDriver(boolean noSmokingDriver) {
        this.noSmokingDriver = noSmokingDriver;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public static OrderOptions generateDefaultOption(){
        OrderOptions orderOptions = new OrderOptions();
        orderOptions.bagCount = 2;
        orderOptions.comment = "";
        orderOptions.flightNumber = "";
        orderOptions.carClass = COMFORT_CAR;
        orderOptions.noSmokingDriver = false;
        return orderOptions;
    }
}
