package moleculus.isaevkonstantin.skytaxi.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Isaev Konstantin on 07.05.16.
 */
public class Order implements Parcelable {

    public static final int NOT_SPECIFIED_STATUS = 0;
    public static final int DRIVER_SEARCH_STATUS = 1;
    public static final int DRIWER_ON_RIDE_STATUS = 2;
    public static final int DRIWER_ON_PLACE_STATUS = 3;
    public static final int ORDER_BEGIN_STATUS = 4;
    public static final int ORDER_SUCCESS_NOT_PAY_STATUS = 5;
    public static final int ORDER_CLOSE_STATUS = 6;
    public static final int ORDER_CANSEL_STATUS = 7;
    public static final int ERROR = 8;
    public static final int CANT_FIND_DRIVER = 9;
    public static final int ASSIGN_DRIVER = 10;

    public static final int NOT_SPECIFIED_TIME = 0;
    public static final int START_SHORT_TIME = 1;
    public static final int START_THIRTY_MINUTES = 2;
    public static final int START_FORTY_FIVE_MINUTES = 3;
    public static final int START_HOUR = 4;

    public static final int PAY_NOT_SPECIFIED = 0;
    public static final int PAY_BY_CASH = 1;
    public static final int PAY_BY_CARD = 2;


    private transient long id;
    private transient long locationFromId;
    private transient long locationToId;
    private transient long orderOptionFk;
    private transient long driverFk;
    private transient boolean currentOrder;
    private transient int timeId;
    private long checkedTripId;
    private int paymentId;


    @SerializedName("index")
    private String serverId;
    @SerializedName("from")
    private Location locationFrom;
    @SerializedName("to")
    private Location locationTo;
    @SerializedName("rate")
    private int rate;
    @SerializedName("status")
    private int status;
    @SerializedName("price")
    private int price;
    @SerializedName("duration")
    private int duration;
    @SerializedName("options")
    private OrderOptions orderOptions;
    private transient List<Trip> trips;
    private transient Driver driver;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getLocationFromId() {
        return locationFromId;
    }

    public void setLocationFromId(long locationFromId) {
        this.locationFromId = locationFromId;
    }

    public long getLocationToId() {
        return locationToId;
    }

    public void setLocationToId(long locationToId) {
        this.locationToId = locationToId;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public Location getLocationFrom() {
        return locationFrom;
    }

    public void setLocationFrom(Location locationFrom) {
        this.locationFrom = locationFrom;
    }

    public Location getLocationTo() {
        return locationTo;
    }

    public void setLocationTo(Location locationTo) {
        this.locationTo = locationTo;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public boolean isCurrentOrder() {
        return currentOrder;
    }

    public void setCurrentOrder(boolean currentOrder) {
        this.currentOrder = currentOrder;
    }

    public int getTimeId() {
        return timeId;
    }

    public void setTimeId(int timeId) {
        this.timeId = timeId;
    }

    public OrderOptions getOrderOptions() {
        return orderOptions;
    }

    public void setOrderOptions(OrderOptions orderOptions) {
        this.orderOptions = orderOptions;
    }

    public long getOrderOptionFk() {
        return orderOptionFk;
    }

    public void setOrderOptionFk(long orderOptionFk) {
        this.orderOptionFk = orderOptionFk;
    }

    public List<Trip> getTrips() {
        return trips;
    }

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }

    public long getCheckedTripId() {
        return checkedTripId;
    }

    public void setCheckedTripId(long checkedTripId) {
        this.checkedTripId = checkedTripId;
    }

    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public long getDriverFk() {
        return driverFk;
    }

    public void setDriverFk(long driverFk) {
        this.driverFk = driverFk;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeLong(orderOptionFk);
        dest.writeLong(locationFromId);
        dest.writeLong(checkedTripId);
        dest.writeLong(locationToId);
        dest.writeByte((byte) (currentOrder ? 1 : 0));
        dest.writeString(serverId);
        dest.writeParcelable(locationFrom, flags);
        dest.writeParcelable(locationTo, flags);
        dest.writeInt(rate);
        dest.writeInt(status);
        dest.writeInt(price);
        dest.writeInt(duration);
        dest.writeInt(timeId);
        dest.writeInt(paymentId);
        dest.writeByte((byte) (currentOrder ? 1 : 0));
    }

    public static final Parcelable.Creator<Order> CREATOR = new Parcelable.Creator<Order>() {
        @Override
        public Order createFromParcel(Parcel source) {
            Order order = new Order();
            order.id = source.readLong();
            order.orderOptionFk = source.readLong();
            order.locationFromId = source.readLong();
            order.locationToId = source.readLong();
            order.checkedTripId = source.readLong();
            order.currentOrder = source.readByte()>0;
            order.locationFrom = source.readParcelable(Location.class.getClassLoader());
            order.locationTo = source.readParcelable(Location.class.getClassLoader());
            order.rate = source.readInt();
            order.status = source.readInt();
            order.price = source.readInt();
            order.duration = source.readInt();
            order.timeId = source.readInt();
            order.paymentId = source.readInt();
            return order;
        }

        @Override
        public Order[] newArray(int size) {
            return new Order[0];
        }
    };
}
