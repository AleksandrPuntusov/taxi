package moleculus.isaevkonstantin.skytaxi.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Isaev Konstantin on 22.05.16.
 */
public class PromoCode implements Parcelable {

    private transient long id;
    @SerializedName("id")
    private String serverId;
    @SerializedName("code")
    private String code;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updatedAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(serverId);
        dest.writeString(code);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
    }

    public static final Parcelable.Creator<PromoCode> CREATOR = new Parcelable.Creator<PromoCode>() {
        @Override
        public PromoCode createFromParcel(Parcel source) {
            PromoCode promoCode = new PromoCode();
            promoCode.id = source.readLong();
            promoCode.serverId = source.readString();
            promoCode.code = source.readString();
            promoCode.createdAt = source.readString();
            promoCode.updatedAt = source.readString();
            return promoCode;

        }

        @Override
        public PromoCode[] newArray(int size) {
            return new PromoCode[0];
        }
    };

}
