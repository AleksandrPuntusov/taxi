package moleculus.isaevkonstantin.skytaxi.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Isaev Konstantin on 12.04.16.
 */
public class User implements Parcelable {

    private transient long id;
    @SerializedName("id")
    private String serverId;
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("last_name")
    private String lastName;
    @SerializedName("middle_name")
    private String middleName;
    @SerializedName("email")
    private String email;
    @SerializedName("phone")
    private String phoneNumber;
    @SerializedName("photo_url")
    private String photoUrl;
    @SerializedName("bonuses")
    private int bonuses;
    @SerializedName("rides_count")
    private int ridesCount;
    @SerializedName("promo_code")
    private PromoCode promoCode;
    @SerializedName("statistics")
    private UserStatistics userStatistics;

    private String authToken;
    private String pathToPhoto;

    private transient long promoCodeFk;
    private transient long userStatisticsFk;
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public int getBonuses() {
        return bonuses;
    }

    public void setBonuses(int bonuses) {
        this.bonuses = bonuses;
    }

    public int getRidesCount() {
        return ridesCount;
    }

    public void setRidesCount(int ridesCount) {
        this.ridesCount = ridesCount;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getPathToPhoto() {
        return pathToPhoto;
    }

    public void setPathToPhoto(String pathToPhoto) {
        this.pathToPhoto = pathToPhoto;
    }

    public PromoCode getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(PromoCode promoCode) {
        this.promoCode = promoCode;
    }

    public UserStatistics getUserStatistics() {
        return userStatistics;
    }

    public void setUserStatistics(UserStatistics userStatistics) {
        this.userStatistics = userStatistics;
    }

    public long getPromoCodeFk() {
        return promoCodeFk;
    }

    public void setPromoCodeFk(long promoCodeFk) {
        this.promoCodeFk = promoCodeFk;
    }

    public long getUserStatisticsFk() {
        return userStatisticsFk;
    }

    public void setUserStatisticsFk(long userStatisticsFk) {
        this.userStatisticsFk = userStatisticsFk;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(serverId);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(middleName);
        dest.writeString(email);
        dest.writeString(phoneNumber);
        dest.writeString(photoUrl);
        dest.writeParcelable(promoCode,flags);
        dest.writeInt(bonuses);
        dest.writeInt(ridesCount);
        dest.writeString(authToken);
        dest.writeString(pathToPhoto);
        dest.writeLong(userStatisticsFk);
        dest.writeLong(promoCodeFk);

    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            User user = new User();
            user.id = source.readLong();
            user.serverId = source.readString();
            user.firstName = source.readString();
            user.lastName = source.readString();
            user.middleName = source.readString();
            user.email = source.readString();
            user.phoneNumber = source.readString();
            user.photoUrl = source.readString();
            user.promoCode = source.readParcelable(PromoCode.class.getClassLoader());
            user.bonuses = source.readInt();
            user.ridesCount = source.readInt();
            user.authToken = source.readString();
            user.pathToPhoto = source.readString();
            user.userStatisticsFk = source.readLong();
            user.promoCodeFk = source.readLong();
            return user;
        }

        @Override
        public User[] newArray(int size) {
            return new User[0];
        }
    };


}

