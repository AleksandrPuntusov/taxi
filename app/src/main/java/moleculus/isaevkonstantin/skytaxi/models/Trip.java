package moleculus.isaevkonstantin.skytaxi.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Isaev Konstantin on 25.05.16.
 */
public class Trip {

    private transient long id;
    private transient long locationFromId;
    private transient long locationToId;
    private transient long orderId;

    @SerializedName("name")
    private String name;
    @SerializedName("about")
    private String about;
    @SerializedName("distance")
    private int distance;
    @SerializedName("cost")
    private int cost;
    @SerializedName("from")
    private Location locationFrom;
    @SerializedName("to")
    private Location locationTo;
    @SerializedName("id")
    private String serverId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getLocationFromId() {
        return locationFromId;
    }

    public void setLocationFromId(long locationFromId) {
        this.locationFromId = locationFromId;
    }

    public long getLocationToId() {
        return locationToId;
    }

    public void setLocationToId(long locationToId) {
        this.locationToId = locationToId;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public Location getLocationFrom() {
        return locationFrom;
    }

    public void setLocationFrom(Location locationFrom) {
        this.locationFrom = locationFrom;
    }

    public Location getLocationTo() {
        return locationTo;
    }

    public void setLocationTo(Location locationTo) {
        this.locationTo = locationTo;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }
}
