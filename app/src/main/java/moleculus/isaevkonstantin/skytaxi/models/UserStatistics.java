package moleculus.isaevkonstantin.skytaxi.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Isaev Konstantin on 22.05.16.
 */
public class UserStatistics implements Parcelable {
    private transient long id;
    @SerializedName("distance_total")
    private int distanceTotal;
    @SerializedName("distance_current_month")
    private int distanceCurrentMonth;
    @SerializedName("rides_total")
    private int ridesTotal;
    @SerializedName("rides_current_month")
    private int ridesCurrentMonth;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getDistanceTotal() {
        return distanceTotal;
    }

    public void setDistanceTotal(int distanceTotal) {
        this.distanceTotal = distanceTotal;
    }

    public int getDistanceCurrentMonth() {
        return distanceCurrentMonth;
    }

    public void setDistanceCurrentMonth(int distanceCurrentMonth) {
        this.distanceCurrentMonth = distanceCurrentMonth;
    }

    public int getRidesTotal() {
        return ridesTotal;
    }

    public void setRidesTotal(int ridesTotal) {
        this.ridesTotal = ridesTotal;
    }

    public int getRidesCurrentMonth() {
        return ridesCurrentMonth;
    }

    public void setRidesCurrentMonth(int ridesCurrentMonth) {
        this.ridesCurrentMonth = ridesCurrentMonth;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeInt(distanceTotal);
        dest.writeInt(distanceCurrentMonth);
        dest.writeInt(ridesTotal);
        dest.writeInt(ridesCurrentMonth);
    }
    public static final Parcelable.Creator<UserStatistics> CREATOR = new Parcelable.Creator<UserStatistics>() {
        @Override
        public UserStatistics createFromParcel(Parcel source) {
            UserStatistics userStatistics = new UserStatistics();
            userStatistics.id  = source.readLong();
            userStatistics.distanceTotal  = source.readInt();
            userStatistics.distanceCurrentMonth  = source.readInt();
            userStatistics.ridesTotal  = source.readInt();
            userStatistics.ridesCurrentMonth = source.readInt();
            return userStatistics;
        }

        @Override
        public UserStatistics[] newArray(int size) {
            return new UserStatistics[0];
        }
    };
}
