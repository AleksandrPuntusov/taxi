package moleculus.isaevkonstantin.skytaxi.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Isaev Konstantin on 11.05.16.
 */
public class Location implements Parcelable {
    public static final int FROM = 1;
    public static final int TO = 2;
    public static final int RAILS_STATION = 1;
    public static final int AIR_PORTS = 2;

    private transient long id;
    @SerializedName("index")
    private String serverId;
    @SerializedName("name")
    private String name;
    @SerializedName("entrance")
    private String entrance;
    @SerializedName("latitude")
    private double latitude;
    @SerializedName("longitude")
    private double longitude;

    private transient boolean favorite;
    private transient boolean station;
    private transient int pathId;
    private transient int stationId;
    private transient boolean fromTrip;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEntrance() {
        return entrance;
    }

    public void setEntrance(String entrance) {
        this.entrance = entrance;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public int getPathId() {
        return pathId;
    }

    public void setPathId(int pathId) {
        this.pathId = pathId;
    }

    public boolean isStation() {
        return station;
    }

    public void setStation(boolean station) {
        this.station = station;
    }

    public int getStationId() {
        return stationId;
    }

    public void setStationId(int stationId) {
        this.stationId = stationId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(serverId);
        dest.writeString(name);
        dest.writeString(entrance);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeInt(pathId);
        dest.writeInt(stationId);
        dest.writeByte((byte)(favorite?1:0));
        dest.writeByte((byte)(station?1:0));
        dest.writeByte((byte)(fromTrip?1:0));
    }

    public static Location createLocation(String name,double latitude,double longitude) {
        Location location = new Location();
        location.name = (name==null?"":name);
        location.latitude = latitude;
        location.longitude = longitude;
        location.serverId = "";
        return location;
    }
    public static final Parcelable.Creator<Location> CREATOR = new Parcelable.Creator<Location>(){
        @Override
        public Location createFromParcel(Parcel source) {
            Location location = new Location();
            location.id = source.readLong();
            location.serverId = source.readString();
            location.name = source.readString();
            location.entrance = source.readString();
            location.latitude = source.readDouble();
            location.longitude = source.readDouble();
            location.favorite = source.readByte()!=0;
            location.station = source.readByte()!=0;
            location.fromTrip = source.readByte()!=0;
            location.pathId = source.readInt();
            location.stationId = source.readInt();
            return location;
        }

        @Override
        public Location[] newArray(int size) {
            return new Location[0];
        }
    };

    public static Location toLocation(TripLocation tripLocation){
        Location location = null;
        if(tripLocation!=null){
            location = new Location();
            location.setName(tripLocation.getName());
            location.setLatitude(tripLocation.getLatitude());
            location.setLongitude(tripLocation.getLongitude());
        }
        return location;
    }

    public static Location toLocation(SearchedLocation searchedLocation){
        Location location = null;
        if(searchedLocation!=null){
            location = new Location();
            location.setName(searchedLocation.getAddress());
            location.setServerId(searchedLocation.getId());
            location.setLatitude(searchedLocation.getLatitude());
            location.setLongitude(searchedLocation.getLongitude());
        }
        return location;
    }
}
