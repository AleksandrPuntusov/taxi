package moleculus.isaevkonstantin.skytaxi.rest.interfaces;


import com.google.gson.JsonObject;

import java.io.File;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by Isaev Konstantin on 19.04.16.
 */
public interface UserRestService {
    String FIRST_NAME = "first_name";
    String LAST_NAME = "last_name";
    String MIDDLE_NAME = "middle_name";
    String EMAIL = "email";
    @FormUrlEncoded
    @POST("user/phone")
    Observable<Response<JsonObject>> sendPhone(@Field("phone") String phone);
    @FormUrlEncoded
    @POST("user/code")
    Observable<Response<JsonObject>> sendSmsCode(@Field("phone") String phone,
                                                 @Field("code") String code,
                                                 @Field("client_id") String clientId,
                                                 @Field("client_secret") String clientSecret);
    @GET("user/profile")
    Observable<Response<JsonObject>> getUser();
    @Multipart
    @POST("user/photo")
    Observable<Response<JsonObject>> updatePhoto(@Part("photo\"; filename=\"picture.jpg\" ") RequestBody requestBody);
//    "file\"; filename=\"pp.png\" "
    @FormUrlEncoded
    @POST("user/profile")
    Observable<Response<JsonObject>> updateUser(@Query("access_token") String accessToken,@FieldMap Map<String, String> options);
    @DELETE("user/logout")
    Observable<Response<JsonObject>> logout(@Query("access_token") String accessToken);


}
