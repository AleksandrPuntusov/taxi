package moleculus.isaevkonstantin.skytaxi.rest.interfaces;

import com.google.gson.JsonObject;

import retrofit2.Response;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Isaev Konstantin on 09.05.16.
 */
public interface LocationRestService {
    String STREET_TYPE = "street_address";
    String LANGUAGE = "ru";
    String BOUNDS = "55.5321281,37.56866458|56.0547853,37.59887698";
    @GET("http://geo.nowtaxi.ru/seek/geostreet")
    Observable<Response<JsonObject>> searchStreet(@Query("text")String address,
                                                  @Query("lon")double longitude,
                                                  @Query("lat")double latitude,
                                                  @Query("limit")int limit);
    @GET("http://geo.nowtaxi.ru/seek/houses")
    Observable<Response<JsonObject>>  searchhousebyStreetId(@Query("id")String houseId);
    @GET("/user/locations")
    Observable<Response<JsonObject>> getFavoritesLocations(@Query("access_token")String accessToken);

    @DELETE("/user/locations")
    Observable<Response<JsonObject>> deleteLocationFromFavorites(@Query("access_token")String accessToken,
                                                                  @Query("index")String locationServerId);
    @GET("locations")
    Observable<Response<JsonObject>> getAirPorts();

    @POST("user/locations/add")
    Observable<Response<JsonObject>> addLocationToFavorites(@Query("access_token")String accessToken,
                                                            @Query("name")String name,
                                                            @Query("latitude")String latitude,
                                                            @Query("longitude")String longitude);



}
