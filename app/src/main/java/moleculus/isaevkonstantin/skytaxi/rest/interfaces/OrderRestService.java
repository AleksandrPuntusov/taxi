package moleculus.isaevkonstantin.skytaxi.rest.interfaces;

import com.google.gson.JsonObject;

import java.util.Date;

import moleculus.isaevkonstantin.skytaxi.models.Location;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Isaev Konstantin on 23.05.16.
 */
public interface OrderRestService {

    @GET("trips/calculate")
    Observable<Response<JsonObject>> getOrderCost(@Query("from[lat]")double fromLatitude ,
                                                  @Query("from[lon]")double fromLongitude,
                                                  @Query("from[address]")String  fromAddress,
                                                  @Query("from[porch]")String fromEntrance,
                                                  @Query("to[lat]")Double toLatitude,
                                                  @Query("to[lon]")Double toLongitude,
                                                  @Query("to[address]")String  toAddress,
                                                  @Query("to[porch]")String  toEntrance,
                                                  @Query("due_date")double dueDate,
                                                  @Query("baggage_count")int bagageCount,
                                                  @Query("car_class")int carClass,
                                                  @Query("child_seat")int childSeat,
                                                  @Query("no_smoking")int noSmoking,
                                                  @Query("flight_number")String  flighNumber,
                                                  @Query("comments")String comments,
                                                  @Query("booking_time") String  bookingTime
                                                  );
    @FormUrlEncoded
    @POST("orders/create")
    Observable<Response<JsonObject>> createOrder(@Field("trip_id")int tripId,@Field("payment_method")int payment);

    @POST("orders/{order_id}/cancel")
    Observable<Response<JsonObject>> cancelOrder(@Path("order_id") int  orderId);
}
