package moleculus.isaevkonstantin.skytaxi.rest;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Isaev Konstantin on 19.04.16.
 */
public class RestAdapter {
    private Retrofit retrofit;
    private static final String BASE_URL = "http://178.62.197.213//api/v1/";
//    private static final String BASE_URL = "http://178.62.62.187/api/v1/";

    public RestAdapter() {
    }

    public void init(final String authToken, final boolean login){
        OkHttpClient defaultHttpClient = new OkHttpClient.Builder()
                .addInterceptor(
                        new Interceptor() {
                            @Override
                            public Response intercept(Interceptor.Chain chain) throws IOException {
                                Request request;
                                if(login){
                                    request = chain.request().newBuilder()
                                            .addHeader("Content-Type", "application/json")
                                                    .build();
                                }else {
                                    request = chain.request().newBuilder()
                                            .addHeader("Content-Type", "application/json")
                                            .addHeader("Authorization", "Bearer "+authToken)
                                                    .build();
                                }
                                return chain.proceed(request);
                            }
                        }).build();
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(defaultHttpClient)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }


    public Retrofit getRetrofit() {
        return retrofit;
    }

}
