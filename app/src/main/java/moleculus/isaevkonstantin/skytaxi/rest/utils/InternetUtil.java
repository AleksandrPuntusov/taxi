package moleculus.isaevkonstantin.skytaxi.rest.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Isaev Konstantin on 20.04.16.
 */
public class InternetUtil {

    /**
     * Function check internet connection
     * @param context
     * @return true if connection available,else if not.
     */
    public static boolean checkInternetConnection(Context context){
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected() && networkInfo.isAvailable();
    }
}
