package moleculus.isaevkonstantin.skytaxi.rest.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Isaev Konstantin on 20.04.16.
 */
public class ResponseResult {
    @SerializedName("code")
    private int code;
    @SerializedName("status")
    private String status;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ResponseResult{" +
                "code=" + code +
                ", status='" + status + '\'' +
                '}';
    }
}
