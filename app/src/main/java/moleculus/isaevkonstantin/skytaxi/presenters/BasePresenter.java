package moleculus.isaevkonstantin.skytaxi.presenters;


import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpView;

/**
 * Created by Isaev Konstantin on 26.03.16.
 */
public abstract class BasePresenter {
    protected MvpView mvpView;

    // TODO: 26.03.16 Refactor this method.
    public abstract void init();
    public void destroy(){
        mvpView = null;
    };

    public MvpView getMvpView() {
        return mvpView;
    }

    public void setMvpView(MvpView mvpView) {
        this.mvpView = mvpView;
    }
}
