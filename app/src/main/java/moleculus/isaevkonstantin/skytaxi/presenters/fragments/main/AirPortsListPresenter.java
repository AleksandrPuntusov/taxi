package moleculus.isaevkonstantin.skytaxi.presenters.fragments.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.Loader;

import moleculus.isaevkonstantin.skytaxi.components.fragments.main.SearchAddressFragment;
import moleculus.isaevkonstantin.skytaxi.eventbus.BusProvider;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.FragmentEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.ListItemClickEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.UpdateUiEvent;
import moleculus.isaevkonstantin.skytaxi.interactors.LocationInteractor;
import moleculus.isaevkonstantin.skytaxi.interactors.OrderInteractor;
import moleculus.isaevkonstantin.skytaxi.models.Location;
import moleculus.isaevkonstantin.skytaxi.models.Order;
import moleculus.isaevkonstantin.skytaxi.presenters.BasePresenter;
import moleculus.isaevkonstantin.skytaxi.presenters.interfaces.PresenterEventListener;
import moleculus.isaevkonstantin.skytaxi.components.constants.ArgumentTitles;
import moleculus.isaevkonstantin.skytaxi.components.fragments.EnterDataTextFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.main.AirPortsListFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.trip.BlueListTimePaymentFragment;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Isaev Konstantin on 22.05.16.
 */
public class AirPortsListPresenter extends BasePresenter implements PresenterEventListener {
    private Subscriber subscriber;
    private int pathId;
    private LocationInteractor locationInteractor;
    private OrderInteractor orderInteractor;


    @Override
    public void init() {
        locationInteractor = new LocationInteractor(((Fragment)mvpView).getContext());
        orderInteractor = new OrderInteractor(((Fragment)mvpView).getContext());

    }

    public void setPathId(int pathId) {
        this.pathId = pathId;
    }

    @Override
    public void registerSubscriber() {
        subscriber = new Subscriber() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Object o) {
                if(o instanceof UpdateUiEvent){
                    UpdateUiEvent updateUiEvent = (UpdateUiEvent)o;
                    if(updateUiEvent.getId()==UpdateUiEvent.GET_AIR_PORTS_EVENT){
                        listChanged(updateUiEvent);
                    }
                }else if(o instanceof ListItemClickEvent){
                    ListItemClickEvent listItemClickEvent = (ListItemClickEvent)o;
                    if(listItemClickEvent.getId()==ListItemClickEvent.LOCATION_LIST_CLICK){
                        enterEntrance((Location) listItemClickEvent.getData());
                    }
                }
            }
        };
        BusProvider.observe().observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);

    }

    @Override
    public void unRegisterSubscriber() {
        if(subscriber!=null&&!subscriber.isUnsubscribed()){
            subscriber.unsubscribe();
        }
    }

    private void listChanged(UpdateUiEvent updateUiEvent){
        if(updateUiEvent.isSuccess()){
            Loader<Object> loader = ((Fragment)mvpView).getLoaderManager().getLoader(AirPortsListFragment.LOCATION_LOADER_ID);
            loader.onContentChanged();
        }else {

        }
    }

    public void enterEntrance(Location location){
//        ((AirPortsListFragment)mvpView).openKeyBoard(false);
        int enterEntranceId = 0;
        String  locationSource = null;
        int fragmentId = -1;
        Bundle bundle = new Bundle();
        Bundle args = ((Fragment)mvpView).getArguments();
        boolean forwardFromOrderDetails = false;
        if(args!=null){
            forwardFromOrderDetails = args.getBoolean(ArgumentTitles.FORWARD_FROM_ORDER_DETAILS);
        }
        if(forwardFromOrderDetails){
            fragmentId = FragmentEvent.TRIP_DETAIL_FRAGMENT;
            if(pathId==AirPortsListFragment.PATH_FROM){
                location.setPathId(Location.FROM);
            }else if(pathId==AirPortsListFragment.PATH_TO){
                location.setPathId(Location.TO);
            }
            location.setEntrance("");
            updateLocationInDatabase(location);
        }else {
            if(pathId==AirPortsListFragment.PATH_FROM){
                enterEntranceId = EnterDataTextFragment.ENTER_ENTRANCE_FROM_FRAGMENT;
                locationSource = ArgumentTitles.LOCATION_FROM;
                fragmentId = FragmentEvent.SEARCH_ADDRESS_FRAGMENT;
                updateLocationInDatabase(location);
                bundle.putInt(ArgumentTitles.SEARCH_ADDRESS_ID_PARAM, SearchAddressFragment.PATH_TO);
            }else if(pathId == AirPortsListFragment.PATH_TO) {
//            enterEntranceId = EnterDataTextFragment.ENTER_ENTRANCE_TO_FRAGMENT;
                locationSource = ArgumentTitles.LOCATION_TO;
                fragmentId = FragmentEvent.TIME_PAYMENT_LIST_FRAGMENT;
                location.setPathId(Location.TO);
                location.setEntrance("");
                updateLocationInDatabase(location);
                bundle.putInt(BlueListTimePaymentFragment.FRAGMENT_LIST_ID_PARAM,BlueListTimePaymentFragment.TIME_LIST_FRAGMENT);
            }
        }
        bundle.putParcelable(locationSource,location);
        FragmentEvent fragmentEvent = FragmentEvent.newInstance(fragmentId,
                true,bundle);
        BusProvider.send(fragmentEvent);
    }

    private void updateLocationInDatabase(Location location){
        locationInteractor.saveLocationDb(location);
        Order order = orderInteractor.getCurrentOrderFromDb();
        if(order==null){
            order = new Order();
            order.setCurrentOrder(true);
        }
        switch (pathId){
            case AirPortsListFragment.PATH_FROM:
                order.setLocationFrom(location);
                order.setLocationFromId(location.getId());
                break;
            case AirPortsListFragment.PATH_TO:
                order.setLocationTo(location);
                order.setLocationToId(location.getId());
                break;
        }

        orderInteractor.updateOrderInDb(order);
    }

}
