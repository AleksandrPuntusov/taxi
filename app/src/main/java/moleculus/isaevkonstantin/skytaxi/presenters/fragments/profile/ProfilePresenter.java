package moleculus.isaevkonstantin.skytaxi.presenters.fragments.profile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.Loader;

import moleculus.isaevkonstantin.skytaxi.eventbus.BusProvider;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.constants.ApiConstants;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.network.NetworkRequestEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.FragmentEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.UpdateUiEvent;
import moleculus.isaevkonstantin.skytaxi.interactors.UserInteractor;
import moleculus.isaevkonstantin.skytaxi.models.User;
import moleculus.isaevkonstantin.skytaxi.presenters.BasePresenter;
import moleculus.isaevkonstantin.skytaxi.presenters.interfaces.PresenterEventListener;
import moleculus.isaevkonstantin.skytaxi.components.activities.MainActivity;
import moleculus.isaevkonstantin.skytaxi.components.constants.ArgumentTitles;
import moleculus.isaevkonstantin.skytaxi.components.fragments.profile.ProfileFragment;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpActionView;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Isaev Konstantin on 11.04.16.
 */
public class ProfilePresenter extends BasePresenter implements PresenterEventListener {
    private UserInteractor userInteractor;
    private Subscriber subscriber;

    @Override
    public void init() {
        userInteractor = new UserInteractor(((Fragment)mvpView).getActivity().getApplicationContext());
    }

    @Override
    public void destroy() {
        super.destroy();
        subscriber = null;
        userInteractor = null;
    }



    private void firstSettingsSuccess(boolean success){
        if(success) {
            Fragment fragment = (Fragment) mvpView;
            SharedPreferences sharedPref = fragment.getContext().getSharedPreferences("SkyTaxiPrefs", Context.MODE_PRIVATE);
            sharedPref.edit().putBoolean(ArgumentTitles.FIRST_SETTINGS_SUCCESS_SHAR_PREF,true).commit();
            Intent intent = new Intent(fragment.getContext(), MainActivity.class);
            BusProvider.send(intent);
        }else {
            ((MvpActionView)((ProfileFragment)mvpView).getActivity()).makeToast("Пользователь не обновлен");
        }

    }

    public void syncUser(User user){
        if (user != null) {
            NetworkRequestEvent<Bundle> networkRequestEvent = new NetworkRequestEvent();
            networkRequestEvent.setId(ApiConstants.GET_USER);
            Bundle bundle = new Bundle();
            bundle.putString(ArgumentTitles.AUTH_TOKEN,user.getAuthToken());
            networkRequestEvent.setData(bundle);
            BusProvider.send(networkRequestEvent);
        }
    }

    public void sendUser(String firstName, String lastName,
                         String middleName, String email, Uri pathToPhoto, String authToken){
        User user = new User();
        NetworkRequestEvent<Bundle> networkRequestEvent = new NetworkRequestEvent<>();
        networkRequestEvent.setId(ApiConstants.UPDATE_USER);
        Bundle bundle = new Bundle();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        user.setPathToPhoto(pathToPhoto!=null?getRealPathFromURI(((Fragment)mvpView).getContext(),
                pathToPhoto):null);
        user.setAuthToken(authToken);
        bundle.putParcelable(ProfileFragment.USER_TITLE_PARAM, user);
        networkRequestEvent.setData(bundle);
        BusProvider.send(networkRequestEvent);
//        getRealPathFromURI(((Fragment)mvpView).getContext(),
//                pathToPhoto);

    }

    public void setCreditCard(){
        FragmentEvent fragmentEvent = FragmentEvent.newInstance(FragmentEvent.SET_CARD_FRAGMENT,
                true,null);
        BusProvider.send(fragmentEvent);
    }

    public void updateUserProfile(User user){
        if(user!=null){
            Loader loader = ((ProfileFragment)mvpView).getLoaderManager().getLoader(ProfileFragment.USER_LOADER_ID);
            loader.onContentChanged();
        }
    }

    public void createUserLogo(){
        FragmentEvent fragmentEvent = FragmentEvent.newDialogInstance(FragmentEvent.LOAD_PHOTO_DIALOG,
                (Fragment) mvpView,null);
        BusProvider.send(fragmentEvent);
    }

    @Override
    public void registerSubscriber() {
        subscriber = new Subscriber() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Object o) {
                if(o instanceof UpdateUiEvent){
                    UpdateUiEvent updateUiEvent = (UpdateUiEvent)o;
                    if(updateUiEvent.getId()==UpdateUiEvent.SYNC_PROFILE_PRESENTER_EVENT){
                        updateUserProfile((User)updateUiEvent.getData());
                    }else if(updateUiEvent.getId()==UpdateUiEvent.UPDATE_PROFILE_EVENT){
                        firstSettingsSuccess(updateUiEvent.isSuccess());

                    }
                }
            }
        };
        BusProvider.observe().observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public void unRegisterSubscriber() {
        if(subscriber!=null&&!subscriber.isUnsubscribed()){
            subscriber.unsubscribe();
        }
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}
