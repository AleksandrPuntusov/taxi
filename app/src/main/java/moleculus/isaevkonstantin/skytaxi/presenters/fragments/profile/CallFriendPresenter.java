package moleculus.isaevkonstantin.skytaxi.presenters.fragments.profile;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.eventbus.BusProvider;
import moleculus.isaevkonstantin.skytaxi.presenters.BasePresenter;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpActionView;
import moleculus.isaevkonstantin.skytaxi.components.model.ActivityViewBuilder;

/**
 * Created by Isaev Konstantin on 12.04.16.
 */
public class CallFriendPresenter extends BasePresenter {
    @Override
    public void init() {

    }

    @Override
    public void destroy() {
        super.destroy();
    }

    public void updateActionBar(){
        ActivityViewBuilder activityViewBuilder = new ActivityViewBuilder();
        activityViewBuilder.setActionBarTitle("Пригласить друга");
        activityViewBuilder.setActionBarNavigation(ActivityViewBuilder.BACK_NAVIGATION);
        activityViewBuilder.setColor(R.color.white);
        AppCompatActivity activity = (AppCompatActivity) ((Fragment)mvpView).getActivity();
        ((MvpActionView) activity).updateUi(activityViewBuilder);
    }

    public void shareInSocial(){
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareString = "Зарегистрируйся в TaxiSkyMoscow с кодом "+"*КОД*"+ " и получили 100 рублей на первую поездку";
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareString);
        BusProvider.send(sharingIntent);
    }
}
