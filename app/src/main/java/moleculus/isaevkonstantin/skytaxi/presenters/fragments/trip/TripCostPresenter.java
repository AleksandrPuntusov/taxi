package moleculus.isaevkonstantin.skytaxi.presenters.fragments.trip;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import java.util.List;

import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.eventbus.BusProvider;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.FragmentEvent;
import moleculus.isaevkonstantin.skytaxi.interactors.OrderInteractor;
import moleculus.isaevkonstantin.skytaxi.models.Order;
import moleculus.isaevkonstantin.skytaxi.models.Trip;
import moleculus.isaevkonstantin.skytaxi.presenters.BasePresenter;
import moleculus.isaevkonstantin.skytaxi.components.fragments.trip.BlueListTimePaymentFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.trip.TripCostFragment;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpActionView;
import moleculus.isaevkonstantin.skytaxi.components.model.ActivityViewBuilder;

/**
 * Created by Isaev Konstantin on 09.05.16.
 */
public class TripCostPresenter extends BasePresenter {
    private OrderInteractor orderInteractor;
    @Override
    public void init() {
        orderInteractor = new OrderInteractor(((Fragment)mvpView).getContext());
    }



    public void setTariff(int tariffId){
        Order order = orderInteractor.getCurrentOrderFromDb();
        List<Trip> trips = order.getTrips();
        Trip optima = null;
        Trip promo = null;
        if(trips!=null&&!trips.isEmpty()){
            optima = trips.get(0);
        }

//        promo = trips.get(1);
        if(tariffId ==TripCostFragment.PROMO_TARIFF){
//            order.setCheckedTripId();
        }else {
            if(optima!=null){
                order.setCheckedTripId(optima.getId());
                orderInteractor.updateOrderInDb(order);
                Bundle bundle = new Bundle();
                bundle.putInt(BlueListTimePaymentFragment.FRAGMENT_LIST_ID_PARAM,BlueListTimePaymentFragment.PAYMENT_LIST_FRAGMENT);
                FragmentEvent fragmentEvent = FragmentEvent.newInstance(FragmentEvent.TIME_PAYMENT_LIST_FRAGMENT
                        ,true,bundle);
                BusProvider.send(fragmentEvent);
            }


        }

//



    }
}
