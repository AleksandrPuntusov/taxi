package moleculus.isaevkonstantin.skytaxi.presenters.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import moleculus.isaevkonstantin.skytaxi.eventbus.BusProvider;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.network.NetworkFailEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.network.NetworkRequestEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.FragmentEvent;
import moleculus.isaevkonstantin.skytaxi.navigation.navigators.MainNavigator;
import moleculus.isaevkonstantin.skytaxi.presenters.BasePresenter;
import moleculus.isaevkonstantin.skytaxi.presenters.interfaces.PresenterEventListener;
import moleculus.isaevkonstantin.skytaxi.rest.utils.InternetUtil;
import moleculus.isaevkonstantin.skytaxi.components.activities.RegistrationActivity;
import moleculus.isaevkonstantin.skytaxi.components.constants.ArgumentTitles;
import moleculus.isaevkonstantin.skytaxi.components.fragments.profile.ProfileFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.EnterDataTextFragment;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpActionView;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Isaev Konstantin on 11.04.16.
 */
public class RegistrationActivityPresenter extends BasePresenter implements PresenterEventListener {
    private MainNavigator mainNavigator;
    private Subscriber subscriber;

    @Override
    public void init() {
        mainNavigator = new MainNavigator();
        mainNavigator.setActivity((AppCompatActivity) mvpView);
        mainNavigator.init();
    }
    @Override
    public void destroy() {
        super.destroy();
        mainNavigator.destroy();
        mainNavigator = null;
        subscriber = null;
    }

    public void enterPhoneNumber() {
        Bundle bundle = new Bundle();
        bundle.putInt(ArgumentTitles.ENTER_DATA_ID,EnterDataTextFragment.ENTER_PHONE_FRAGMENT);
        FragmentEvent fragmentEvent = FragmentEvent.newInstance(FragmentEvent.ENTER_DATA_TEXT_FRAGMENT,
                true,bundle);
        mainNavigator.navigateToFragment(fragmentEvent);
    }

    public void enterProfile() {
        Bundle bundle = new Bundle();
        bundle.putInt(ProfileFragment.PROFILE_ID_TITLE_PARAM,ProfileFragment.FIRST_SETTINGS_PROFILE);
        FragmentEvent fragmentEvent = FragmentEvent.newInstance(FragmentEvent.PROFILE_FRAGMENT,
                true,bundle);
        mainNavigator.navigateToFragment(fragmentEvent);

    }


    @Override
    public void registerSubscriber() {
        subscriber = new Subscriber() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Object o) {
                if(o instanceof NetworkRequestEvent){
                    NetworkRequestEvent event = (NetworkRequestEvent)o;
                    if(InternetUtil.checkInternetConnection((AppCompatActivity) mvpView)){
                        ((RegistrationActivity)mvpView).loadFromApi(event);
                    }else {
                        ((MvpActionView)mvpView).makeToast("Нет интернет соединения");

                    }
                }else if(o instanceof FragmentEvent){
                    FragmentEvent fragmentEvent = (FragmentEvent)o;
                    mainNavigator.navigateToFragment(fragmentEvent);
                }else if(o instanceof Intent){
                    Intent intent = (Intent)o;
                    mainNavigator.navigateToActivity(intent);
                }else if(o instanceof NetworkFailEvent){
                    ((MvpActionView)mvpView).makeToast("Ошибка");
                }
            }
        };
        BusProvider.observe().observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public void unRegisterSubscriber() {
        if(subscriber!=null&&!subscriber.isUnsubscribed()){
            subscriber.unsubscribe();
        }
    }

}
