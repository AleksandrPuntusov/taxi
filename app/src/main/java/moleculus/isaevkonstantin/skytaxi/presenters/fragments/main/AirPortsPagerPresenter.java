package moleculus.isaevkonstantin.skytaxi.presenters.fragments.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import moleculus.isaevkonstantin.skytaxi.eventbus.BusProvider;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.constants.ApiConstants;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.network.NetworkRequestEvent;
import moleculus.isaevkonstantin.skytaxi.navigation.navigators.MainNavigator;
import moleculus.isaevkonstantin.skytaxi.presenters.BasePresenter;

/**
 * Created by Isaev Konstantin on 27.04.16.
 */
public class AirPortsPagerPresenter extends BasePresenter {
    private MainNavigator mainNavigator;
    @Override
    public void init() {
        mainNavigator = new MainNavigator();
        mainNavigator.setActivity((AppCompatActivity) ((Fragment) mvpView).getActivity());
        mainNavigator.init();
    }

    @Override
    public void destroy() {
        super.destroy();
        mainNavigator.init();
    }

    public void loadStations(){
        NetworkRequestEvent<Bundle> requestEvent = new NetworkRequestEvent();
        requestEvent.setId(ApiConstants.GET_AIR_PORTS);
        Bundle bundle = new Bundle();
        requestEvent.setData(bundle);
        BusProvider.send(requestEvent);
    }
}
