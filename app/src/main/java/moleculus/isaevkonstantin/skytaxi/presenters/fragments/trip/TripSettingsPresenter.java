package moleculus.isaevkonstantin.skytaxi.presenters.fragments.trip;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.eventbus.BusProvider;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.FragmentEvent;
import moleculus.isaevkonstantin.skytaxi.interactors.OrderInteractor;
import moleculus.isaevkonstantin.skytaxi.models.OrderOptions;
import moleculus.isaevkonstantin.skytaxi.presenters.BasePresenter;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpActionView;
import moleculus.isaevkonstantin.skytaxi.components.model.ActivityViewBuilder;

/**
 * Created by Isaev Konstantin on 07.05.16.
 */
public class TripSettingsPresenter  extends BasePresenter{
    private OrderInteractor orderInteractor;
    @Override
    public void init() {
        orderInteractor = new OrderInteractor(((Fragment)mvpView).getContext());

    }

    public void updateActionBar(){
        String title = ((Fragment) mvpView).getContext().getResources().getString(R.string.trip_settings_action_bar_title);
        ActivityViewBuilder activityViewBuilder = ActivityViewBuilder.createNavigation(title,ActivityViewBuilder.BACK_NAVIGATION);
        activityViewBuilder.setColor(R.color.white);
        ((MvpActionView) ((Fragment)mvpView).getActivity()).updateUi(activityViewBuilder);
    }

    public void addComment(){
        Bundle bundle = new Bundle();
        FragmentEvent fragmentEvent = FragmentEvent.newInstance(FragmentEvent.TRIP_COMMENT_FRAGMENT
                ,true,null);
        BusProvider.send(fragmentEvent);
    }

    public void updateOrderOptions(OrderOptions orderOptions){
            orderInteractor.updateOrderOptionInDb(orderOptions);
    }


}
