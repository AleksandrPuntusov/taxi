package moleculus.isaevkonstantin.skytaxi.presenters.interfaces;

/**
 * Created by Isaev Konstantin on 02.05.16.
 */
public interface PresenterEventListener {
    /**
     * Method register event lisener. for Presenter.
     * Called in onStart for Activity or Fragment
     * Called in ,OnCreate for Service
     */
    void registerSubscriber();

    /**
     * Method unregister event lisener. for Presenter.
     * Called in onStop for Activity or Fragment
     * Called in OnDestroy for Service
     */
    void unRegisterSubscriber();
}
