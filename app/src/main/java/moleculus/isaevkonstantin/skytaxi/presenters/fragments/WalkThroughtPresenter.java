package moleculus.isaevkonstantin.skytaxi.presenters.fragments;

import android.content.Intent;
import android.support.v4.app.Fragment;

import moleculus.isaevkonstantin.skytaxi.eventbus.BusProvider;
import moleculus.isaevkonstantin.skytaxi.presenters.BasePresenter;
import moleculus.isaevkonstantin.skytaxi.components.activities.RegistrationActivity;

/**
 * Created by Isaev Konstantin on 11.04.16.
 */
public class WalkThroughtPresenter extends BasePresenter {

    @Override
    public void init() {

    }

    @Override
    public void destroy() {
        super.destroy();
    }

    public void startRegistrationActivity(){
        Fragment fragment = (Fragment) mvpView;
        Intent intent = new Intent(fragment.getContext(), RegistrationActivity.class);
        BusProvider.send(intent);
    }
}
