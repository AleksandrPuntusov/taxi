package moleculus.isaevkonstantin.skytaxi.presenters.fragments.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import java.util.List;

import moleculus.isaevkonstantin.skytaxi.eventbus.BusProvider;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.constants.ApiConstants;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.network.NetworkRequestEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.FragmentEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.ListItemClickEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.UpdateUiEvent;
import moleculus.isaevkonstantin.skytaxi.interactors.LocationInteractor;
import moleculus.isaevkonstantin.skytaxi.interactors.OrderInteractor;
import moleculus.isaevkonstantin.skytaxi.models.Location;
import moleculus.isaevkonstantin.skytaxi.models.Order;
import moleculus.isaevkonstantin.skytaxi.presenters.BasePresenter;
import moleculus.isaevkonstantin.skytaxi.presenters.interfaces.PresenterEventListener;
import moleculus.isaevkonstantin.skytaxi.components.constants.ArgumentTitles;
import moleculus.isaevkonstantin.skytaxi.components.fragments.main.AirPortsListFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.main.MapFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.main.SearchAddressFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.EnterDataTextFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.trip.BlueListTimePaymentFragment;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Isaev Konstantin on 17.04.16.
 */
public class SearchAddressPresenter extends BasePresenter implements PresenterEventListener {
    private Subscriber subscriber;
    private int pathId;
    private LocationInteractor locationInteractor;
    private OrderInteractor orderInteractor;
    @Override
    public void init() {
        locationInteractor = new LocationInteractor(((Fragment)mvpView).getContext());
        orderInteractor = new OrderInteractor(((Fragment)mvpView).getContext());

    }

    @Override
    public void destroy() {
        super.destroy();
        subscriber = null;
        locationInteractor = null;
    }

    public void searchOnMap(){
        Bundle bundle = new Bundle();
        Bundle args = ((Fragment)mvpView).getArguments();
        boolean forwardFromOrderDetails = false;
        if(args!=null){
            forwardFromOrderDetails = args.getBoolean(ArgumentTitles.FORWARD_FROM_ORDER_DETAILS);
            if(forwardFromOrderDetails){
                bundle.putBoolean(ArgumentTitles.FORWARD_FROM_ORDER_DETAILS,forwardFromOrderDetails);
            }
        }
        int mapId = 0;
        if(pathId==SearchAddressFragment.PATH_FROM){
            mapId = MapFragment.PATH_FROM;
        }else if(pathId == SearchAddressFragment.PATH_TO) {
            mapId = MapFragment.PATH_TO;
        }
        bundle.putInt(ArgumentTitles.PATH_ID_TITLE_PARAM,mapId);
        FragmentEvent fragmentEvent = FragmentEvent.newInstance(FragmentEvent.MAP_FRAGMENT,
                true,bundle);
        BusProvider.send(fragmentEvent);
    }

    public void searchInFavorites(){
        Bundle bundle = new Bundle();
        bundle.putInt(ArgumentTitles.PATH_ID_TITLE_PARAM,pathId);
        Bundle args = ((Fragment)mvpView).getArguments();
        boolean forwardFromOrderDetails = false;
        if(args!=null){
            forwardFromOrderDetails = args.getBoolean(ArgumentTitles.FORWARD_FROM_ORDER_DETAILS);
            if(forwardFromOrderDetails){
                bundle.putBoolean(ArgumentTitles.FORWARD_FROM_ORDER_DETAILS,forwardFromOrderDetails);
            }
        }
        FragmentEvent fragmentEvent = FragmentEvent.newInstance(FragmentEvent.SEARCH_IN_FAVORITES_FRAGMENT,
                true,bundle);
        BusProvider.send(fragmentEvent);
    }

    public void searchInAirports(){
        Bundle bundle = new Bundle();
        int airPortsId = 0;
        if(pathId==SearchAddressFragment.PATH_FROM){
            airPortsId = AirPortsListFragment.PATH_FROM;
        }else if(pathId==MapFragment.PATH_TO){
            airPortsId = AirPortsListFragment.PATH_TO;
        }
        Bundle args = ((Fragment)mvpView).getArguments();
        boolean forwardFromOrderDetails = false;
        if(args!=null){
            forwardFromOrderDetails = args.getBoolean(ArgumentTitles.FORWARD_FROM_ORDER_DETAILS);
            if(forwardFromOrderDetails){
                bundle.putBoolean(ArgumentTitles.FORWARD_FROM_ORDER_DETAILS,forwardFromOrderDetails);
            }
        }

        bundle.putInt(ArgumentTitles.PATH_ID_TITLE_PARAM,airPortsId);
        FragmentEvent fragmentEvent = FragmentEvent.newInstance(FragmentEvent.SEARCH_IN_AIRPORTS_FRAGMENT,
                true,bundle);
        BusProvider.send(fragmentEvent);
    }

    public void enterEntrance(Location location){
        ((SearchAddressFragment)mvpView).openKeyBoard(false);
        int enterEntranceId = 0;
        String  locationSource = null;
        int fragmentId = -1;
        Bundle bundle = new Bundle();
        Bundle args = ((Fragment)mvpView).getArguments();
        boolean forwardFromOrderDetails = false;
        if(args!=null){
            forwardFromOrderDetails = args.getBoolean(ArgumentTitles.FORWARD_FROM_ORDER_DETAILS);

        }
        if(forwardFromOrderDetails){
            fragmentId = FragmentEvent.TRIP_DETAIL_FRAGMENT;
            if(pathId==SearchAddressFragment.PATH_FROM){
                location.setPathId(Location.FROM);
            }else if(pathId==SearchAddressFragment.PATH_TO){
                location.setPathId(Location.TO);
            }
            location.setEntrance("");
            updateLocationInDatabase(location);
        }else {
            if(pathId==SearchAddressFragment.PATH_FROM){
                enterEntranceId = EnterDataTextFragment.ENTER_ENTRANCE_FROM_FRAGMENT;
                locationSource = ArgumentTitles.LOCATION_FROM;
                fragmentId = FragmentEvent.ENTER_DATA_TEXT_FRAGMENT;
                bundle.putInt(ArgumentTitles.ENTER_DATA_ID,enterEntranceId);
                bundle.putParcelable(locationSource,location);
            }else if(pathId == SearchAddressFragment.PATH_TO) {
//            enterEntranceId = EnterDataTextFragment.ENTER_ENTRANCE_TO_FRAGMENT;
//            locationSource = ArgumentTitles.LOCATION_TO;
                fragmentId = FragmentEvent.TIME_PAYMENT_LIST_FRAGMENT;
                location.setPathId(Location.TO);
                location.setEntrance("");
                updateLocationInDatabase(location);
                bundle.putInt(BlueListTimePaymentFragment.FRAGMENT_LIST_ID_PARAM,BlueListTimePaymentFragment.TIME_LIST_FRAGMENT);
            }
        }

        FragmentEvent fragmentEvent = FragmentEvent.newInstance(fragmentId,
                true,bundle);
        BusProvider.send(fragmentEvent);
    }

    public void searchAddressByTitle(String title){
        Bundle bundle = new Bundle();
        bundle.putString(ArgumentTitles.LOCATION_TITLE_PARAM,title);
        NetworkRequestEvent<Bundle> event = new NetworkRequestEvent();
        event.setId(ApiConstants.SEARCH_STREET);
        event.setData(bundle);
        BusProvider.send(event);
//        Geocoder geoCoder = new Geocoder(((Fragment) mvpView).getContext(), Locale.getDefault());
//        try
//        {
//            List<Address> addresses = geoCoder.getFromLocationName(title, 10,55.5321281,37.56866458,
//                    56.0547853,37.59887698);
//            if (addresses.size() > 0)
//            {
//                List<Location> locationList = new ArrayList<>();
//                for (Address address:addresses){
//                    if(address.getCountryCode().equalsIgnoreCase("RU")){
//                        String addressTitle = "";
//                        if(address.getAddressLine(0)!=null){
//                            addressTitle +=address.getAddressLine(0);
//                        }
//                        if(address.getAddressLine(1)!=null){
//                            addressTitle +=address.getAddressLine(1);
//
//                        }
//                        Location location = Location.createLocation(addressTitle,
//                                address.getLatitude(),address.getLongitude());
//                        locationList.add(location);
//                    }
//                }
//                ((SearchAddressFragment) mvpView).updateAddressList(locationList);
//            }
//        }
//        catch (IOException e)
//        {
//            e.printStackTrace();
//        }
    }



    public void searchAddressByTitleCallBack(List<Location> locations){
        ((SearchAddressFragment)mvpView).searchAddressSuccess(locations);
    }

    public void searchHouseByStreetId(Location location){
        if (location != null) {
            ((SearchAddressFragment)mvpView).setCheckedLocation(location);
            Bundle bundle = new Bundle();
            bundle.putString(ArgumentTitles.STREET_ID,location.getServerId());
            NetworkRequestEvent<Bundle> event = new NetworkRequestEvent();
            event.setId(ApiConstants.SEARCH_HOUSE_BY_STREET_ID);
            event.setData(bundle);
            BusProvider.send(event);
        }
    }



    public void getAllLocation(int pathId){
        List<Location> locationList;
        if(pathId==SearchAddressFragment.PATH_FROM){
            locationList = locationInteractor.getAllFromLocationsDb();
        }else {
            locationList = locationInteractor.getAllToLocationsDb();
        }
        ((SearchAddressFragment) mvpView).updateAddressList(locationList);

    }

    @Override
    public void registerSubscriber() {
        subscriber = new Subscriber() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Object o) {
                if(o instanceof UpdateUiEvent){
                    UpdateUiEvent updateUiEvent = (UpdateUiEvent)o;
                     if(updateUiEvent.getId()==UpdateUiEvent.SEARCH_ADDRESS_BY_TITLE){
                        searchAddressByTitleCallBack((List<Location>) updateUiEvent.getData());
                    }
                }else if(o instanceof ListItemClickEvent){
                    ListItemClickEvent listItemClickEvent = (ListItemClickEvent)o;
                    if(listItemClickEvent.getId()==ListItemClickEvent.SEARCH_ADDRESS_FULL_LIST_CLICK){
                        enterEntrance((Location) listItemClickEvent.getData());
                    }else if(listItemClickEvent.getId()==ListItemClickEvent.SEARCH_ADDRESS_LIST_CLICK_EVENT){
                        searchHouseByStreetId((Location) listItemClickEvent.getData());

                    }
                }
            }
        };
        BusProvider.observe().observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public void unRegisterSubscriber() {
        if(subscriber!=null&&!subscriber.isUnsubscribed()){
            subscriber.unsubscribe();
        }
    }

    public void setPathId(int pathId) {
        this.pathId = pathId;
    }

    private void updateLocationInDatabase(Location location){
        locationInteractor.saveLocationDb(location);
        Order order = orderInteractor.getCurrentOrderFromDb();
        if(order==null){
            order = new Order();
            order.setCurrentOrder(true);
        }
        switch (pathId){
            case SearchAddressFragment.PATH_FROM:
                order.setLocationFrom(location);
                order.setLocationFromId(location.getId());
                break;
            case SearchAddressFragment.PATH_TO:
                order.setLocationTo(location);
                order.setLocationToId(location.getId());
                break;
        }

        orderInteractor.updateOrderInDb(order);
    }
}
