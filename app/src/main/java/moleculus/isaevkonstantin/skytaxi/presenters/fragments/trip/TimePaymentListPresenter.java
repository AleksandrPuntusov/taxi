package moleculus.isaevkonstantin.skytaxi.presenters.fragments.trip;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.eventbus.BusProvider;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.constants.ApiConstants;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.network.NetworkRequestEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.FragmentEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.ListItemClickEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.UpdateUiEvent;
import moleculus.isaevkonstantin.skytaxi.interactors.OrderInteractor;
import moleculus.isaevkonstantin.skytaxi.models.Order;
import moleculus.isaevkonstantin.skytaxi.presenters.BasePresenter;
import moleculus.isaevkonstantin.skytaxi.presenters.interfaces.PresenterEventListener;
import moleculus.isaevkonstantin.skytaxi.components.constants.ArgumentTitles;
import moleculus.isaevkonstantin.skytaxi.components.fragments.main.MapFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.EnterDataTextFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.trip.BlueListTimePaymentFragment;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpActionView;
import moleculus.isaevkonstantin.skytaxi.components.model.ActivityViewBuilder;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Isaev Konstantin on 23.04.16.
 */
public class TimePaymentListPresenter extends BasePresenter implements PresenterEventListener {
    private Subscriber subscriber;
    private OrderInteractor orderInteractor;

    @Override
    public void init() {
        orderInteractor = new OrderInteractor(((Fragment)mvpView).getContext());
    }


    @Override
    public void destroy() {
        super.destroy();
        subscriber = null;
    }



    public void updateActionBar(int fragmentId){
        String title;
        if(fragmentId==BlueListTimePaymentFragment.TIME_LIST_FRAGMENT){
            title = ((Fragment) mvpView).getContext().getResources().getString(R.string.enter_time_action_bar_title);
        }else {
            title = ((Fragment) mvpView).getContext().getResources().getString(R.string.payment_action_bar_title);
        }
        ActivityViewBuilder activityViewBuilder = ActivityViewBuilder.createNavigation(title,ActivityViewBuilder.BACK_NAVIGATION);
        activityViewBuilder.setColor(R.color.white);
        ((MvpActionView) ((Fragment)mvpView).getActivity()).updateUi(activityViewBuilder);
    }

    @Override
    public void registerSubscriber() {
        subscriber = new Subscriber() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Object o) {
                if(o instanceof ListItemClickEvent){
                    ListItemClickEvent listItemClickEvent = (ListItemClickEvent)o;
                    onListItemClick(listItemClickEvent);
                }else if(o instanceof UpdateUiEvent){
                    UpdateUiEvent updateUiEvent  =(UpdateUiEvent)o;
                    if(updateUiEvent.getId()==UpdateUiEvent.CREATE_ORDER){
                        Bundle bundle = new Bundle();
                        bundle.putInt(ArgumentTitles.PATH_ID_TITLE_PARAM,MapFragment.TRIP_MAP);
                        FragmentEvent fragmentEvent = FragmentEvent.newInstance(FragmentEvent.MAP_FRAGMENT,
                                true,bundle);
                        BusProvider.send(fragmentEvent);
                    }
                }
            }
        };
        BusProvider.observe().observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public void unRegisterSubscriber() {
        if(subscriber!=null&&!subscriber.isUnsubscribed()){
            subscriber.unsubscribe();
        }
    }

    private void onListItemClick(ListItemClickEvent listItemClickEvent){
        FragmentEvent fragmentEvent = null;
        Bundle bundle = new Bundle();

        int id = listItemClickEvent.getId();
//        switch (id){
//            case ListItemClickEvent.SHORT_TIME_LIST_CLICK:
//                break;
//            case ListItemClickEvent.THIRTY_MINUTES_LIST_CLICK:
//                break;
//            case ListItemClickEvent.FORTY_FIVE_LIST_CLICK:
//                break;
//            case ListItemClickEvent.HOUR_LIST_CLICK:
//            case ListItemClickEvent.PAY_BY_CARD_LIST_CLICK:
//            case ListItemClickEvent.PAY_BY_CASH_LIST_CLICK:
//            case ListItemClickEvent.PAY_BY_PROMO_LIST_CLICK:
//        }
        if((id==ListItemClickEvent.SHORT_TIME_LIST_CLICK)
                ||(id==ListItemClickEvent.THIRTY_MINUTES_LIST_CLICK)
                ||(id==ListItemClickEvent.FORTY_FIVE_LIST_CLICK)
                ||(id==ListItemClickEvent.HOUR_LIST_CLICK)){
            Bundle args = ((Fragment)mvpView).getArguments();

            boolean forwardFromOrderDetails;
            if(args!=null){
                forwardFromOrderDetails = args.getBoolean(ArgumentTitles.FORWARD_FROM_ORDER_DETAILS);
            }
            updateCurrentOrderTime(id);
            bundle.putInt(BlueListTimePaymentFragment.TIME_TITLE_PARAM,
                    BlueListTimePaymentFragment.SHORT_TIME);
            fragmentEvent = FragmentEvent.newInstance(FragmentEvent.TRIP_DETAIL_FRAGMENT,
                    true,null);
        }else{
            if((id==ListItemClickEvent.PAY_BY_CARD_LIST_CLICK)
                    ||(id==ListItemClickEvent.PAY_BY_CASH_LIST_CLICK)){
                updateCurrentOrderPayment(id);
//
            }else if(id==ListItemClickEvent.PAY_BY_PROMO_LIST_CLICK){
                bundle.putInt(ArgumentTitles.ENTER_DATA_ID,EnterDataTextFragment.ENTER_PROMO_FRAGMENT);
                fragmentEvent = FragmentEvent.newInstance(FragmentEvent.ENTER_DATA_TEXT_FRAGMENT,
                        true,bundle);
            }
        }
        if (fragmentEvent != null) {
            BusProvider.send(fragmentEvent);
        }

    }

    private void updateCurrentOrderTime(int id){
        int startOrderTimeId;
        switch (id){
            case ListItemClickEvent.SHORT_TIME_LIST_CLICK:
                startOrderTimeId = Order.START_SHORT_TIME;
                break;
            case ListItemClickEvent.THIRTY_MINUTES_LIST_CLICK:
                startOrderTimeId = Order.START_THIRTY_MINUTES;
                break;
            case ListItemClickEvent.FORTY_FIVE_LIST_CLICK:
                startOrderTimeId = Order.START_FORTY_FIVE_MINUTES;
                break;
            case ListItemClickEvent.HOUR_LIST_CLICK:
                startOrderTimeId = Order.START_HOUR;
                break;
            default:
                startOrderTimeId = Order.NOT_SPECIFIED_TIME;
        }
        Order order = orderInteractor.getCurrentOrderFromDb();
        if (order!=null){
            order.setTimeId(startOrderTimeId);
            orderInteractor.updateOrderInDb(order);
        }
    }

    private void updateCurrentOrderPayment(int id){
        int paymentId;
        switch (id){
            case ListItemClickEvent.PAY_BY_CARD_LIST_CLICK:
                paymentId = Order.PAY_BY_CARD;
                break;
            case ListItemClickEvent.PAY_BY_CASH_LIST_CLICK:
                paymentId = Order.PAY_BY_CASH;
                break;
            default:
                paymentId = Order.PAY_NOT_SPECIFIED;
        }
        Order order = orderInteractor.getCurrentOrderFromDb();
        if(order!=null){
            order.setPaymentId(paymentId);
            orderInteractor.updateOrderInDb(order);
            NetworkRequestEvent<Bundle> networkRequestEvent = new NetworkRequestEvent();
            networkRequestEvent.setId(ApiConstants.CREATE_ORDER);
            Bundle bundle = new Bundle();
            networkRequestEvent.setData(bundle);
            BusProvider.send(networkRequestEvent);
        }


    }
}


