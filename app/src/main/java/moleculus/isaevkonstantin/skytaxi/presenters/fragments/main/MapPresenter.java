package moleculus.isaevkonstantin.skytaxi.presenters.fragments.main;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import moleculus.isaevkonstantin.skytaxi.components.activities.MainActivity;
import moleculus.isaevkonstantin.skytaxi.components.constants.ArgumentTitles;
import moleculus.isaevkonstantin.skytaxi.components.fragments.EnterDataTextFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.main.MapFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.main.SearchAddressFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.trip.BlueListTimePaymentFragment;
import moleculus.isaevkonstantin.skytaxi.eventbus.BusProvider;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.LocationEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.constants.ApiConstants;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.network.NetworkRequestEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.FragmentEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.MenuClickEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.UpdateUiEvent;
import moleculus.isaevkonstantin.skytaxi.interactors.LocationInteractor;
import moleculus.isaevkonstantin.skytaxi.interactors.OrderInteractor;
import moleculus.isaevkonstantin.skytaxi.models.Location;
import moleculus.isaevkonstantin.skytaxi.models.Order;
import moleculus.isaevkonstantin.skytaxi.presenters.BasePresenter;
import moleculus.isaevkonstantin.skytaxi.presenters.interfaces.PresenterEventListener;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Isaev Konstantin on 17.04.16.
 */
public class MapPresenter extends BasePresenter implements PresenterEventListener {
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private LocationInteractor locationInteractor;
    private Subscriber subscriber;
    private OrderInteractor orderInteractor;


    @Override
    public void init() {
        googleApiClient = new GoogleApiClient.Builder(((Fragment)mvpView).getContext())
                .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) mvpView)
                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) mvpView)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();
        createLocationRequest();
        locationInteractor = new LocationInteractor(((Fragment)mvpView).getContext());
        orderInteractor = new OrderInteractor(((Fragment)mvpView).getContext());


    }

    @Override
    public void destroy() {
        super.destroy();
        googleApiClient.disconnect();
    }

    public Order getCurrentOrder(){
        Order order  = orderInteractor.getCurrentOrderFromDb();
        return order;
    }




    public void getAddressTitleByCoordinates(double latitude, double longitude) {
        Geocoder geoCoder = new Geocoder(((Fragment) mvpView).getContext(), Locale.getDefault());
        try {
            List<Address> addresses = geoCoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                String street = address.getAddressLine(0);
                ((MapFragment) mvpView).updateAddress(street);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void getLocation() {
        if (ActivityCompat.checkSelfPermission(((Fragment)mvpView).getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(((Fragment)mvpView).getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return ;
        }
        if(isLocationEnable()){
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    googleApiClient, locationRequest, (LocationListener) mvpView);
        }else {
            Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            ((Fragment)mvpView).getContext().startActivity(myIntent);
        }

    }

    public boolean isLocationEnable(){
        Context context = ((Fragment)mvpView).getContext();
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }


    }


    public void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        googleApiClient,
                        buildLocationSettingsRequest()
                );
        result.setResultCallback((ResultCallback<? super LocationSettingsResult>) mvpView);
    }

    private LocationSettingsRequest buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(locationRequest);
        return builder.build();
    }
    public void searchAddressFrom(int mapId) {
        Bundle bundle = new Bundle();
        if(mapId==MapFragment.PATH_FROM){
            bundle.putInt(ArgumentTitles.SEARCH_ADDRESS_ID_PARAM,SearchAddressFragment.PATH_FROM);
        }else if(mapId==MapFragment.PATH_TO){
            bundle.putInt(ArgumentTitles.SEARCH_ADDRESS_ID_PARAM,SearchAddressFragment.PATH_TO);
        }
        bundle.putBoolean(ArgumentTitles.SEARCH_ADDRESS_FROM_MAP,true);
        FragmentEvent fragmentEvent = FragmentEvent.newInstance(FragmentEvent.SEARCH_ADDRESS_FRAGMENT,
                true,bundle);
        BusProvider.send(fragmentEvent);
    }

    public void enterEntrance(String name, double latitude, double longitude, int mapId){
        Bundle bundle = new Bundle();
        Location location = Location.createLocation(name,latitude,longitude);
        String locationParam = "";
        int fragmentId = -1;
        Bundle args = ((Fragment)mvpView).getArguments();
        boolean forwardFromOrderDetails = false;
        if(args!=null){
            forwardFromOrderDetails = args.getBoolean(ArgumentTitles.FORWARD_FROM_ORDER_DETAILS);
        }
        if(forwardFromOrderDetails){
            fragmentId = FragmentEvent.TRIP_DETAIL_FRAGMENT;
            if(mapId==MapFragment.PATH_FROM){
                location.setPathId(Location.FROM);
            }else if(mapId==MapFragment.PATH_TO){
                location.setPathId(Location.TO);
            }
            location.setEntrance("");
            updateLocationInDatabase(location,mapId);
        }else {
            if (mapId == MapFragment.PATH_FROM) {
                locationParam = ArgumentTitles.LOCATION_FROM;
                mapId = EnterDataTextFragment.ENTER_ENTRANCE_FROM_FRAGMENT;
                fragmentId = FragmentEvent.ENTER_DATA_TEXT_FRAGMENT;
                bundle.putInt(ArgumentTitles.ENTER_DATA_ID, mapId);
                bundle.putParcelable(locationParam, location);
            } else if (mapId == MapFragment.PATH_TO) {
//            locationParam = ArgumentTitles.LOCATION_TO;
//            mapId = EnterDataTextFragment.ENTER_ENTRANCE_TO_FRAGMENT;
                fragmentId = FragmentEvent.TIME_PAYMENT_LIST_FRAGMENT;
                location.setPathId(Location.TO);
                location.setEntrance("");
                updateLocationInDatabase(location, mapId);
                bundle.putInt(BlueListTimePaymentFragment.FRAGMENT_LIST_ID_PARAM, BlueListTimePaymentFragment.TIME_LIST_FRAGMENT);

            }
        }
        FragmentEvent fragmentEvent = FragmentEvent.newInstance(fragmentId,
                true,bundle);
        BusProvider.send(fragmentEvent);
    }

    public void showCancelOrder(){
        FragmentEvent fragmentEvent = FragmentEvent.newDialogInstance(FragmentEvent.CANCEL_ORDER_DIALOG,
                (Fragment)mvpView,null);
        BusProvider.send(fragmentEvent);
    }

    public void showSuccessOrder(){
        FragmentEvent fragmentEvent = FragmentEvent.newInstance(FragmentEvent.TRIP_SUCCESS,
                true,null);
        BusProvider.send(fragmentEvent);
    }



    public void cancelOrder(){
        NetworkRequestEvent networkRequestEvent = new NetworkRequestEvent();
        networkRequestEvent.setId(ApiConstants.CANCEL_ORDER);
        Bundle bundle = new Bundle();
        networkRequestEvent.setData(bundle);
        BusProvider.send(networkRequestEvent);
    }

    private void updateLocationInDatabase(Location location,int mapId){
        locationInteractor.saveLocationDb(location);
        Order order = orderInteractor.getCurrentOrderFromDb();
        if(order==null){
            order = new Order();
            order.setCurrentOrder(true);
        }
        switch (mapId){
            case MapFragment.PATH_FROM:
                order.setLocationFrom(location);
                order.setLocationFromId(location.getId());
                break;
            case MapFragment.PATH_TO:
                order.setLocationTo(location);
                order.setLocationToId(location.getId());
                break;
        }
        orderInteractor.updateOrderInDb(order);
    }

    private void createLocationRequest() {
        locationRequest = new LocationRequest();
//        locationRequest.setInterval(UPDATE_INTERVAL);
//        locationRequest.setFastestInterval(FATEST_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
//        locationRequest.setSmallestDisplacement(LocationRequest.);
    }

    public void cancelOrderCallBack(){
        Intent intent = new Intent((AppCompatActivity)((Fragment)mvpView).getActivity(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        BusProvider.send(intent);
    }

    @Override
    public void registerSubscriber() {
        subscriber = new Subscriber() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Object o) {
                if(o instanceof MenuClickEvent){
                    MenuClickEvent updateUiEvent = (MenuClickEvent) o;
                    if(updateUiEvent.getId()==MenuClickEvent.SAVE_LOCATION_MENU_CLICK){
                    }
                }else if(o instanceof LocationEvent){
                    getLocation();
                }else if(o instanceof UpdateUiEvent){
                    UpdateUiEvent event = (UpdateUiEvent)o;
                    if(event.getId()==UpdateUiEvent.CANCEL_ORDER){
                        cancelOrderCallBack();
                    }else if(event.getId()==UpdateUiEvent.UPDATE_ORDER){
                        ((MapFragment)mvpView).updateOrder();
                    }
                }
            }
        };
        BusProvider.observe().observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public void unRegisterSubscriber() {
        if(subscriber!=null&&!subscriber.isUnsubscribed()){
            subscriber.unsubscribe();
        }
    }
}
