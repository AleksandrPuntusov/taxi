package moleculus.isaevkonstantin.skytaxi.presenters.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;

import moleculus.isaevkonstantin.skytaxi.eventbus.BusProvider;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.FragmentEvent;
import moleculus.isaevkonstantin.skytaxi.navigation.navigators.MainNavigator;
import moleculus.isaevkonstantin.skytaxi.presenters.BasePresenter;
import moleculus.isaevkonstantin.skytaxi.presenters.interfaces.PresenterEventListener;
import moleculus.isaevkonstantin.skytaxi.components.activities.MainActivity;
import moleculus.isaevkonstantin.skytaxi.components.activities.RegistrationActivity;
import moleculus.isaevkonstantin.skytaxi.components.activities.SplashActivity;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Isaev Konstantin on 10.04.16.
 */
public class SplashActivityPresenter extends BasePresenter implements PresenterEventListener {
    private MainNavigator navigator;
    private Subscriber subscriber;

    @Override
    public void init() {
        navigator = new MainNavigator();
        navigator.setActivity((AppCompatActivity)mvpView);
        navigator.init();

    }

    @Override
    public void destroy() {
        super.destroy();
        navigator.destroy();
        navigator = null;
        subscriber = null;
    }

    public void createSplash(){
        FragmentEvent fragmentEvent =
                FragmentEvent.newInstance(FragmentEvent.SPLASH_FRAGMENT,false,null);
        navigator.navigateToFragment(fragmentEvent);
    }

    public void createWalkThrought(){
        FragmentEvent fragmentEvent =
                FragmentEvent.newInstance(FragmentEvent.WALKTHROUGHT_FRAGMENT,true,null);
        navigator.navigateToFragment(fragmentEvent);
    }

    public void startRegistrationActivity(){
        Intent intent = new Intent((AppCompatActivity)mvpView, RegistrationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        navigator.navigateToActivity(intent);
    }

    public void startMainActivity(){
        Intent intent = new Intent((AppCompatActivity)mvpView, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        navigator.navigateToActivity(intent);
    }

    @Override
    public void registerSubscriber() {
        subscriber = new Subscriber() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Object o) {
                if(o instanceof FragmentEvent){
                    FragmentEvent fragmentEvent = (FragmentEvent)o;
                    navigator.navigateToFragment(fragmentEvent);
                }else if(o instanceof Intent){
                    Intent intent = (Intent)o;
                    SharedPreferences sharedPref = ((AppCompatActivity)mvpView).getApplicationContext().getSharedPreferences("SkyTaxiPrefs", Context.MODE_PRIVATE);
                    sharedPref.edit().putBoolean(SplashActivity.WALK_THROUGHT_TITLE,true).commit();
                    navigator.navigateToActivity(intent);
                }
            }
        };
        BusProvider.observe().observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public void unRegisterSubscriber() {
        if(subscriber!=null&&!subscriber.isUnsubscribed()){
            subscriber.unsubscribe();
        }
    }
}
