package moleculus.isaevkonstantin.skytaxi.presenters.fragments.trip;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import moleculus.isaevkonstantin.skytaxi.eventbus.BusProvider;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.constants.ApiConstants;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.network.NetworkRequestEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.FragmentEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.UpdateUiEvent;
import moleculus.isaevkonstantin.skytaxi.interactors.LocationInteractor;
import moleculus.isaevkonstantin.skytaxi.interactors.OrderInteractor;
import moleculus.isaevkonstantin.skytaxi.models.Order;
import moleculus.isaevkonstantin.skytaxi.presenters.BasePresenter;
import moleculus.isaevkonstantin.skytaxi.presenters.interfaces.PresenterEventListener;
import moleculus.isaevkonstantin.skytaxi.components.constants.ArgumentTitles;
import moleculus.isaevkonstantin.skytaxi.components.fragments.trip.BlueListTimePaymentFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.trip.TripDetailsFragment;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Isaev Konstantin on 07.05.16.
 */
public class TripDetailsPresenter extends BasePresenter implements PresenterEventListener {
    private Subscriber subscriber;
    private LocationInteractor locationInteractor;
    private OrderInteractor orderInteractor;
    @Override
    public void init() {
        orderInteractor = new OrderInteractor(((Fragment)mvpView).getContext());
        locationInteractor = new LocationInteractor(((Fragment)mvpView).getContext());
    }



    /**
     * Method send network request event to Activity for getting order cost.
     */
    public void getTripCost(){
        NetworkRequestEvent<Bundle> networkRequestEvent = new NetworkRequestEvent();
        networkRequestEvent.setId(ApiConstants.GET_ORDER_COST);
        Bundle bundle = new Bundle();
        networkRequestEvent.setData(bundle);
        BusProvider.send(networkRequestEvent);
    }

    public void getTripSettings(){
        FragmentEvent fragmentEvent = FragmentEvent.newInstance(FragmentEvent.TRIP_SETTINGS_FRAGMENT,
                true,null);
        BusProvider.send(fragmentEvent);
    }

    public void getCurentOrder(){
        Order order = orderInteractor.getCurrentOrderFromDb();
        ((TripDetailsFragment)mvpView).updateOrder(order);
    }

    public void setOrderTime(){
        Bundle bundle = new Bundle();
        bundle.putInt(BlueListTimePaymentFragment.FRAGMENT_LIST_ID_PARAM,BlueListTimePaymentFragment.TIME_LIST_FRAGMENT);
        FragmentEvent fragmentEvent = FragmentEvent.newInstance(FragmentEvent.TIME_PAYMENT_LIST_FRAGMENT,
                true,bundle);
        BusProvider.send(fragmentEvent);
    }

    public void setLocation(int locationId){
        Bundle bundle = new Bundle();
        bundle.putBoolean(ArgumentTitles.SEARCH_ADDRESS_FROM_MAP,false);
        bundle.putInt(ArgumentTitles.SEARCH_ADDRESS_ID_PARAM,locationId);
        bundle.putBoolean(ArgumentTitles.FORWARD_FROM_ORDER_DETAILS,true);
        FragmentEvent fragmentEvent = FragmentEvent.newInstance(FragmentEvent.SEARCH_ADDRESS_FRAGMENT,
                true,bundle);
        BusProvider.send(fragmentEvent);
    }

    public void showCost(){
        FragmentEvent fragmentEvent = FragmentEvent.newInstance(FragmentEvent.TRIP_COST_FRAGMENT,
                true,null);
        BusProvider.send(fragmentEvent);
    }

    @Override
    public void registerSubscriber() {
        subscriber = new Subscriber() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Object o) {
                if(o instanceof UpdateUiEvent){
                    UpdateUiEvent updateUiEvent = (UpdateUiEvent)o;
                    if(updateUiEvent.getId()==UpdateUiEvent.GET_ORDER_COST_EVENT){
                        showCost();
                    }
                }
            }
        };
        BusProvider.observe().observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public void unRegisterSubscriber() {
        if(subscriber!=null&&!subscriber.isUnsubscribed()){
            subscriber.unsubscribe();
        }
    }
}
