package moleculus.isaevkonstantin.skytaxi.presenters.fragments.main;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;

import moleculus.isaevkonstantin.skytaxi.eventbus.BusProvider;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.constants.ApiConstants;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.network.NetworkRequestEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.FragmentEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.UpdateUiEvent;
import moleculus.isaevkonstantin.skytaxi.interactors.LocationInteractor;
import moleculus.isaevkonstantin.skytaxi.interactors.OrderInteractor;
import moleculus.isaevkonstantin.skytaxi.models.Location;
import moleculus.isaevkonstantin.skytaxi.models.Order;
import moleculus.isaevkonstantin.skytaxi.models.OrderOptions;
import moleculus.isaevkonstantin.skytaxi.presenters.BasePresenter;
import moleculus.isaevkonstantin.skytaxi.presenters.interfaces.PresenterEventListener;
import moleculus.isaevkonstantin.skytaxi.components.constants.ArgumentTitles;
import moleculus.isaevkonstantin.skytaxi.components.fragments.main.SearchAddressFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.profile.ProfileFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.EnterDataTextFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.trip.BlueListTimePaymentFragment;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Isaev Konstantin on 11.04.16.
 */
public class EnterDataPresenter extends BasePresenter implements PresenterEventListener {
    private Subscriber subscriber;
    private OrderInteractor orderInteractor;
    private LocationInteractor locationInteractor;

    @Override
    public void init() {
        orderInteractor = new OrderInteractor(((Fragment) mvpView).getContext());
        locationInteractor = new LocationInteractor(((Fragment) mvpView).getContext());
    }

    @Override
    public void destroy() {
        super.destroy();
        subscriber = null;
    }


    public void sendData(String text, String smsCode, int fragmentId) {
        NetworkRequestEvent<Bundle> networkRequestEvent = new NetworkRequestEvent<>();
        Bundle bundle = new Bundle();
        Bundle args = null;
        FragmentEvent fragmentEvent;
        switch (fragmentId) {
            case EnterDataTextFragment.ENTER_PHONE_FRAGMENT:
                networkRequestEvent.setId(ApiConstants.SEND_PHONE);
                bundle.putString(ArgumentTitles.ENTER_DATA_TEXT, text);
                networkRequestEvent.setData(bundle);
                BusProvider.send(networkRequestEvent);
                break;
            case EnterDataTextFragment.ENTER_SMS_CODE_FRAGMENT:
                networkRequestEvent.setId(ApiConstants.CONFIRM_SMS_CODE);
                bundle.putString(ArgumentTitles.PHONE, smsCode);
                bundle.putString(ArgumentTitles.SMS_CODE, text);
                networkRequestEvent.setData(bundle);
                BusProvider.send(networkRequestEvent);
                break;
            case EnterDataTextFragment.ENTER_ENTRANCE_FROM_FRAGMENT:
                args = ((Fragment) mvpView).getArguments();
                if (args != null) {
                    Location location = args.getParcelable(ArgumentTitles.LOCATION_FROM);
                    location.setPathId(Location.FROM);
                    location.setEntrance(text);
                    locationInteractor.saveLocationDb(location);
                    Order order = orderInteractor.getCurrentOrderFromDb();
                    if (order == null) {
                        order = new Order();
                        order.setCurrentOrder(true);

                    }
                    OrderOptions orderOptions = order.getOrderOptions();
                    OrderOptions defaultOptions = OrderOptions.generateDefaultOption();
                    ;
                    if (order.getOrderOptions() != null) {
                        defaultOptions.setId(orderOptions.getId());
                    }
                    orderOptions = defaultOptions;
                    order.setOrderOptions(orderOptions);
                    orderInteractor.updateOrderOptionInDb(orderOptions);
                    order.setOrderOptionFk(orderOptions.getId());
                    order.setLocationFrom(location);
                    order.setLocationFromId(location.getId());
                    orderInteractor.updateOrderInDb(order);

                }
                bundle.putInt(ArgumentTitles.SEARCH_ADDRESS_ID_PARAM, SearchAddressFragment.PATH_TO);
                fragmentEvent = FragmentEvent.newInstance(FragmentEvent.SEARCH_ADDRESS_FRAGMENT
                        , true, bundle);
                BusProvider.send(fragmentEvent);
                break;
            case EnterDataTextFragment.ENTER_ENTRANCE_TO_FRAGMENT:
                args = ((Fragment) mvpView).getArguments();
                if (args != null) {
                    Location location = args.getParcelable(ArgumentTitles.LOCATION_TO);
                    location.setPathId(Location.TO);
                    location.setEntrance(text);
                    locationInteractor.saveLocationDb(location);
                    Order order = orderInteractor.getCurrentOrderFromDb();
                    if (order == null) {
                        order = new Order();
                        order.setCurrentOrder(true);
                    }
                    order.setLocationTo(location);
                    order.setLocationToId(location.getId());

                    orderInteractor.updateOrderInDb(order);

                }
                bundle.putInt(BlueListTimePaymentFragment.FRAGMENT_LIST_ID_PARAM, BlueListTimePaymentFragment.TIME_LIST_FRAGMENT);
                fragmentEvent = FragmentEvent.newInstance(FragmentEvent.TIME_PAYMENT_LIST_FRAGMENT
                        , true, bundle);
                BusProvider.send(fragmentEvent);
                break;
            case EnterDataTextFragment.ENTER_PROMO_FRAGMENT:
                fragmentEvent = FragmentEvent.newDialogInstance(FragmentEvent.PROMO_SUCCESS_DIALOG,
                        (Fragment) mvpView, bundle);
                BusProvider.send(fragmentEvent);
                break;
        }
    }

    public void enterDataCallBack(final UpdateUiEvent updateUiEvent) {
        boolean success = updateUiEvent.isSuccess();
        ((EnterDataTextFragment) mvpView).sendDataResponse(success, "");
        Handler handler;
        Runnable runnable;
        if (success) {
            switch (updateUiEvent.getId()) {
                case UpdateUiEvent.ENTER_PHONE_PRESENTER_EVENT:
                    runnable = new Runnable() {
                        public void run() {
                            Bundle bundle = new Bundle();
                            bundle.putInt(ArgumentTitles.ENTER_DATA_ID, EnterDataTextFragment.ENTER_SMS_CODE_FRAGMENT);
                            bundle.putString(ArgumentTitles.PHONE, (String) updateUiEvent.getData());
                            FragmentEvent fragmentEvent = FragmentEvent.newInstance(
                                    FragmentEvent.ENTER_DATA_TEXT_FRAGMENT,
                                    true, bundle);
                            BusProvider.send(fragmentEvent);
                        }
                    };
                    handler = new Handler();
                    handler.postDelayed(runnable, 500);
                    break;
                case UpdateUiEvent.ENTER_SMS_CODE_PRESENTER_EVENT:
                    runnable = new Runnable() {
                        public void run() {
                            Bundle bundle = new Bundle();
                            Context context = ((Fragment) mvpView).getContext();
                            context.getSharedPreferences("SkyTaxiPrefs", Context.MODE_PRIVATE).edit()
                                    .putBoolean(ArgumentTitles.REGISTRATION_SUCCESS_SHAR_PREF, true).commit();
                            bundle.putInt(ProfileFragment.PROFILE_ID_TITLE_PARAM, ProfileFragment.FIRST_SETTINGS_PROFILE);
                            FragmentEvent fragmentEvent = FragmentEvent.newInstance(FragmentEvent.PROFILE_FRAGMENT
                                    , true, bundle);
                            BusProvider.send(fragmentEvent);
                        }
                    };
                    handler = new Handler();
                    handler.postDelayed(runnable, 500);
                    break;
            }
        }
    }

    public boolean validateText(String text, int fragmentId) {
        boolean success = false;
        switch (fragmentId) {
            case EnterDataTextFragment.ENTER_PHONE_FRAGMENT:
                success = isValidPhoneNumber(text);
                break;
            case EnterDataTextFragment.ENTER_SMS_CODE_FRAGMENT:
                success = isValidSmsCode(text);
                break;
            case EnterDataTextFragment.ENTER_ENTRANCE_FROM_FRAGMENT:
//                success = isValidFlatNumber(text); //  no do validation
                success = true;
                break;
            case EnterDataTextFragment.ENTER_ENTRANCE_TO_FRAGMENT:
                success = isValidFlatNumber(text);
                break;
            case EnterDataTextFragment.ENTER_PROMO_FRAGMENT:
                success = isValidPromo(text);
                break;
        }

        return success;
    }

    @Override
    public void registerSubscriber() {
        subscriber = new Subscriber() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Object o) {
                if (o instanceof UpdateUiEvent) {
                    UpdateUiEvent updateUiEvent = (UpdateUiEvent) o;
                    int id = updateUiEvent.getId();
                    if ((id == UpdateUiEvent.ENTER_PHONE_PRESENTER_EVENT)
                            || (id == UpdateUiEvent.ENTER_SMS_CODE_PRESENTER_EVENT)) {
                        enterDataCallBack(updateUiEvent);
                    }
                }
            }
        };
        BusProvider.observe().observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public void unRegisterSubscriber() {
        if (subscriber != null && !subscriber.isUnsubscribed()) {
            subscriber.unsubscribe();
        }
    }

    private boolean isValidPhoneNumber(String text) {
        boolean success = false;
        if (text.length() == 11) {
            success = true;
        }
        return success;
    }

    private boolean isValidSmsCode(String text) {
        boolean success = false;
        if (text.length() == 4) {
            success = true;
        }
        return success;
    }

    private boolean isValidFlatNumber(String text) {
        boolean success = false;
        if (text.length() > 0) {
            success = true;
        }
        return success;
    }

    private boolean isValidPromo(String text) {
        boolean success = true;

        return success;
    }

}
