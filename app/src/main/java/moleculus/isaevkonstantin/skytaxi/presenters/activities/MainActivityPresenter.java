package moleculus.isaevkonstantin.skytaxi.presenters.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;

import moleculus.isaevkonstantin.skytaxi.components.activities.MainActivity;
import moleculus.isaevkonstantin.skytaxi.components.activities.SplashActivity;
import moleculus.isaevkonstantin.skytaxi.components.constants.ArgumentTitles;
import moleculus.isaevkonstantin.skytaxi.components.fragments.main.MapFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.profile.ProfileFragment;
import moleculus.isaevkonstantin.skytaxi.eventbus.BusProvider;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.LocationEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.constants.ApiConstants;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.network.NetworkRequestEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.network.NetworkResponseEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.FragmentEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.ListItemClickEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.UpdateUiEvent;
import moleculus.isaevkonstantin.skytaxi.interactors.OrderInteractor;
import moleculus.isaevkonstantin.skytaxi.interactors.UserInteractor;
import moleculus.isaevkonstantin.skytaxi.models.Order;
import moleculus.isaevkonstantin.skytaxi.models.User;
import moleculus.isaevkonstantin.skytaxi.navigation.navigators.MainNavigator;
import moleculus.isaevkonstantin.skytaxi.presenters.BasePresenter;
import moleculus.isaevkonstantin.skytaxi.presenters.interfaces.PresenterEventListener;
import moleculus.isaevkonstantin.skytaxi.rest.utils.InternetUtil;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Isaev Konstantin on 14.04.16.
 */
public class MainActivityPresenter extends BasePresenter implements PresenterEventListener {
    private MainNavigator mainNavigator;
    private Subscriber subscriber;
    private UserInteractor userInteractor;
    private OrderInteractor orderInteractor;

    @Override
    public void init() {
        mainNavigator = new MainNavigator();
        mainNavigator.setActivity((AppCompatActivity) mvpView);
        mainNavigator.init();
        userInteractor = new UserInteractor(((AppCompatActivity)mvpView).getApplicationContext());
        orderInteractor = new OrderInteractor(((AppCompatActivity)mvpView).getApplicationContext());

    }

    @Override
    public void destroy() {
        super.destroy();
        mainNavigator.destroy();
        mainNavigator = null;
        subscriber = null;
    }

    public boolean checkOrder(){
        Order order = orderInteractor.getCurrentOrderFromDb();
        if(order!=null){
            if(order.getId()==Order.DRIVER_SEARCH_STATUS||
                    order.getId()==Order.DRIWER_ON_RIDE_STATUS||
                    order.getId()==Order.DRIWER_ON_PLACE_STATUS
                    ){
                createMap(MapFragment.TRIP_MAP);
                return true;
            }else {
                createMap(MapFragment.PATH_FROM);

                return false;
            }
        }else {
            createMap(MapFragment.PATH_FROM);

            return false;

        }
    }

    public void createMap(int mapId) {
        Bundle bundle = new Bundle();
        bundle.putInt(ArgumentTitles.PATH_ID_TITLE_PARAM,mapId);
        FragmentEvent fragmentEvent = FragmentEvent.newInstance(FragmentEvent.MAP_FRAGMENT,
                true,bundle);
        mainNavigator.navigateToFragment(fragmentEvent);
    }

    public void clearAddToBackStack() {
        mainNavigator.clearAddToBackStack();
    }

    public void shareWithFriend() {
        FragmentEvent fragmentEvent = FragmentEvent.newInstance(FragmentEvent.CALL_FRIEND_FRAGMENT,
                true,null);
        mainNavigator.navigateToFragment(fragmentEvent);

    }

    public void openProfile() {
        Bundle bundle = new Bundle();
        bundle.putInt(ProfileFragment.PROFILE_ID_TITLE_PARAM,ProfileFragment.FROM_SLIDE_MENU_SETTINGS_PROFILE);
        FragmentEvent fragmentEvent = FragmentEvent.newInstance(FragmentEvent.PROFILE_FRAGMENT,
                true,bundle);
        mainNavigator.navigateToFragment(fragmentEvent);
    }



    private void makeCallToCallCentre() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:+7(495)255-55-05"));
        if (ActivityCompat.checkSelfPermission(((AppCompatActivity) mvpView).getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mainNavigator.navigateToActivity(callIntent);
    }

    private void logout() {
        NetworkRequestEvent<Bundle> event = new NetworkRequestEvent();
        event.setId(ApiConstants.LOGOUT);
        User user = userInteractor.getUserFromDb(0);
        Bundle bundle = new Bundle();
        bundle.putString(ArgumentTitles.AUTH_TOKEN, user.getAuthToken());
        event.setData(bundle);
        ((MainActivity) mvpView).loadFromApi(event);
    }

    public void logoutCallBack(UpdateUiEvent event){
        Context context = ((AppCompatActivity)mvpView).getApplicationContext();
        Intent intent = new Intent(context,SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        SharedPreferences sharedPref = context.getSharedPreferences("SkyTaxiPrefs", Context.MODE_PRIVATE);
        sharedPref.edit().putBoolean(ArgumentTitles.REGISTRATION_SUCCESS_SHAR_PREF,false)
                .putBoolean(ArgumentTitles.FIRST_SETTINGS_SUCCESS_SHAR_PREF,false).commit();
        mainNavigator.navigateToActivity(intent);
    }



    public void syncUser(User user){
        if(user!=null) {
            NetworkRequestEvent<Bundle> networkRequestEvent = new NetworkRequestEvent();
            networkRequestEvent.setId(ApiConstants.GET_USER);
            Bundle bundle = new Bundle();
            bundle.putString(ArgumentTitles.AUTH_TOKEN, user.getAuthToken());
            networkRequestEvent.setData(bundle);
            ((MainActivity) mvpView).loadFromApi(networkRequestEvent);
        }
    }


    public void updateUserProfile(User user){
        if(user!=null){
            ((MainActivity)mvpView).updateUserProfile(user);
        }
    }

    public void rightMenuClick(){
        FragmentEvent fragmentEvent = FragmentEvent.newInstance(FragmentEvent.SEARCH_IN_FAVORITES_FRAGMENT,
                true,null);
        mainNavigator.navigateToFragment(fragmentEvent);
    }

    public void locationSuccess(){
        LocationEvent locationEvent = new LocationEvent();
        BusProvider.send(locationEvent);
    }

    @Override
    public void registerSubscriber() {
        subscriber = new Subscriber() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Object o) {
                if (o instanceof ListItemClickEvent) {
                    ListItemClickEvent event = (ListItemClickEvent) o;
                    switch (event.getId()) {
                        case ListItemClickEvent.CALL_FRIEND_NAV_CLICK:
                            shareWithFriend();
                            ((MainActivity) mvpView).closeNavDrawer();
                            break;
                        case ListItemClickEvent.PROMO_NAV_CLICK:
                            ((MainActivity) mvpView).closeNavDrawer();
                            getPromoCode();
                            break;
                        case ListItemClickEvent.CREDIT_CARD_NAV_CLICK:
                            ((MainActivity) mvpView).closeNavDrawer();
                            setCreditCard();
                            break;
                        case ListItemClickEvent.CARS_NAV_CLICK:
                            ((MainActivity) mvpView).closeNavDrawer();
                            break;
                        case ListItemClickEvent.MAKE_CALL_NAV_CLICK:
                            ((MainActivity) mvpView).closeNavDrawer();
                            makeCallToCallCentre();
                            break;
                        case ListItemClickEvent.LOGOUT_NAV_CLICK:
                            ((MainActivity) mvpView).closeNavDrawer();
                            logout();
                            break;
                        case ListItemClickEvent.SHOW_DRIVERS_ON_MAP_SETTINGS_NAV:
                            break;

                    }
                }else if(o instanceof NetworkResponseEvent){
                    NetworkResponseEvent event = (NetworkResponseEvent)o;
                    switch (event.getId()){
                        case ApiConstants.GET_USER:
                            updateUserProfile((User) event.getData());
                            break;
                        case ApiConstants.CREATE_ORDER:
                            ((MainActivity)mvpView).listenOrderStatus();
                            break;
                    }
                }else if(o instanceof NetworkRequestEvent){
                    NetworkRequestEvent event = (NetworkRequestEvent)o;
                    if(InternetUtil.checkInternetConnection((AppCompatActivity) mvpView)){
                        ((MainActivity)mvpView).loadFromApi(event);
                    }else {
                        ((MainActivity) mvpView).makeToast("Нет интернет соединени");
                    }
                }else if(o instanceof Intent){
                    Intent intent = (Intent) o;
                    mainNavigator.navigateToActivity(intent);
                }else if(o instanceof FragmentEvent){
                    FragmentEvent fragmentEvent = (FragmentEvent)o;
                    mainNavigator.navigateToFragment(fragmentEvent);
                }else if(o instanceof UpdateUiEvent){
                    UpdateUiEvent event = (UpdateUiEvent)o;
                    if(event.getId()==UpdateUiEvent.UPDATE_PROFILE_EVENT
                            ||event.getId()==UpdateUiEvent.SYNC_PROFILE_PRESENTER_EVENT){
                        ((AppCompatActivity)mvpView)
                                .getSupportLoaderManager().getLoader(MainActivity.USER_LOADER_ID).onContentChanged();
                    }else if(event.getId()==UpdateUiEvent.LOGOUT_EVENT){
                        logoutCallBack(event);
                    }
                }
            }
        };
        BusProvider.observe().observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public void unRegisterSubscriber() {
        if(subscriber!=null&&!subscriber.isUnsubscribed()){
            subscriber.unsubscribe();
        }

    }

    public void setCreditCard(){
        FragmentEvent fragmentEvent = FragmentEvent.newInstance(FragmentEvent.SET_CARD_FRAGMENT,
                true,null);
        mainNavigator.navigateToFragment(fragmentEvent);
    }

    public void getPromoCode(){
        FragmentEvent fragmentEvent = FragmentEvent.newInstance(FragmentEvent.PROMO_FRAGMENT,
                true,null);
        mainNavigator.navigateToFragment(fragmentEvent);

    }
}
