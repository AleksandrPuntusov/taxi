package moleculus.isaevkonstantin.skytaxi.logger;

import android.util.Log;

/**
 * Created by Isaev Konstantin on 02.05.16.
 */
public class Logger {

    public static final boolean ENABLE_LOG = true;

    public static void debug(String tag,String message){
        if(ENABLE_LOG){
            Log.d(tag,message);
        }
    }
}
