package moleculus.isaevkonstantin.skytaxi.eventbus.events.ui;

import moleculus.isaevkonstantin.skytaxi.eventbus.events.BaseEvent;

/**
 * Created by Isaev Konstantin on 02.05.16.
 */
public class UpdateUiEvent<T> extends BaseEvent {

    public static final int ENTER_PHONE_PRESENTER_EVENT = 0;
    public static final int ENTER_SMS_CODE_PRESENTER_EVENT = 1;
    public static final int SYNC_PROFILE_PRESENTER_EVENT = 2;
    public static final int UPDATE_PROFILE_EVENT = 3;
    public static final int LOGOUT_EVENT = 6;
    public static final int GET_AIR_PORTS_EVENT = 7;
    public static final int GET_ORDER_COST_EVENT = 8;
    public static final int CREATE_ORDER = 9;
    public static final int SEARCH_ADDRESS_BY_TITLE = 10;
    public static final int CANCEL_ORDER = 11;
    public static final int UPDATE_ORDER = 12;

    private T data;
    private boolean success;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
