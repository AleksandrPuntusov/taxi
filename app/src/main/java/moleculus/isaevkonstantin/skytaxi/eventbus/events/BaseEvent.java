package moleculus.isaevkonstantin.skytaxi.eventbus.events;

/**
 * Created by Isaev Konstantin on 30.03.16.
 */
public class BaseEvent {

    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
