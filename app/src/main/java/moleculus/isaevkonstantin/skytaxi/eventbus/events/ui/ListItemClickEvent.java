package moleculus.isaevkonstantin.skytaxi.eventbus.events.ui;

import moleculus.isaevkonstantin.skytaxi.eventbus.events.BaseEvent;

/**
 * Created by Isaev Konstantin on 16.04.16.
 */
public class ListItemClickEvent<T> extends BaseEvent {

    public static final int PROMO_NAV_CLICK = 0;
    public static final int CREDIT_CARD_NAV_CLICK = 1;
    public static final int CALL_FRIEND_NAV_CLICK = 2;
    public static final int CARS_NAV_CLICK = 3;
    public static final int MAKE_CALL_NAV_CLICK = 4;
    public static final int LOGOUT_NAV_CLICK = 5;

    public static final int SHORT_TIME_LIST_CLICK = 6;
    public static final int THIRTY_MINUTES_LIST_CLICK = 7;
    public static final int FORTY_FIVE_LIST_CLICK = 8;
    public static final int HOUR_LIST_CLICK = 9;

    public static final int PAY_BY_CARD_LIST_CLICK = 10;
    public static final int PAY_BY_CASH_LIST_CLICK = 11;
    public static final int PAY_BY_PROMO_LIST_CLICK = 12;

    public static final int LOCATION_LIST_CLICK = 13;

    public static final int SEARCH_ADDRESS_FULL_LIST_CLICK = 14;

    public static final int SHOW_DRIVERS_ON_MAP_SETTINGS_NAV = 20;
    public static final int SEARCH_ADDRESS_LIST_CLICK_EVENT = 15;

    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
