package moleculus.isaevkonstantin.skytaxi.eventbus.events.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import moleculus.isaevkonstantin.skytaxi.eventbus.events.BaseEvent;

/**
 * Created by Isaev Konstantin on 02.05.16.
 */
public class FragmentEvent extends BaseEvent {

    public static final int SPLASH_FRAGMENT = 0;
    public static final int WALKTHROUGHT_FRAGMENT = 1;
    public static final int ENTER_DATA_TEXT_FRAGMENT = 2;
    public static final int CALL_FRIEND_FRAGMENT = 3;
    public static final int PROFILE_FRAGMENT = 4;
    public static final int MAP_FRAGMENT = 5;
    public static final int SEARCH_ADDRESS_FRAGMENT = 6;
    public static final int SEARCH_IN_FAVORITES_FRAGMENT = 7;
    public static final int SEARCH_IN_AIRPORTS_FRAGMENT = 8;
    public static final int LOAD_PHOTO_DIALOG = 9;
    public static final int TIME_PAYMENT_LIST_FRAGMENT = 10;
    public static final int TRIP_DETAIL_FRAGMENT = 11;
    public static final int TRIP_COST_FRAGMENT = 12;
    public static final int TRIP_SETTINGS_FRAGMENT = 13;
    public static final int TRIP_COMMENT_FRAGMENT = 14;
    public static final int PROMO_SUCCESS_DIALOG = 15;
    public static final int PROMO_FRAGMENT = 16;
    public static final int SET_CARD_FRAGMENT= 17;
    public static final int CANCEL_ORDER_DIALOG= 18;
    public static final int TRIP_SUCCESS= 19;

    private Bundle bundle;
    private boolean backStack;
    private boolean dialog;
    private Fragment owner;

    public Bundle getBundle() {
        return bundle;
    }

    public void setBundle(Bundle bundle) {
        this.bundle = bundle;
    }

    public boolean isBackStack() {
        return backStack;
    }

    public void setBackStack(boolean backStack) {
        this.backStack = backStack;
    }

    public boolean isDialog() {
        return dialog;
    }

    public void setDialog(boolean dialog) {
        this.dialog = dialog;
    }

    public Fragment getOwner() {
        return owner;
    }

    public void setOwner(Fragment owner) {
        this.owner = owner;
    }

    public static FragmentEvent newInstance(int fragmentId, boolean backStack, Bundle bundle){
        FragmentEvent fragmentEvent = new FragmentEvent();
        fragmentEvent.setId(fragmentId);
        fragmentEvent.setBackStack(backStack);
        fragmentEvent.setBundle(bundle);
        return fragmentEvent;
    }

    public static FragmentEvent newDialogInstance(int fragmentId, Fragment owner, Bundle bundle){
        FragmentEvent fragmentEvent = new FragmentEvent();
        fragmentEvent.setId(fragmentId);
        fragmentEvent.setDialog(true);
        fragmentEvent.setOwner(owner);
        fragmentEvent.setBundle(bundle);
        return fragmentEvent;
    }
}
