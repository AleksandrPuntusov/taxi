package moleculus.isaevkonstantin.skytaxi.eventbus.events.network;

/**
 * Created by Isaev Konstantin on 03.05.16.
 */
public class NetworkFailEvent {

    private int errorCode;
    private String message;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
