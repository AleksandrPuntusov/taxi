package moleculus.isaevkonstantin.skytaxi.eventbus.events.ui;

import moleculus.isaevkonstantin.skytaxi.eventbus.events.BaseEvent;

/**
 * Created by Isaev Konstantin on 11.05.16.
 */
public class MenuClickEvent extends BaseEvent {

    public static final int SAVE_LOCATION_MENU_CLICK = 0;
}
