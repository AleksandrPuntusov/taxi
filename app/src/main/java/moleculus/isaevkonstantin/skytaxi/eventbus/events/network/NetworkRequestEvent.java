package moleculus.isaevkonstantin.skytaxi.eventbus.events.network;

import moleculus.isaevkonstantin.skytaxi.eventbus.events.BaseEvent;

/**
 * Created by Isaev Konstantin on 19.04.16.
 */
public class NetworkRequestEvent<T> extends BaseEvent{



    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
