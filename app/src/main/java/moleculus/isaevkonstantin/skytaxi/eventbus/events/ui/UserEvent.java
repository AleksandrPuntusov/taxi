package moleculus.isaevkonstantin.skytaxi.eventbus.events.ui;

import moleculus.isaevkonstantin.skytaxi.eventbus.events.BaseEvent;

/**
 * Created by Isaev Konstantin on 12.04.16.
 */
public class UserEvent<T> extends BaseEvent{

    private T data;



    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
