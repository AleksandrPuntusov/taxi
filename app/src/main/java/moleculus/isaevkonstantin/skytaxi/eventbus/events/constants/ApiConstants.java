package moleculus.isaevkonstantin.skytaxi.eventbus.events.constants;

/**
 * Created by Isaev Konstantin on 19.04.16.
 */
public interface ApiConstants {

    int SEND_PHONE = 0;
    int CONFIRM_SMS_CODE = 1;
    int GET_USER = 2;
    int UPDATE_PROFILE_PHOTO = 3;
    int DELETE_PROFILE_PHOTO = 4;
    int UPDATE_USER = 5;
    int SEARCH_STREET = 6;
    int LOGOUT = 7;
    int GET_AIR_PORTS = 8;
    int GET_ORDER_COST = 9;
    int CREATE_ORDER = 10;
    int SEARCH_HOUSE_BY_STREET_ID = 11;
    int CANCEL_ORDER = 12;


}
