package moleculus.isaevkonstantin.skytaxi.eventbus;

import rx.Observable;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;

/**
 * @author  Isaev Konstantin
 * on 27.03.16.
 */
public class BusProvider {

    private static final Subject<Object, Object> eventBus = new SerializedSubject<>(PublishSubject.create());

    public static void send(Object o) {
        eventBus.onNext(o);
    }

    public static Observable<Object> observe() {
        return eventBus;
    }





}
