package moleculus.isaevkonstantin.skytaxi.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import moleculus.isaevkonstantin.skytaxi.database.DbAdapter;
import moleculus.isaevkonstantin.skytaxi.database.interfaces.CursorWrapper;
import moleculus.isaevkonstantin.skytaxi.database.tables.OrderOptionsTable;
import moleculus.isaevkonstantin.skytaxi.models.OrderOptions;

/**
 * Created by Isaev Konstantin on 26.05.16.
 */
public class OrderOptionsDao extends BaseDao<OrderOptions>{
    public OrderOptionsDao(Context context) {
        super(context);
        cursorWrapper = new OrderOptionsCursorWrapper();
    }

    @Override
    public List<OrderOptions> getAll() {
        return null;
    }

    @Override
    public long add(OrderOptions entity) {
        long id = -1;
        try {
            SQLiteDatabase db =DbAdapter.openDb();
            id = db.insert(OrderOptionsTable.ORDER_OPTIONS_TABLE, null, cursorWrapper.setContentValuesFromEntity(entity));
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
        return id;
    }

    @Override
    public OrderOptions getById(long id) {
        OrderOptions orderOptions = null;
        String query = "SELECT * FROM "+ OrderOptionsTable.ORDER_OPTIONS_TABLE+ " WHERE " + OrderOptionsTable.ORDER_OPTIONS_ID+
                "='"+ String.valueOf(id)+ "'";
        Cursor cursor = null;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                orderOptions =(OrderOptions) cursorWrapper.setEntityFromCursor(cursor);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (cursor != null) {
                cursor.close();
            }
            DbAdapter.closeDb();
        }
        return orderOptions;
    }

    @Override
    public boolean update(OrderOptions entity) {
        int id = 0;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            id=db.update(OrderOptionsTable.ORDER_OPTIONS_TABLE, cursorWrapper.setContentValuesFromEntity(entity),
                    OrderOptionsTable.ORDER_OPTIONS_ID+ "='" + entity.getId() + "'", null);
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
        return id>0;
    }

    @Override
    public boolean deleteById(long id) {
        return false;
    }

    @Override
    public void deleteAll() {
        SQLiteDatabase db = null;
        try {
            db = DbAdapter.openDb();
            db.delete(OrderOptionsTable.ORDER_OPTIONS_TABLE,null,null);
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
    }

    private static class OrderOptionsCursorWrapper implements CursorWrapper<OrderOptions>{
        @Override
        public ContentValues setContentValuesFromEntity(OrderOptions entity) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(OrderOptionsTable.ORDER_CAR_CLASS,entity.getCarClass());
            contentValues.put(OrderOptionsTable.ORDER_BAG_COUNTER,entity.getBagCount());
            contentValues.put(OrderOptionsTable.ORDER_AIR_BUS_NUMBER,entity.getFlightNumber());
            contentValues.put(OrderOptionsTable.ORDER_COMMENT,entity.getComment());
            contentValues.put(OrderOptionsTable.ORDER_NO_SMOKING_DRIVER,entity.isNoSmokingDriver()?1:0);
            return contentValues;
        }

        @Override
        public OrderOptions setEntityFromCursor(Cursor cursor) {
            OrderOptions orderOptions = new OrderOptions();
            orderOptions.setId(cursor.getLong(cursor.getColumnIndex(OrderOptionsTable.ORDER_OPTIONS_ID)));
            orderOptions.setCarClass(cursor.getInt(cursor.getColumnIndex(OrderOptionsTable.ORDER_CAR_CLASS)));
            orderOptions.setBagCount(cursor.getInt(cursor.getColumnIndex(OrderOptionsTable.ORDER_BAG_COUNTER)));
            orderOptions.setComment(cursor.getString(cursor.getColumnIndex(OrderOptionsTable.ORDER_COMMENT)));
            orderOptions.setFlightNumber(cursor.getString(cursor.getColumnIndex(OrderOptionsTable.ORDER_AIR_BUS_NUMBER)));
            orderOptions.setNoSmokingDriver(cursor.getInt(cursor.getColumnIndex(OrderOptionsTable.ORDER_NO_SMOKING_DRIVER))>0);
            return orderOptions;
        }
    }
}
