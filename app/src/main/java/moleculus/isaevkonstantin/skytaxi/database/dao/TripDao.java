package moleculus.isaevkonstantin.skytaxi.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import moleculus.isaevkonstantin.skytaxi.database.DbAdapter;
import moleculus.isaevkonstantin.skytaxi.database.interfaces.CursorWrapper;
import moleculus.isaevkonstantin.skytaxi.database.tables.LocationTable;
import moleculus.isaevkonstantin.skytaxi.database.tables.OrderOptionsTable;
import moleculus.isaevkonstantin.skytaxi.database.tables.TripTable;
import moleculus.isaevkonstantin.skytaxi.models.OrderOptions;
import moleculus.isaevkonstantin.skytaxi.models.Trip;

/**
 * Created by Isaev Konstantin on 25.05.16.
 */
public class TripDao extends BaseDao<Trip> {
    public TripDao(Context context) {
        super(context);
        cursorWrapper = new TripDaoHelper();
    }

    @Override
    public List<Trip> getAll() {
        return null;
    }

    public List<Trip> getAllByOrderFk(long orderId) {
        List<Trip> trips = null;
        Cursor cursor = null;
        String query = "SELECT * FROM "+ TripTable.TRIP_TABLE+ " WHERE " + TripTable.TRIP_ORDER_FK+
                "='"+ String.valueOf(orderId)+ "'";
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                trips = new ArrayList<>();
                do {
                    Trip trip =(Trip) cursorWrapper.setEntityFromCursor(cursor);
                    trips.add(trip);
                }while (cursor.moveToNext());
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (cursor != null) {
                cursor.close();
            }
            DbAdapter.closeDb();
        }
        return trips;
    }


    @Override
    public long add(Trip entity) {
        long id = -1;
        try {
            SQLiteDatabase db =DbAdapter.openDb();
            id = db.insert(TripTable.TRIP_TABLE, null, cursorWrapper.setContentValuesFromEntity(entity));
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
        return id;
    }

    @Override
    public Trip getById(long id) {
        Trip trip = null;
        String query = "SELECT * FROM "+ TripTable.TRIP_TABLE+ " WHERE " + TripTable.TRIP_ID+
                "='"+ String.valueOf(id)+ "'";
        Cursor cursor = null;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                trip =(Trip) cursorWrapper.setEntityFromCursor(cursor);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (cursor != null) {
                cursor.close();
            }
            DbAdapter.closeDb();
        }
        return trip;
    }

    public Trip getByIdAndOrderId(long id,long orderId) {
        Trip trip = null;
        String query = "SELECT * FROM "+ TripTable.TRIP_TABLE+ " WHERE " + TripTable.TRIP_ID+
                "='"+ String.valueOf(id)+ "'" + " AND "+ TripTable.TRIP_ORDER_FK+
                "='"+ String.valueOf(orderId)+ "'";
        Cursor cursor = null;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                trip =(Trip) cursorWrapper.setEntityFromCursor(cursor);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (cursor != null) {
                cursor.close();
            }
            DbAdapter.closeDb();
        }
        return trip;

    }

    @Override
    public boolean update(Trip entity) {
        int id = 0;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            id=db.update(TripTable.TRIP_TABLE, cursorWrapper.setContentValuesFromEntity(entity),
                    TripTable.TRIP_ID+ "='" + entity.getId() + "'", null);
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
        return id>0;
    }

    public boolean updateByOrderId(Trip entity){
        int id = 0;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            id=db.update(TripTable.TRIP_TABLE, cursorWrapper.setContentValuesFromEntity(entity),
                    TripTable.TRIP_ORDER_FK+ "='" + entity.getOrderId() + "'"
                    +" AND "+ TripTable.TRIP_NAME+ "='"+ entity.getName()+"'", null);
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
        return id>0;
    }

    @Override
    public boolean deleteById(long id) {
        SQLiteDatabase db = null;
        int row = 0;
        try {
            db = DbAdapter.openDb();
            row = db.delete(TripTable.TRIP_TABLE,TripTable.TRIP_ID+"=?",new String[]{Long.toString(id)});
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
        return row>0;
    }

    @Override
    public void deleteAll() {
        SQLiteDatabase db = null;
        try {
            db = DbAdapter.openDb();
            db.delete(TripTable.TRIP_TABLE,null,null);
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
    }

    private static class TripDaoHelper implements CursorWrapper<Trip>{

        @Override
        public ContentValues setContentValuesFromEntity(Trip entity) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(TripTable.TRIP_NAME,entity.getName());
            contentValues.put(TripTable.TRIP_ABOUT,entity.getAbout());
            contentValues.put(TripTable.TRIP_COST,entity.getCost());
            contentValues.put(TripTable.TRIP_DISTANCE,entity.getDistance());
            contentValues.put(TripTable.TRIP_SERVER_ID,entity.getServerId());
            contentValues.put(TripTable.TRIP_ORDER_FK,entity.getOrderId());
            contentValues.put(TripTable.TRIP_LOCATION_FROM_FK,entity.getLocationFromId());
            contentValues.put(TripTable.TRIP_LOCATION_TO_FK,entity.getLocationToId());
            return contentValues;
        }

        @Override
        public Trip setEntityFromCursor(Cursor cursor) {
            Trip trip = new Trip();
            trip.setId(cursor.getLong(cursor.getColumnIndex(TripTable.TRIP_ID)));
            trip.setName(cursor.getString(cursor.getColumnIndex(TripTable.TRIP_NAME)));
            trip.setAbout(cursor.getString(cursor.getColumnIndex(TripTable.TRIP_ABOUT)));
            trip.setCost(cursor.getInt(cursor.getColumnIndex(TripTable.TRIP_COST)));
            trip.setDistance(cursor.getInt(cursor.getColumnIndex(TripTable.TRIP_DISTANCE)));
            trip.setServerId(cursor.getString(cursor.getColumnIndex(TripTable.TRIP_SERVER_ID)));
            trip.setOrderId(cursor.getLong(cursor.getColumnIndex(TripTable.TRIP_ORDER_FK)));
            trip.setLocationFromId(cursor.getLong(cursor.getColumnIndex(TripTable.TRIP_LOCATION_FROM_FK)));
            trip.setLocationToId(cursor.getLong(cursor.getColumnIndex(TripTable.TRIP_LOCATION_TO_FK)));
            return trip;
        }
    }


}
