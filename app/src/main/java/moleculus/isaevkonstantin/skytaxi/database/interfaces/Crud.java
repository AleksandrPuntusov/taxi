package moleculus.isaevkonstantin.skytaxi.database.interfaces;

import java.util.List;

/**
 * Created by Isaev Konstantin on 03.04.16.
 */
public interface Crud<T> {

    long add(T entity);
    T getById(long id);
    List<T> getAll();
    boolean update(T entity);
    boolean deleteById(long id);
    void deleteAll();
}
