package moleculus.isaevkonstantin.skytaxi.database.tables;

/**
 * Created by Isaev Konstantin on 26.05.16.
 */
public interface TripTable {

    String TRIP_TABLE = "TripTable";

    String TRIP_ID = "TripId";
    String TRIP_NAME = "TripName";
    String TRIP_ABOUT = "TripAbout";
    String TRIP_DISTANCE = "TripDistance";
    String TRIP_COST= "TripCost";
    String TRIP_SERVER_ID= "TripServerId";
    String TRIP_LOCATION_FROM_FK= "TripLocationFromFk";
    String TRIP_LOCATION_TO_FK = "TripLocationToFk";
    String TRIP_ORDER_FK = "OrderFk";

    String CREATE_TRIP_TABLE = "CREATE TABLE " + TRIP_TABLE +"("+
            TRIP_ID + " INTEGER PRIMARY KEY,"+
            TRIP_NAME + " TEXT," +
            TRIP_ABOUT + " TEXT," +
            TRIP_DISTANCE + " INTEGER," +
            TRIP_COST + " INTEGER," +
            TRIP_SERVER_ID + " TEXT," +
            TRIP_LOCATION_FROM_FK + " INTEGER," +
            TRIP_LOCATION_TO_FK + " INTEGER," +
            TRIP_ORDER_FK + " INTEGER," +
            "FOREIGN KEY(" + TRIP_LOCATION_FROM_FK +") REFERENCES "+ LocationTable.LOCATION_TABLE+"("+LocationTable.LOCATION_ID+
            "),"+
            "FOREIGN KEY(" + TRIP_LOCATION_TO_FK +") REFERENCES "+ LocationTable.LOCATION_TABLE+"("+LocationTable.LOCATION_ID+
            "),"+
            "FOREIGN KEY(" + TRIP_ORDER_FK +") REFERENCES "+ OrderTable.ORDER_TABLE+"("+OrderTable.ORDER_ID+
            "))";


}
