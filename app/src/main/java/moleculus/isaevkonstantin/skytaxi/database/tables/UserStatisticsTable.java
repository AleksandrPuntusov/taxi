package moleculus.isaevkonstantin.skytaxi.database.tables;

/**
 * Created by Isaev Konstantin on 22.05.16.
 */
public interface UserStatisticsTable {

    String USER_STATISTICS_TABLE = "UserStatisticsTable";

    String USER_STATISTICS_ID = "UserStatisticsId";
    String TOTAL_DISTANCE = "TotalDistance";
    String CURRENT_MONTH_DISTANCE = "CurrentMonthDistance";
    String TOTAL_RIDES= "TotalRides";
    String CURRENT_MONTH_RIDES = "CurentMonthRides";

    String CREATE_USER_STATISTICS_TABLE = "CREATE TABLE "+ USER_STATISTICS_TABLE + "("
            + USER_STATISTICS_ID + " INTEGER PRIMARY KEY,"
            + TOTAL_DISTANCE + " INTEGER,"
            + CURRENT_MONTH_DISTANCE + " INTEGER,"
            + TOTAL_RIDES + " INTEGER,"
            + CURRENT_MONTH_RIDES + " INTEGER)";
}
