package moleculus.isaevkonstantin.skytaxi.database.tables;

/**
 * Created by Isaev Konstantin on 22.05.16.
 */
public interface PromoCodeTable {

    String PROMO_CODE_TABLE = "PromoCodeTable";

    String PROMO_CODE_ID = "PromoCodeId";
    String PROMO_CODE_SERVER_ID = "PromoCodeServerId";
    String PROMO_CODE_CODE = "PromoCodeCode";
    String PROMO_CODE_CREATED_AT = "PromoCodeCreatedAt";
    String PROMO_CODE_UPDATED_AT = "PromoCodeUpdatedAt";

    String CREATE_PROMO_CODE_TABLE = "CREATE TABLE "+ PROMO_CODE_TABLE + "("
            + PROMO_CODE_ID + " INTEGER PRIMARY KEY,"
            + PROMO_CODE_SERVER_ID + " TEXT,"
            + PROMO_CODE_CODE + " TEXT,"
            + PROMO_CODE_CREATED_AT + " TEXT,"
            + PROMO_CODE_UPDATED_AT + " TEXT)";
}
