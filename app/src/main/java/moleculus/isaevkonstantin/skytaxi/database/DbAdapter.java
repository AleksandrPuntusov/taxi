package moleculus.isaevkonstantin.skytaxi.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;


import com.pushtorefresh.storio.sqlite.SQLiteTypeMapping;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.impl.DefaultStorIOSQLite;

import moleculus.isaevkonstantin.skytaxi.database.tables.DriverTable;
import moleculus.isaevkonstantin.skytaxi.database.tables.LocationTable;
import moleculus.isaevkonstantin.skytaxi.database.tables.OrderOptionsTable;
import moleculus.isaevkonstantin.skytaxi.database.tables.OrderTable;
import moleculus.isaevkonstantin.skytaxi.database.tables.PromoCodeTable;
import moleculus.isaevkonstantin.skytaxi.database.tables.TripTable;
import moleculus.isaevkonstantin.skytaxi.database.tables.UserStatisticsTable;
import moleculus.isaevkonstantin.skytaxi.database.tables.UserTable;
import moleculus.isaevkonstantin.skytaxi.logger.Logger;
import moleculus.isaevkonstantin.skytaxi.models.OrderOptions;

/**
 * Created by Isaev Konstantin on 27.03.16.
 */
public class DbAdapter {

    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "SkyTaxiDb";
    private static Context context;
    private static DatabaseHelper databaseHelper;
    private static volatile int threadCount;

    public DbAdapter(Context context) {
        DbAdapter.context = context;
    }

    public static synchronized SQLiteDatabase openDb() {
        if (databaseHelper == null) {
            databaseHelper = new DatabaseHelper(context);
        }
        threadCount++;
        Logger.debug("DbAdapter", String.valueOf(threadCount));
        Logger.debug("DbAdapter", "open");

        return databaseHelper.getWritableDatabase();
    }

    public static synchronized void closeDb(){
        threadCount--;
        Logger.debug("DbAdapter", String.valueOf(threadCount)+"is opened");
        if(threadCount==0){
            databaseHelper.close();
            Logger.debug("DbAdapter", "close");

        }
    }



    private static class DatabaseHelper extends SQLiteOpenHelper{
        public DatabaseHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(UserTable.CREATE_USER_TABLE);
            db.execSQL(LocationTable.CREATE_LOCATION_TABLE);
            db.execSQL(OrderTable.CREATE_ORDER_TABLE);
            db.execSQL(UserStatisticsTable.CREATE_USER_STATISTICS_TABLE);
            db.execSQL(PromoCodeTable.CREATE_PROMO_CODE_TABLE);
            db.execSQL(OrderOptionsTable.CREATE_ORDER_OPTIONS_TABLE);
            db.execSQL(TripTable.CREATE_TRIP_TABLE);
            db.execSQL(DriverTable.CREATE_DRIVER_TABLE);

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + UserTable.USER_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + LocationTable.LOCATION_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + OrderTable.ORDER_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + UserStatisticsTable.USER_STATISTICS_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + PromoCodeTable.PROMO_CODE_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + OrderOptionsTable.ORDER_OPTIONS_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + TripTable.TRIP_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + DriverTable.DRIVER_TABLE);

        }
    }



}
