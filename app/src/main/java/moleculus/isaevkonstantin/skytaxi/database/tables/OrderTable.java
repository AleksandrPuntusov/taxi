package moleculus.isaevkonstantin.skytaxi.database.tables;

/**
 * Created by Isaev Konstantin on 11.05.16.
 */
public interface OrderTable {

    String ORDER_TABLE = "OrderTable";

    String ORDER_ID = "OrderId";
    String ORDER_SERVER_ID = "OrderServerId";
    String ORDER_RATE = "OrderRate";
    String ORDER_STATUS = "OrderStatus";
    String START_TIME_ID = "OrderStartTimeId";
    String ORDER_PAYMENT_ID = "OrderPaymentId";
    String CHECKED_TRIP_FK = "CheckeTripid";
    String ORDER_PRICE = "OrderPrice";
    String ORDER_DURATION = "OrderDuration";
    String ORDER_CURRENT = "OrderCurrent";
    String ORDER_LOCATION_FROM_FK = "OrderLocationFromId";
    String ORDER_LOCATION_TO_FK = "OrderLocationToId";
    String ORDER_OPTIONS_FK = "OrderOptionsFk";
    String DRIVER_FK = "DriverFk";
    String CREATE_ORDER_TABLE = "CREATE TABLE " + ORDER_TABLE +"("+
            ORDER_ID + " INTEGER PRIMARY KEY,"+
            ORDER_SERVER_ID + " TEXT," +
            ORDER_RATE + " INTEGER," +
            ORDER_STATUS+" INTEGER,"+
            START_TIME_ID+" INTEGER,"+
            ORDER_PAYMENT_ID+" INTEGER,"+
            CHECKED_TRIP_FK +" INTEGER,"+
            ORDER_PRICE+" INTEGER,"+
            ORDER_DURATION+" INTEGER,"+
            ORDER_CURRENT+"  INTEGER DEFAULT 0,"+
            ORDER_LOCATION_FROM_FK + " INTEGER,"+
            ORDER_LOCATION_TO_FK + " INTEGER,"+
            ORDER_OPTIONS_FK + " INTEGER,"+
            DRIVER_FK + " INTEGER,"+
            "FOREIGN KEY(" + ORDER_LOCATION_FROM_FK +") REFERENCES "+ LocationTable.LOCATION_TABLE+"("+LocationTable.LOCATION_ID+
            "),"+
            "FOREIGN KEY(" + DRIVER_FK +") REFERENCES "+ DriverTable.DRIVER_TABLE+"("+DriverTable.ID +
            "),"+
            "FOREIGN KEY(" + ORDER_OPTIONS_FK+") REFERENCES "+ OrderOptionsTable.ORDER_OPTIONS_TABLE+"("+OrderOptionsTable.ORDER_OPTIONS_ID+
            "),"+
            "FOREIGN KEY(" + CHECKED_TRIP_FK +") REFERENCES "+ TripTable.TRIP_TABLE+"("+TripTable.TRIP_ID+
            "),"+
//            "FOREIGN KEY(" + ORDER_TRIP_FK+") REFERENCES "+ LocationTable.LOCATION_TABLE+"("+LocationTable.LOCATION_ID+"),"+
            "FOREIGN KEY(" + ORDER_LOCATION_TO_FK +") REFERENCES "+ LocationTable.LOCATION_TABLE+"("+LocationTable.LOCATION_ID+"))";
}
