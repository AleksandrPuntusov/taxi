package moleculus.isaevkonstantin.skytaxi.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.google.gson.Gson;

import java.util.List;

import moleculus.isaevkonstantin.skytaxi.database.DbAdapter;
import moleculus.isaevkonstantin.skytaxi.database.interfaces.CursorWrapper;
import moleculus.isaevkonstantin.skytaxi.database.tables.DriverTable;
import moleculus.isaevkonstantin.skytaxi.models.Driver;

/**
 * Created by Isaev Konstantin on 28.05.16.
 */
public class DriverDao extends BaseDao<Driver> {
    public DriverDao(Context context) {
        super(context);
        cursorWrapper = new DriverDaoHelper();
    }

    @Override
    public List<Driver> getAll() {
        return null;
    }

    @Override
    public long add(Driver entity) {
        long id = -1;
        try {
            SQLiteDatabase db =DbAdapter.openDb();
            id = db.insert(DriverTable.DRIVER_TABLE, null, cursorWrapper.setContentValuesFromEntity(entity));
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
        return id;
    }

    @Override
    public Driver getById(long id) {
        Driver driver = null;
        String query = "SELECT * FROM "+ DriverTable.DRIVER_TABLE+ " WHERE " + DriverTable.ID +
                "='"+ String.valueOf(id)+ "'";
        Cursor cursor = null;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                driver =(Driver) cursorWrapper.setEntityFromCursor(cursor);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (cursor != null) {
                cursor.close();
            }
            DbAdapter.closeDb();
        }
        return driver;
    }

    @Override
    public boolean update(Driver entity) {
        int id = 0;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            id=db.update(DriverTable.DRIVER_TABLE, cursorWrapper.setContentValuesFromEntity(entity),
                    DriverTable.ID + "='" + entity.getId() + "'", null);
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
        return id>0;
    }

    @Override
    public boolean deleteById(long id) {
        return false;
    }

    @Override
    public void deleteAll() {
        SQLiteDatabase db = null;
        try {
            db = DbAdapter.openDb();
            db.delete(DriverTable.DRIVER_TABLE,null,null);
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
    }

    public boolean updateByServerId(Driver entity) {
        int id = 0;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            id=db.update(DriverTable.DRIVER_TABLE, cursorWrapper.setContentValuesFromEntity(entity),
                    DriverTable.SERVER_ID + "='" + entity.getServerId() + "'", null);
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
        return id>0;
    }

    public Driver getByServerId(String  id) {
        Driver driver = null;
        String query = "SELECT * FROM "+ DriverTable.DRIVER_TABLE+ " WHERE " + DriverTable.SERVER_ID +
                "='"+ String.valueOf(id)+ "'";
        Cursor cursor = null;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                driver =(Driver) cursorWrapper.setEntityFromCursor(cursor);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (cursor != null) {
                cursor.close();
            }
            DbAdapter.closeDb();
        }
        return driver;
    }

    private static class DriverDaoHelper implements CursorWrapper<Driver>{

        @Override
        public ContentValues setContentValuesFromEntity(Driver entity) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DriverTable.SERVER_ID,entity.getServerId());
            contentValues.put(DriverTable.NAME,entity.getName());
            contentValues.put(DriverTable.PHONE,entity.getPhone());
            contentValues.put(DriverTable.CAR,entity.getCar());
            contentValues.put(DriverTable.DRIVER_LICENSE,entity.getDriverLicense());
            contentValues.put(DriverTable.CAR_LICENSE,entity.getCarLicense());
            contentValues.put(DriverTable.RATING,entity.getDriverRaiting());
            contentValues.put(DriverTable.NUMBER,entity.getNumber());
            Gson gson = new Gson();
            String pointJson = gson.toJson(entity.getPoint());
            String serviceJson = gson.toJson(entity.getTaxiService());
            String cardetailsJson = gson.toJson(entity.getCarDetails());
            entity.setPointJson(pointJson);
            entity.setCarDetailsJson(cardetailsJson);
            entity.setTaxiServiceJson(serviceJson);
            contentValues.put(DriverTable.POINT_JSON,entity.getPointJson());
            contentValues.put(DriverTable.SERVICE_JSON,entity.getTaxiServiceJson());
            contentValues.put(DriverTable.CAR_DETAILS_JSON,entity.getCarDetailsJson());
            return contentValues;
        }

        @Override
        public Driver setEntityFromCursor(Cursor cursor) {
            Driver driver = new Driver();
            driver.setId(cursor.getLong(cursor.getColumnIndex(DriverTable.ID)));
            driver.setServerId(cursor.getString(cursor.getColumnIndex(DriverTable.SERVER_ID)));
            driver.setPhone(cursor.getString(cursor.getColumnIndex(DriverTable.PHONE)));
            driver.setName(cursor.getString(cursor.getColumnIndex(DriverTable.NAME)));
            driver.setNumber(cursor.getString(cursor.getColumnIndex(DriverTable.NUMBER)));
            driver.setDriverRaiting(cursor.getString(cursor.getColumnIndex(DriverTable.RATING)));
            driver.setDriverLicense(cursor.getString(cursor.getColumnIndex(DriverTable.DRIVER_LICENSE)));
            driver.setCar(cursor.getString(cursor.getColumnIndex(DriverTable.CAR)));
            driver.setCarLicense(cursor.getString(cursor.getColumnIndex(DriverTable.CAR_LICENSE)));
            String pointJson = cursor.getString(cursor.getColumnIndex(DriverTable.POINT_JSON));
            String serviceJson = cursor.getString(cursor.getColumnIndex(DriverTable.SERVICE_JSON));
            String carDetailsJson = cursor.getString(cursor.getColumnIndex(DriverTable.CAR_DETAILS_JSON));
            driver.setPointJson(pointJson);
            driver.setTaxiServiceJson(serviceJson);
            driver.setCarDetailsJson(carDetailsJson);
            Gson gson = new Gson();
            driver.setPoint(gson.fromJson(pointJson, Driver.Point.class));
            driver.setTaxiService(gson.fromJson(serviceJson, Driver.TaxiService.class));
            driver.setCarDetails(gson.fromJson(carDetailsJson, Driver.CarDetails.class));
            return driver;
        }
    }
}
