package moleculus.isaevkonstantin.skytaxi.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import moleculus.isaevkonstantin.skytaxi.database.DbAdapter;
import moleculus.isaevkonstantin.skytaxi.database.interfaces.CursorWrapper;
import moleculus.isaevkonstantin.skytaxi.database.tables.PromoCodeTable;
import moleculus.isaevkonstantin.skytaxi.models.PromoCode;

/**
 * Created by Isaev Konstantin on 22.05.16.
 */
public class PromoCodeDao extends BaseDao<PromoCode> {
    public PromoCodeDao(Context context) {
        super(context);
        cursorWrapper = new PromoCodeCursorWrapper();

    }

    @Override
    public List<PromoCode> getAll() {
        List<PromoCode> promoCodeList = null;
        String query = "SELECT * FROM "+ PromoCodeTable.PROMO_CODE_TABLE;
        Cursor cursor = null;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                promoCodeList = new ArrayList<>();
                do {
                    PromoCode promoCode=(PromoCode) cursorWrapper.setEntityFromCursor(cursor);
                    promoCodeList.add(promoCode);
                }while (cursor.moveToNext());
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (cursor != null) {
                cursor.close();
            }
            DbAdapter.closeDb();
        }
        return promoCodeList;
    }

    @Override
    public long add(PromoCode entity) {
        long id = -1;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            id = db.insert(PromoCodeTable.PROMO_CODE_TABLE, null, cursorWrapper.setContentValuesFromEntity(entity));
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
        return id;
    }

    @Override
    public PromoCode getById(long id) {
        PromoCode promoCode = null;
        String query = "SELECT * FROM "+ PromoCodeTable.PROMO_CODE_TABLE + " WHERE "+ PromoCodeTable.PROMO_CODE_ID
                + "='"+ String.valueOf(id)+"'";
        Cursor cursor = null;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                promoCode = (PromoCode) cursorWrapper.setEntityFromCursor(cursor);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (cursor != null) {
                cursor.close();
            }
            DbAdapter.closeDb();
        }
        return promoCode;
    }

    @Override
    public boolean update(PromoCode entity) {
        int id = 0;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            id=db.update(PromoCodeTable.PROMO_CODE_TABLE, cursorWrapper.setContentValuesFromEntity(entity),
                    PromoCodeTable.PROMO_CODE_ID+ "='" + entity.getId() + "'", null);
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
        return id>0;
    }

    @Override
    public boolean deleteById(long id) {
        return false;
    }

    @Override
    public void deleteAll() {
        SQLiteDatabase db = null;
        try {
            db = DbAdapter.openDb();
            db.delete(PromoCodeTable.PROMO_CODE_TABLE,null,null);
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
    }

    private static class PromoCodeCursorWrapper implements CursorWrapper<PromoCode> {
        @Override
        public ContentValues setContentValuesFromEntity(PromoCode entity) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(PromoCodeTable.PROMO_CODE_SERVER_ID,entity.getServerId());
            contentValues.put(PromoCodeTable.PROMO_CODE_CODE,entity.getCode());
            contentValues.put(PromoCodeTable.PROMO_CODE_CREATED_AT,entity.getCreatedAt());
            contentValues.put(PromoCodeTable.PROMO_CODE_UPDATED_AT,entity.getUpdatedAt());
            return contentValues;
        }

        @Override
        public PromoCode setEntityFromCursor(Cursor cursor) {
            PromoCode promoCode = new PromoCode();
            promoCode.setId(cursor.getLong(cursor.getColumnIndex(PromoCodeTable.PROMO_CODE_ID)));
            promoCode.setServerId(cursor.getString(cursor.getColumnIndex(PromoCodeTable.PROMO_CODE_SERVER_ID)));
            promoCode.setCode(cursor.getString(cursor.getColumnIndex(PromoCodeTable.PROMO_CODE_CODE)));
            promoCode.setCreatedAt(cursor.getString(cursor.getColumnIndex(PromoCodeTable.PROMO_CODE_CREATED_AT)));
            promoCode.setUpdatedAt(cursor.getString(cursor.getColumnIndex(PromoCodeTable.PROMO_CODE_UPDATED_AT)));
            return promoCode;
        }
    }
}
