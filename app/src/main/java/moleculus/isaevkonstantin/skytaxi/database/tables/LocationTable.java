package moleculus.isaevkonstantin.skytaxi.database.tables;

/**
 * Created by Isaev Konstantin on 11.05.16.
 */
public interface LocationTable {

    String LOCATION_TABLE = "LocationTable";

    String LOCATION_ID = "LocationId";
    String LOCATION_SERVER_ID = "LocationServerId";
    String LOCATION_NAME = "LocationName";
    String LOCATION_ENTRANCE = "LocationEntrance";
    String LOCATION_FAVORITE = "LocationFavorite";
    String LOCATION_STATION = "LocationStation";
    String LOCATION_PATH = "LocationPath";
    String LOCATION_STATION_ID = "LocationStationId";
    String LOCATION_LATITUDE = "LocationLatitude";
    String LOCATION_LONGITUDE = "LocationLongitude";

    String CREATE_LOCATION_TABLE = "CREATE TABLE " + LOCATION_TABLE + "("+
            LOCATION_ID + " INTEGER PRIMARY KEY,"+
            LOCATION_SERVER_ID + " TEXT,"+
            LOCATION_NAME + " TEXT,"+
            LOCATION_ENTRANCE + " TEXT,"+
            LOCATION_FAVORITE + " INTEGER DEFAULT 0,"+
            LOCATION_STATION + " INTEGER DEFAULT 0,"+
            LOCATION_PATH + " INTEGER,"+
            LOCATION_STATION_ID + " INTEGER,"+
            LOCATION_LATITUDE+ " REAL,"+
            LOCATION_LONGITUDE + " REAL)";

}
