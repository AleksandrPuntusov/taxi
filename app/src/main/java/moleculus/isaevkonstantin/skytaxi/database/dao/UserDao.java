package moleculus.isaevkonstantin.skytaxi.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import moleculus.isaevkonstantin.skytaxi.database.DbAdapter;
import moleculus.isaevkonstantin.skytaxi.database.interfaces.CursorWrapper;
import moleculus.isaevkonstantin.skytaxi.database.tables.UserTable;
import moleculus.isaevkonstantin.skytaxi.models.User;

/**
 * Created by Isaev Konstantin on 12.04.16.
 */
public class UserDao extends BaseDao<User>{

    public UserDao(Context context) {
        super(context);
        cursorWrapper = new UserCursorWrapper();
    }



    @Override
    public List<User> getAll() {
        List<User> userList = null;
        String query = "SELECT * FROM "+ UserTable.USER_TABLE;
        Cursor cursor = null;
        try {
            SQLiteDatabase db = dbAdapter.openDb();
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                userList = new ArrayList<>();
                do {
                    User user =(User) cursorWrapper.setEntityFromCursor(cursor);
                    userList.add(user);
                }while (cursor.moveToNext());
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (cursor != null) {
                cursor.close();
            }
            DbAdapter.closeDb();
        }
        return userList;
    }

    public User getByPhoneNumber(String phoneNumber) {
        User user = null;
        String query = "SELECT * FROM "+ UserTable.USER_TABLE + " WHERE " + UserTable.USER_PHONE_NUMBER +
                "='"+ phoneNumber+"'";
        Cursor cursor = null;
        try {
            SQLiteDatabase db = dbAdapter.openDb();
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                user =(User) cursorWrapper.setEntityFromCursor(cursor);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (cursor != null) {
                cursor.close();
            }
            DbAdapter.closeDb();
        }
        return user;
    }

    @Override
    public long add(User entity) {
        long id = -1;
        try {
            SQLiteDatabase db =DbAdapter.openDb();
            id = db.insert(UserTable.USER_TABLE, null, cursorWrapper.setContentValuesFromEntity(entity));
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
        return id;
    }

    @Override
    public User getById(long id) {
        return null;
    }

    @Override
    public boolean update(User entity) {
        int id = 0;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            id=db.update(UserTable.USER_TABLE, cursorWrapper.setContentValuesFromEntity(entity),
                    UserTable.USER_ID+ "='" + entity.getId() + "'", null);
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
        return id>0;
    }

    public boolean updateByServerId(User entity) {
        int id = 0;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            id=db.update(UserTable.USER_TABLE, cursorWrapper.setContentValuesFromEntity(entity),
                    UserTable.USER_SERVER_ID+ "='" + entity.getServerId() + "'", null);
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
        return id>0;
    }



    @Override
    public boolean deleteById(long id) {
        return false;
    }

    @Override
    public void deleteAll() {
        SQLiteDatabase db = null;
        try {
            db = DbAdapter.openDb();
            db.delete(UserTable.USER_TABLE,null,null);
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
    }

    private static class UserCursorWrapper implements CursorWrapper<User> {
        @Override
        public ContentValues setContentValuesFromEntity(User entity) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(UserTable.USER_SERVER_ID,entity.getServerId());
            contentValues.put(UserTable.USER_FIRST_NAME,entity.getFirstName());
            contentValues.put(UserTable.USER_LAST_NAME,entity.getLastName());
            contentValues.put(UserTable.USER_MIDDLE_NAME,entity.getMiddleName());
            contentValues.put(UserTable.USER_PHONE_NUMBER,entity.getPhoneNumber());
            contentValues.put(UserTable.USER_EMAIL,entity.getEmail());
            contentValues.put(UserTable.USER_PHOTO_URL,entity.getPhotoUrl());
            contentValues.put(UserTable.USER_BONUSES,entity.getBonuses());
            contentValues.put(UserTable.USER_RIDES_COUNT,entity.getRidesCount());
            contentValues.put(UserTable.USER_AUTH_TOKEN,entity.getAuthToken());
            contentValues.put(UserTable.USER_STATISTICS_FK,entity.getUserStatisticsFk());
            contentValues.put(UserTable.PROMO_CODE_FK,entity.getPromoCodeFk());
            return contentValues;
        }

        @Override
        public User setEntityFromCursor(Cursor cursor) {
            User user = new User();
            user.setId(cursor.getInt(cursor.getColumnIndex(UserTable.USER_ID)));
            user.setServerId(cursor.getString(cursor.getColumnIndex(UserTable.USER_SERVER_ID)));
            user.setFirstName(cursor.getString(cursor.getColumnIndex(UserTable.USER_FIRST_NAME)));
            user.setLastName(cursor.getString(cursor.getColumnIndex(UserTable.USER_LAST_NAME)));
            user.setMiddleName(cursor.getString(cursor.getColumnIndex(UserTable.USER_MIDDLE_NAME)));
            user.setPhoneNumber(cursor.getString(cursor.getColumnIndex(UserTable.USER_PHONE_NUMBER)));
            user.setEmail(cursor.getString(cursor.getColumnIndex(UserTable.USER_EMAIL)));
            user.setPhotoUrl(cursor.getString(cursor.getColumnIndex(UserTable.USER_PHOTO_URL)));
            user.setBonuses(cursor.getInt(cursor.getColumnIndex(UserTable.USER_BONUSES)));
            user.setRidesCount(cursor.getInt(cursor.getColumnIndex(UserTable.USER_RIDES_COUNT)));
            user.setAuthToken(cursor.getString(cursor.getColumnIndex(UserTable.USER_AUTH_TOKEN)));
            user.setUserStatisticsFk(cursor.getLong(cursor.getColumnIndex(UserTable.USER_STATISTICS_FK)));
            user.setPromoCodeFk(cursor.getLong(cursor.getColumnIndex(UserTable.PROMO_CODE_FK)));
            return user;
        }
    }
}
