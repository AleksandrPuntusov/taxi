package moleculus.isaevkonstantin.skytaxi.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import moleculus.isaevkonstantin.skytaxi.database.DbAdapter;
import moleculus.isaevkonstantin.skytaxi.database.interfaces.CursorWrapper;
import moleculus.isaevkonstantin.skytaxi.database.tables.PromoCodeTable;
import moleculus.isaevkonstantin.skytaxi.database.tables.UserStatisticsTable;
import moleculus.isaevkonstantin.skytaxi.models.UserStatistics;

/**
 * Created by Isaev Konstantin on 22.05.16.
 */
public class UserStatisticsDao extends BaseDao<UserStatistics> {

    public UserStatisticsDao(Context context) {
        super(context);
        cursorWrapper = new PromoCodeCursorWrapper();
    }



    @Override
    public List<UserStatistics> getAll() {
        List<UserStatistics> userStatisticsList = null;
        String query = "SELECT * FROM "+ UserStatisticsTable.USER_STATISTICS_TABLE;
        Cursor cursor = null;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                userStatisticsList = new ArrayList<>();
                do {
                    UserStatistics userStatistics=(UserStatistics) cursorWrapper.setEntityFromCursor(cursor);
                    userStatisticsList.add(userStatistics);
                }while (cursor.moveToNext());
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (cursor != null) {
                cursor.close();
            }
            DbAdapter.closeDb();
        }
        return userStatisticsList;
    }

    @Override
    public long add(UserStatistics entity) {
        long id = -1;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            id = db.insert(UserStatisticsTable.USER_STATISTICS_TABLE, null, cursorWrapper.setContentValuesFromEntity(entity));
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
        return id;
    }

    @Override
    public UserStatistics getById(long id) {
        UserStatistics userStatistics = null;
        String query = "SELECT * FROM "+ UserStatisticsTable.USER_STATISTICS_TABLE + " WHERE "+ UserStatisticsTable.USER_STATISTICS_ID
                + "='"+ String.valueOf(id)+ "'";
        Cursor cursor = null;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                    userStatistics=(UserStatistics) cursorWrapper.setEntityFromCursor(cursor);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (cursor != null) {
                cursor.close();
            }
            DbAdapter.closeDb();
        }
        return userStatistics;
    }

    @Override
    public boolean update(UserStatistics entity) {
        int id = 0;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            id=db.update(UserStatisticsTable.USER_STATISTICS_TABLE, cursorWrapper.setContentValuesFromEntity(entity),
                    UserStatisticsTable.USER_STATISTICS_ID+ "='" + entity.getId() + "'", null);
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
        return id>0;
    }

    @Override
    public boolean deleteById(long id) {
        return false;
    }

    @Override
    public void deleteAll() {
        SQLiteDatabase db = null;
        try {
            db = DbAdapter.openDb();
            db.delete(UserStatisticsTable.USER_STATISTICS_TABLE,null,null);
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
    }

    private static class PromoCodeCursorWrapper implements CursorWrapper<UserStatistics> {
        @Override
        public ContentValues setContentValuesFromEntity(UserStatistics entity) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(UserStatisticsTable.TOTAL_DISTANCE,entity.getDistanceTotal());
            contentValues.put(UserStatisticsTable.CURRENT_MONTH_DISTANCE,entity.getDistanceCurrentMonth());
            contentValues.put(UserStatisticsTable.TOTAL_RIDES,entity.getRidesTotal());
            contentValues.put(UserStatisticsTable.CURRENT_MONTH_RIDES,entity.getRidesCurrentMonth());
            return contentValues;
        }

        @Override
        public UserStatistics setEntityFromCursor(Cursor cursor) {
            UserStatistics userStatistics = new UserStatistics();
            userStatistics.setId(cursor.getLong(cursor.getColumnIndex(UserStatisticsTable.USER_STATISTICS_ID)));
            userStatistics.setDistanceTotal(cursor.getInt(cursor.getColumnIndex(UserStatisticsTable.TOTAL_DISTANCE)));
            userStatistics.setDistanceCurrentMonth(cursor.getInt(cursor.getColumnIndex(UserStatisticsTable.CURRENT_MONTH_DISTANCE)));
            userStatistics.setRidesTotal(cursor.getInt(cursor.getColumnIndex(UserStatisticsTable.TOTAL_RIDES)));
            userStatistics.setRidesCurrentMonth(cursor.getInt(cursor.getColumnIndex(UserStatisticsTable.CURRENT_MONTH_RIDES)));
            return userStatistics;
        }
    }
}
