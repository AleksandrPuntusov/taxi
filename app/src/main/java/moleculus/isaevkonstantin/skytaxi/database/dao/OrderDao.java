package moleculus.isaevkonstantin.skytaxi.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import moleculus.isaevkonstantin.skytaxi.database.DbAdapter;
import moleculus.isaevkonstantin.skytaxi.database.interfaces.CursorWrapper;
import moleculus.isaevkonstantin.skytaxi.database.tables.OrderTable;
import moleculus.isaevkonstantin.skytaxi.models.Order;

/**
 * Created by Isaev Konstantin on 11.05.16.
 */
public class OrderDao extends BaseDao<Order> {
    public OrderDao(Context context) {
        super(context);
        cursorWrapper = new OrderCursorWrapper();
    }


    @Override
    public List<Order> getAll() {
        List<Order> orderList = null;
        String query = "SELECT * FROM "+ OrderTable.ORDER_TABLE;
        Cursor cursor = null;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                orderList = new ArrayList<>();
                do {
                    Order order =(Order) cursorWrapper.setEntityFromCursor(cursor);
                    orderList.add(order);
                }while (cursor.moveToNext());
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (cursor != null) {
                cursor.close();
            }
            DbAdapter.closeDb();
        }
        return orderList;
    }

    @Override
    public long add(Order entity) {
        long id = -1;
        try {
            SQLiteDatabase db =DbAdapter.openDb();
            id = db.insert(OrderTable.ORDER_TABLE, null, cursorWrapper.setContentValuesFromEntity(entity));
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
        return id;
    }

    @Override
    public Order getById(long id) {
        return null;
    }

    @Override
    public boolean update(Order entity) {
        int id = 0;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            id=db.update(OrderTable.ORDER_TABLE, cursorWrapper.setContentValuesFromEntity(entity),
                    OrderTable.ORDER_ID+ "='" + entity.getId() + "'", null);
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
        return id>0;
    }

    @Override
    public boolean deleteById(long id) {

        return false;
    }

    @Override
    public void deleteAll() {
        SQLiteDatabase db = null;
        try {
            db = DbAdapter.openDb();
            db.delete(OrderTable.ORDER_TABLE,null,null);
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
    }

    public Order getCurrentOrder(){
        Order order = null;
        String query = "SELECT * FROM "+ OrderTable.ORDER_TABLE + " WHERE " + OrderTable.ORDER_CURRENT +
                "='1'";
        Cursor cursor = null;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                order =(Order) cursorWrapper.setEntityFromCursor(cursor);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (cursor != null) {
                cursor.close();
            }
            DbAdapter.closeDb();
        }
        return order;
    }

    public Order getOrderByServerId(String orderServerId){
        Order order = null;
        String query = "SELECT * FROM "+ OrderTable.ORDER_TABLE + " WHERE " + OrderTable.ORDER_SERVER_ID +
                "='"+ orderServerId+"'" ;
        Cursor cursor = null;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                order =(Order) cursorWrapper.setEntityFromCursor(cursor);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (cursor != null) {
                cursor.close();
            }
            DbAdapter.closeDb();
        }
        return order;
    }
    private static class OrderCursorWrapper implements CursorWrapper<Order> {
        @Override
        public ContentValues setContentValuesFromEntity(Order entity) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(OrderTable.ORDER_SERVER_ID,entity.getServerId());
            contentValues.put(OrderTable.ORDER_RATE,entity.getRate());
            contentValues.put(OrderTable.ORDER_STATUS,entity.getStatus());
            contentValues.put(OrderTable.ORDER_LOCATION_FROM_FK,entity.getLocationFromId());
            contentValues.put(OrderTable.ORDER_LOCATION_TO_FK,entity.getLocationToId());
            contentValues.put(OrderTable.DRIVER_FK,entity.getDriverFk());
            contentValues.put(OrderTable.ORDER_DURATION,entity.getDuration());
            contentValues.put(OrderTable.ORDER_PRICE,entity.getPrice());
            contentValues.put(OrderTable.ORDER_OPTIONS_FK,entity.getOrderOptionFk());
            contentValues.put(OrderTable.START_TIME_ID,entity.getTimeId());
            contentValues.put(OrderTable.ORDER_PAYMENT_ID,entity.getPaymentId());
            contentValues.put(OrderTable.CHECKED_TRIP_FK,entity.getCheckedTripId());
            contentValues.put(OrderTable.ORDER_CURRENT,entity.isCurrentOrder()?1:0);
            return contentValues;
        }

        @Override
        public Order setEntityFromCursor(Cursor cursor) {
            Order order = new Order();
            order.setId(cursor.getLong(cursor.getColumnIndex(OrderTable.ORDER_ID)));
            order.setLocationFromId(cursor.getLong(cursor.getColumnIndex(OrderTable.ORDER_LOCATION_FROM_FK)));
            order.setLocationToId(cursor.getLong(cursor.getColumnIndex(OrderTable.ORDER_LOCATION_TO_FK)));
            order.setCurrentOrder(cursor.getInt(cursor.getColumnIndex(OrderTable.ORDER_CURRENT))>0);
            order.setServerId(cursor.getString(cursor.getColumnIndex(OrderTable.ORDER_SERVER_ID)));
            order.setRate(cursor.getInt(cursor.getColumnIndex(OrderTable.ORDER_RATE)));
            order.setStatus(cursor.getInt(cursor.getColumnIndex(OrderTable.ORDER_STATUS)));
            order.setTimeId(cursor.getInt(cursor.getColumnIndex(OrderTable.START_TIME_ID)));
            order.setPaymentId(cursor.getInt(cursor.getColumnIndex(OrderTable.ORDER_PAYMENT_ID)));
            order.setDuration(cursor.getInt(cursor.getColumnIndex(OrderTable.ORDER_DURATION)));
            order.setPrice(cursor.getInt(cursor.getColumnIndex(OrderTable.ORDER_PRICE)));
            order.setCheckedTripId(cursor.getInt(cursor.getColumnIndex(OrderTable.CHECKED_TRIP_FK)));
            order.setOrderOptionFk(cursor.getInt(cursor.getColumnIndex(OrderTable.ORDER_OPTIONS_FK)));
            order.setDriverFk(cursor.getInt(cursor.getColumnIndex(OrderTable.DRIVER_FK)));
            return order;
        }
    }
}
