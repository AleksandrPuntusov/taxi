package moleculus.isaevkonstantin.skytaxi.database.tables;

/**
 * Created by Isaev Konstantin on 28.05.16.
 */
public interface DriverTable {

    String DRIVER_TABLE = "DriverTable";

    String ID = "DriverId";
    String SERVER_ID = "DriverServerId";
    String NAME = "DriverName";
    String PHONE = "DriverPhone";
    String CAR = "DriverCar";
    String DRIVER_LICENSE = "DriverLicense";
    String NUMBER = "DriverNumber";
    String CAR_LICENSE = "DriverCarLicense";
    String RATING = "DriverRating";
    String POINT_JSON = "DriverPoint";
    String SERVICE_JSON = "DriverServiceJson";
    String CAR_DETAILS_JSON = "DriverCarDetailsJson";

    String CREATE_DRIVER_TABLE = "CREATE TABLE " + DRIVER_TABLE +"("+
            ID + " INTEGER PRIMARY KEY,"+
            SERVER_ID + " INTEGER,"+
            NAME + " TEXT," +
            PHONE + " TEXT," +
            CAR + " TEXT," +
            DRIVER_LICENSE + " REAL," +
            NUMBER + " REAL," +
            CAR_LICENSE + " TEXT," +
            RATING + " TEXT," +
            POINT_JSON + " TEXT," +
            SERVICE_JSON + " TEXT," +
            CAR_DETAILS_JSON + " TEXT)";

}
