package moleculus.isaevkonstantin.skytaxi.database.interfaces;

import android.content.ContentValues;
import android.database.Cursor;

/**
 * Created by Isaev Konstantin on 25.12.15.
 */
public interface CursorWrapper<T> {

    ContentValues setContentValuesFromEntity(T entity);
    T setEntityFromCursor(Cursor cursor);
}
