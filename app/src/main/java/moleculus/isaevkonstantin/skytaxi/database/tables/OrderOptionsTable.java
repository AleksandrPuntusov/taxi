package moleculus.isaevkonstantin.skytaxi.database.tables;

/**
 * Created by Isaev Konstantin on 26.05.16.
 */
public interface OrderOptionsTable {

    String ORDER_OPTIONS_TABLE = "OrderOptionsTable";

    String ORDER_OPTIONS_ID = "OrderOptionsId";
    String ORDER_COMMENT = "OrderOptionComment";
    String ORDER_BAG_COUNTER = "OrderOptionsBagCounter";
    String ORDER_AIR_BUS_NUMBER = "OrderOptionsAirBusNumber";
    String ORDER_CAR_CLASS = "OrderOptionsCarClass";
    String ORDER_NO_SMOKING_DRIVER = "OrderOptionsNoSmokingDriver";

    String CREATE_ORDER_OPTIONS_TABLE = "CREATE TABLE " + ORDER_OPTIONS_TABLE +"("+
            ORDER_OPTIONS_ID + " INTEGER PRIMARY KEY,"+
            ORDER_AIR_BUS_NUMBER + " TEXT," +
            ORDER_COMMENT + " TEXT," +
            ORDER_CAR_CLASS+" INTEGER,"+
            ORDER_BAG_COUNTER+" INTEGER,"+
            ORDER_NO_SMOKING_DRIVER+"  INTEGER DEFAULT 0)";

}
