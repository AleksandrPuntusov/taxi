package moleculus.isaevkonstantin.skytaxi.database.dao;

import android.content.Context;


import java.util.List;

import moleculus.isaevkonstantin.skytaxi.database.DbAdapter;
import moleculus.isaevkonstantin.skytaxi.database.interfaces.Crud;
import moleculus.isaevkonstantin.skytaxi.database.interfaces.CursorWrapper;

/**
 * Created by Isaev Konstantin
 * on 12.08.15.
 */
public abstract class BaseDao<T> implements Crud<T> {
    protected DbAdapter dbAdapter;
    protected CursorWrapper cursorWrapper;
    public BaseDao(Context context) {
        dbAdapter = new DbAdapter(context);
    }

    public abstract List<T> getAll();

    public abstract  long add(T entity);

    public abstract T getById(long id);

    public abstract boolean update(T entity);

    public abstract boolean deleteById(long id);

    public abstract void deleteAll();

}
