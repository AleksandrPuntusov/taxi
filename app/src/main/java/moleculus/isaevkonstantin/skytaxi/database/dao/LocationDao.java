package moleculus.isaevkonstantin.skytaxi.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import moleculus.isaevkonstantin.skytaxi.database.DbAdapter;
import moleculus.isaevkonstantin.skytaxi.database.interfaces.CursorWrapper;
import moleculus.isaevkonstantin.skytaxi.database.tables.LocationTable;
import moleculus.isaevkonstantin.skytaxi.models.Location;

/**
 * Created by Isaev Konstantin on 11.05.16.
 */
public class LocationDao extends BaseDao<Location> {
    public LocationDao(Context context) {
        super(context);
        cursorWrapper = new LocationCursorWrapper();
    }



    @Override
    public List<Location> getAll() {
        String query = "SELECT * FROM "+ LocationTable.LOCATION_TABLE;
        return getLocationList(query);
    }

    @Override
    public long add(Location entity) {
        long id = -1;
        try {
            SQLiteDatabase db =DbAdapter.openDb();
            id = db.insert(LocationTable.LOCATION_TABLE, null, cursorWrapper.setContentValuesFromEntity(entity));
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
        return id;
    }

    @Override
    public Location getById(long id) {
        Location location = null;
        String query = "SELECT * FROM "+ LocationTable.LOCATION_TABLE+ " WHERE " + LocationTable.LOCATION_ID+
                "='"+ String.valueOf(id)+ "'";
        Cursor cursor = null;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                location =(Location) cursorWrapper.setEntityFromCursor(cursor);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (cursor != null) {
                cursor.close();
            }
            DbAdapter.closeDb();
        }
        return location;
    }

    @Override
    public boolean update(Location entity) {
        int id = -1;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            id=db.update(LocationTable.LOCATION_TABLE, cursorWrapper.setContentValuesFromEntity(entity),
                    LocationTable.LOCATION_ID+ "='" + entity.getId() + "'", null);
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
        return id>0;
    }

    @Override
    public boolean deleteById(long id) {
        return false;
    }

    @Override
    public void deleteAll() {
        SQLiteDatabase db = null;
        try {
            db = DbAdapter.openDb();
            db.delete(LocationTable.LOCATION_TABLE,null,null);
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbAdapter.closeDb();
        }
    }

    public List<Location> getAllFavorites() {
        String query = "SELECT * FROM "+ LocationTable.LOCATION_TABLE+ " WHERE " + LocationTable.LOCATION_FAVORITE+
                "='0'";
        return getLocationList(query);
    }

    public List<Location> getAllNotFavorites() {
        String query = "SELECT * FROM "+ LocationTable.LOCATION_TABLE+ " WHERE " + LocationTable.LOCATION_FAVORITE+
                "='0'";
        return getLocationList(query);
    }

    public List<Location> getAllByTitle(String title){
        String query = "SELECT * FROM "+ LocationTable.LOCATION_TABLE+ " WHERE " + LocationTable.LOCATION_NAME+
                "='"+ title+ "'";
        return getLocationList(query);
    }

    public List<Location> getAllStations(){
        String query = "SELECT * FROM "+ LocationTable.LOCATION_TABLE+ " WHERE " + LocationTable.LOCATION_STATION+
                "='1'";
        return getLocationList(query);
    }

    public Location getAllStationsByName(String name){
        String query = "SELECT * FROM "+ LocationTable.LOCATION_TABLE+ " WHERE " + LocationTable.LOCATION_STATION+
                "='1' AND " + LocationTable.LOCATION_NAME + "='"+ name + "'";
        List <Location> locationList = getLocationList(query);
        return locationList!=null?locationList.get(0):null;
    }

    public List<Location> getAllStationsByType(int type){
        String query = "SELECT * FROM "+ LocationTable.LOCATION_TABLE+ " WHERE " + LocationTable.LOCATION_STATION+
                "='1' AND " + LocationTable.LOCATION_STATION_ID + "='"+ type + "'";
        return getLocationList(query);
    }

    public List<Location> getAllByPath(int pathId) {
        String query = "SELECT * FROM "+ LocationTable.LOCATION_TABLE+ " WHERE " + LocationTable.LOCATION_PATH+
                "='"+ String.valueOf(pathId)+ "'";
        return getLocationList(query);
    }


    private List<Location> getLocationList(String query){
        List<Location> locationList = null;
        Cursor cursor = null;
        try {
            SQLiteDatabase db = DbAdapter.openDb();
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                locationList = new ArrayList<>();
                do {
                    Location location =(Location) cursorWrapper.setEntityFromCursor(cursor);
                    locationList.add(location);
                }while (cursor.moveToNext());
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (cursor != null) {
                cursor.close();
            }
            DbAdapter.closeDb();
        }
        return locationList;
    }

    private static class LocationCursorWrapper implements CursorWrapper<Location> {
        @Override
        public ContentValues setContentValuesFromEntity(Location entity) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(LocationTable.LOCATION_SERVER_ID,entity.getServerId());
            contentValues.put(LocationTable.LOCATION_NAME,entity.getName());
            contentValues.put(LocationTable.LOCATION_ENTRANCE,entity.getEntrance());
            contentValues.put(LocationTable.LOCATION_FAVORITE,entity.isFavorite());
            contentValues.put(LocationTable.LOCATION_STATION,entity.isStation());
            contentValues.put(LocationTable.LOCATION_PATH,entity.getPathId());
            contentValues.put(LocationTable.LOCATION_STATION_ID,entity.getStationId());
            contentValues.put(LocationTable.LOCATION_LATITUDE,entity.getLatitude());
            contentValues.put(LocationTable.LOCATION_LONGITUDE,entity.getLongitude());
            return contentValues;
        }

        @Override
        public Location setEntityFromCursor(Cursor cursor) {
            Location location = Location.createLocation(
                    cursor.getString(cursor.getColumnIndex(LocationTable.LOCATION_NAME)),
                    Double.valueOf(cursor.getString(cursor.getColumnIndex(LocationTable.LOCATION_LATITUDE))),
                    Double.valueOf(cursor.getString(cursor.getColumnIndex(LocationTable.LOCATION_LONGITUDE)))
                    );
            location.setId(cursor.getLong(cursor.getColumnIndex(LocationTable.LOCATION_ID)));
            location.setServerId(cursor.getString(cursor.getColumnIndex(LocationTable.LOCATION_SERVER_ID)));
            location.setFavorite(cursor.getInt(cursor.getColumnIndex(LocationTable.LOCATION_FAVORITE))>0);
            location.setStation(cursor.getInt(cursor.getColumnIndex(LocationTable.LOCATION_STATION))>0);
            location.setPathId(cursor.getInt(cursor.getColumnIndex(LocationTable.LOCATION_PATH)));
            location.setStationId(cursor.getInt(cursor.getColumnIndex(LocationTable.LOCATION_STATION_ID)));
            return location;
        }
    }
}
