package moleculus.isaevkonstantin.skytaxi.database.tables;

/**
 * Created by Isaev Konstantin on 12.04.16.
 */
public interface UserTable {

    String USER_TABLE = "UserTable";

    String USER_ID = "UserId";
    String USER_SERVER_ID = "UserServerId";
    String USER_FIRST_NAME = "UserFirstName";
    String USER_LAST_NAME = "UserLastName";
    String USER_MIDDLE_NAME = "UserMiddleName";
    String USER_AUTH_TOKEN = "UserAuthToken";
    String USER_PHONE_NUMBER = "UserPhoneNumber";
    String USER_EMAIL = "UserEmail";
    String USER_PHOTO_URL = "UserPhotoUrl";
    String USER_BONUSES = "UserBonuses";
    String USER_RIDES_COUNT = "UserRidesCount";
    String USER_STATISTICS_FK = "UserStatisticsFk";
    String PROMO_CODE_FK = "PromoCodeFk";

    String CREATE_USER_TABLE = "CREATE TABLE "+ USER_TABLE + "("
            + USER_ID + " INTEGER PRIMARY KEY,"
            + USER_SERVER_ID + " TEXT,"
            + USER_FIRST_NAME + " TEXT,"
            + USER_LAST_NAME + " TEXT,"
            + USER_MIDDLE_NAME + " TEXT,"
            + USER_AUTH_TOKEN + " TEXT,"
            + USER_PHONE_NUMBER + " TEXT,"
            + USER_EMAIL + " TEXT,"
            + USER_PHOTO_URL + " TEXT,"
            + USER_BONUSES + " INTEGER,"
            + USER_STATISTICS_FK+ " INTEGER,"
            + PROMO_CODE_FK + " INTEGER,"
            + USER_RIDES_COUNT + " INTEGER,"
    +"FOREIGN KEY(" + USER_STATISTICS_FK+") REFERENCES "+ UserStatisticsTable.USER_STATISTICS_TABLE+"("+UserStatisticsTable.USER_STATISTICS_ID+"),"
    +"FOREIGN KEY(" + PROMO_CODE_FK +") REFERENCES "+ PromoCodeTable.PROMO_CODE_TABLE+"("+PromoCodeTable.PROMO_CODE_ID+"))";

}
