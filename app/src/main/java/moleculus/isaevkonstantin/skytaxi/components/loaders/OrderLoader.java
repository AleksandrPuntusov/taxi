package moleculus.isaevkonstantin.skytaxi.components.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import moleculus.isaevkonstantin.skytaxi.interactors.OrderInteractor;
import moleculus.isaevkonstantin.skytaxi.models.Order;

/**
 * Created by Isaev Konstantin on 12.05.16.
 */
public class OrderLoader extends AsyncTaskLoader<Order> {
    private OrderInteractor orderInteractor;

    public static final int LOAD_CURRENT_ORDER = 1;
    private int jobId;


    public OrderLoader(Context context,int jobId) {
        super(context);
        orderInteractor = new OrderInteractor(context);
        this.jobId = jobId;
        onContentChanged();
    }

    @Override
    public Order loadInBackground() {
        switch (jobId){
            case LOAD_CURRENT_ORDER:
                return orderInteractor.getCurrentOrderFromDb();
        }
        return null;
    }

    @Override
    public void forceLoad() {
        super.forceLoad();
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        if (takeContentChanged())
            forceLoad();
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
    }

    @Override
    public void deliverResult(Order data) {
        super.deliverResult(data);
    }
}
