package moleculus.isaevkonstantin.skytaxi.components.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import moleculus.isaevkonstantin.skytaxi.interactors.UserInteractor;
import moleculus.isaevkonstantin.skytaxi.models.User;

/**
 * Created by Isaev Konstantin on 03.05.16.
 */
public class UserLoader extends AsyncTaskLoader<User> {
    private UserInteractor userInteractor;
    public UserLoader(Context context) {
        super(context);
        userInteractor = new UserInteractor(context);
        onContentChanged();
    }

    @Override
    public User loadInBackground() {

        return userInteractor.getUserFromDb(0l);
    }

    @Override
    public void forceLoad() {
        super.forceLoad();
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        if (takeContentChanged())
            forceLoad();
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
    }

    @Override
    public void deliverResult(User data) {
        super.deliverResult(data);
    }
}
