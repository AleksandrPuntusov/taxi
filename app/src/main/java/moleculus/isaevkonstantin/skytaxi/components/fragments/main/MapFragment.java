package moleculus.isaevkonstantin.skytaxi.components.fragments.main;


import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.components.loaders.OrderLoader;
import moleculus.isaevkonstantin.skytaxi.models.Driver;
import moleculus.isaevkonstantin.skytaxi.models.Order;
import moleculus.isaevkonstantin.skytaxi.presenters.fragments.main.MapPresenter;
import moleculus.isaevkonstantin.skytaxi.components.activities.MainActivity;
import moleculus.isaevkonstantin.skytaxi.components.constants.ArgumentTitles;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpActionView;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpView;
import moleculus.isaevkonstantin.skytaxi.components.model.ActivityViewBuilder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MapFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback,
        MvpView, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener,
        ResultCallback<LocationSettingsResult>,LoaderManager.LoaderCallbacks {

    private SupportMapFragment mapFragment;
    private LatLng userPosition;
    private int ZOOM_LEVEL = 16;

    private int mapId;
    public static final int PATH_FROM = 1;
    public static final int PATH_TO = 2;
    public static final int TRIP_MAP = 3;
    private int orderStatus;

    public static final int CANCEL_ORDER_DIALOG = 1;
    private static final long MINUTE = 60*1000;


    public static final int MAP_FROM_ORDER_DETAILS = 4;
    public static final int MAP_TO_ORDER_DETAILS = 5;

    public static final int ORDER_LOADER_ID = 1;


    private MapPresenter presenter;
    @Bind(R.id.set_address_button)
    Button setAddressButton;
    @Bind(R.id.next_from_map_button)
    Button nextFromMapButton;
    @Bind(R.id.current_address_text_view)
    TextView addressTitleTextView;
    @Bind(R.id.get_location_fab)
    FloatingActionButton locationFab;
    @Bind(R.id.order_timer_layout)
    LinearLayout orderTimerLayout;
    @Bind(R.id.order_timer_text_view)
    TextView orderTimerTextView;


    private GoogleMap googleMap;
    private Marker userMarker;
    private CameraPosition cameraPosition;
    public MapFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MapFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MapFragment newInstance() {
        MapFragment fragment = new MapFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mapId = getArguments().getInt(ArgumentTitles.PATH_ID_TITLE_PARAM, -1);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        ButterKnife.bind(this, view);
        createMapFragment();
        String buttonTitle = " ";
        switch (mapId){
            case PATH_FROM:
                buttonTitle = getContext().getResources().getString(R.string.next_from_map_button_text_from);
                break;
            case PATH_TO:
                buttonTitle = getContext().getResources().getString(R.string.next_from_map_button_text_to);
                break;
            case TRIP_MAP:
                addressTitleTextView.setVisibility(View.GONE);
                setAddressButton.setVisibility(View.GONE);
                nextFromMapButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.timer_button_selector));
                nextFromMapButton.setTextColor(getResources().getColor(R.color.main_orange));
                locationFab.setVisibility(View.GONE);
                if (orderTimerLayout.getVisibility() == View.GONE) {
                    orderTimerLayout.setVisibility(View.VISIBLE);
                }
                buttonTitle = "Отменить заказ";
                break;
        }
        nextFromMapButton.setText(buttonTitle);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter = new MapPresenter();
        presenter.setMvpView(this);
        presenter.init();
        if(savedInstanceState==null){
            if (mapFragment != null) {
                if(googleMap==null){
                    mapFragment.getMapAsync(this);
                }else {
                    if(userPosition!=null){
                        presenter.getAddressTitleByCoordinates(userPosition.latitude,userPosition.longitude);
                    }
                }
            }
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        updateActionBar(mapId);
        presenter.registerSubscriber();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode== Activity.RESULT_OK&&requestCode==CANCEL_ORDER_DIALOG){
            presenter.cancelOrder();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                displayLocation(latLng.latitude,latLng.longitude);
            }
        });
        googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                addMarker(cameraPosition.target.latitude,cameraPosition.target.longitude);
                presenter.getAddressTitleByCoordinates(cameraPosition.target.latitude,cameraPosition.target.longitude);
            }
        });
        googleMap.getUiSettings().setCompassEnabled(false);
        googleMap.getUiSettings().setRotateGesturesEnabled(false);

        if(mapId==TRIP_MAP){
            googleMap.getUiSettings().setRotateGesturesEnabled(false);
            googleMap.getUiSettings().setScrollGesturesEnabled(false);
            getLoaderManager().initLoader(ORDER_LOADER_ID, getArguments(), this);
        }else {
            if(userPosition!=null){
                displayLocation(userPosition.latitude,userPosition.longitude);
            }
        }

//        else {
//            if(!presenter.isLocationEnable()){
//                mapFragment.getView().setVisibility(View.INVISIBLE);
//                presenter.checkLocationSettings();
//            }
//        }

    }
    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.unRegisterSubscriber();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.destroy();
    }

    public void updateAddress(String address) {
        addressTitleTextView.setText(address);
    }

    @OnClick(R.id.set_address_button)
    public void searchAddress() {
        presenter.searchAddressFrom(mapId);
    }

    @OnClick(R.id.get_location_fab)
    public void getLocation() {
        presenter.getLocation();
    }

    @OnClick(R.id.next_from_map_button)
    public void setFlat(){
        String locationName = addressTitleTextView.getText().toString();
        double latitude = userPosition.latitude;
        double longitude = userPosition.longitude;
        if(mapId==TRIP_MAP){
            if(orderStatus==Order.DRIVER_SEARCH_STATUS){
                presenter.showCancelOrder();

            }else if(orderStatus==Order.DRIWER_ON_RIDE_STATUS||orderStatus==Order.DRIWER_ON_PLACE_STATUS){

            }
        }else {
            presenter.enterEntrance(locationName,latitude,longitude,mapId);
        }
    }



    @Override
    public void onLocationChanged(Location location) {
        displayLocation(location.getLatitude(),location.getLongitude());
        mapFragment.getView().setVisibility(View.VISIBLE);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if(presenter.isLocationEnable()){
            presenter.getLocation();
        }else {
            Toast.makeText(getContext(),"Включите определение координат.Установка координат по умолчанию"
                    ,Toast.LENGTH_LONG).show();
            if(mapId!=TRIP_MAP){
                displayLocation(55.719,37.572);
            }
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(getContext(), "Suspended!",
                Toast.LENGTH_SHORT).show();
    }

    public void displayLocation(double latitude,double longitude){
        presenter.getAddressTitleByCoordinates(latitude,longitude);
        addMarker(latitude,longitude);
        cameraPosition = CameraPosition.builder()
                .target(userPosition)
                .zoom(ZOOM_LEVEL)
                .bearing(90)
                .build();
        if(googleMap!=null){
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }

    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void createMapFragment(){
        if(mapFragment==null){
            mapFragment = SupportMapFragment.newInstance();
            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.place_for_map, mapFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }

    }

    public void updateActionBar(int mapId){
        String title  = "";
        int drawableId = -1;
        switch (mapId){
            case MapFragment.PATH_FROM:
                title = getContext().getResources().getString(R.string.map_from_action_bar_title);
                drawableId = R.drawable.blue_star;
                break;
            case MapFragment.PATH_TO:
                title = getContext().getResources().getString(R.string.map_to_action_bar_title);
                drawableId = R.drawable.blue_star;
                break;
            case MapFragment.TRIP_MAP:
                switch (orderStatus){
                    case Order.DRIVER_SEARCH_STATUS:
                        title = getContext().getResources().getString(R.string.search_driver_action_bar_title);
                        break;
                    case Order.ASSIGN_DRIVER:
                        break;
                    case Order.DRIWER_ON_RIDE_STATUS:
                        title = getContext().getResources().getString(R.string.driver_riding_action_bar_title);
                        break;
                    case Order.DRIWER_ON_PLACE_STATUS:
                        title = getContext().getResources().getString(R.string.driver_in_place_action_bar_title);
                        break;
                }
                break;
        }
        ActivityViewBuilder activityViewBuilder = ActivityViewBuilder.createNavigation(title,ActivityViewBuilder.MENU_NAVIGATION);
        activityViewBuilder.setColor(R.color.transparent_white);
        activityViewBuilder.setActionBarNavigation(ActivityViewBuilder.MENU_NAVIGATION);
        activityViewBuilder.setActionBarTitle(title);
        activityViewBuilder.setMenuDrawableId(drawableId);
        ((MvpActionView)getActivity()).updateUi(activityViewBuilder);
    }

    private void addMarker(double latitude,double longitude){
        if (userMarker != null) {
            userMarker.remove();
        }
        userPosition = new LatLng(latitude,longitude);
////        userMarker = googleMap.addMarker(new MarkerOptions()
//                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin))
//                .position(userPosition)
//                .draggable(true)
//                .draggable(true)
//                .rotation(0));
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                presenter.getLocation();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(getActivity(), MainActivity.REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                break;
        }
    }

    public void updateOrder(){
        getLoaderManager().getLoader(ORDER_LOADER_ID).onContentChanged();

    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        Loader loader = null;
        if (id == ORDER_LOADER_ID) {
            loader =  new OrderLoader(getContext(),OrderLoader.LOAD_CURRENT_ORDER);
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        Order order = (Order)data;
        if (order != null) {
            orderStatus = order.getStatus();
            switch (order.getStatus()){
                case Order.DRIVER_SEARCH_STATUS:
                    searchDriverStatus(order);
                    break;
                case Order.DRIWER_ON_RIDE_STATUS:
                    driverOnRideStatus(order);
                    break;
                case Order.DRIWER_ON_PLACE_STATUS:
                    driverOnPlaceStatus(order);
                    break;
                case Order.ORDER_CLOSE_STATUS:
                    closeOrder();
                    break;
            }
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }

    private void searchDriverStatus(Order order){
        long time = 0;
        switch (order.getTimeId()) {
            case Order.START_SHORT_TIME:
                time = 10 * MINUTE;
                break;
            case Order.START_THIRTY_MINUTES:
                time = 30 * MINUTE;
                break;
            case Order.START_FORTY_FIVE_MINUTES:
                time = 45 * MINUTE;
                break;
            case Order.START_HOUR:
                time = 60 * MINUTE;
                break;
        }
        updateActionBar(TRIP_MAP);

        addMarker(order.getLocationFrom().getLatitude(),order.getLocationFrom().getLongitude());
        final long hours = TimeUnit.MILLISECONDS.toHours(time);
        final long minutes = TimeUnit.MILLISECONDS.toMinutes(time);
        CountDownTimer countDownTimer = new CountDownTimer(time, 1000) {
            public void onTick(long millisUntilFinished) {
                orderTimerTextView.setText(String.format("%02d:%02d:%02d",
                        (millisUntilFinished / (1000 * 60 * 60) % 24),
                        millisUntilFinished / 60000, (millisUntilFinished / 1000) % 60));
            }

            @Override
            public void onFinish() {

            }
        }.start();
        displayLocation(order.getLocationFrom().getLatitude(),order.getLocationFrom().getLongitude());
    }

    private void driverOnRideStatus(Order order){
        updateActionBar(TRIP_MAP);
        Driver driver = order.getDriver();
        if(driver!=null){
            String driverName = driver.getName();
            Driver.CarDetails carDetails = driver.getCarDetails();
            String text = "";
            if(driverName!=null){
                text += driverName;
            }
            if(carDetails!=null){
                text+="\n";
                text = carDetails.getModel()!=null?text+carDetails.getModel()+",":"";
                text = carDetails.getColor()!=null?text+carDetails.getColor()+",":"";
                text = carDetails.getNumber()!=null?text+carDetails.getNumber()+",":"";
            }
            addressTitleTextView.setText(text);
            nextFromMapButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.blue_button_selector));
            nextFromMapButton.setTextColor(getResources().getColor(R.color.white));
            nextFromMapButton.setText("Позвонить водителю");
            locationFab.setVisibility(View.VISIBLE);
            addressTitleTextView.setVisibility(View.VISIBLE);

        }

//        Driver driver = order.
    }

    public void driverOnPlaceStatus(Order order){
        updateActionBar(TRIP_MAP);

    }

    public void closeOrder(){
        presenter.showSuccessOrder();
    }
}

