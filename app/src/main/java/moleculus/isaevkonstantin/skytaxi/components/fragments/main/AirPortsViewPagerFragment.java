package moleculus.isaevkonstantin.skytaxi.components.fragments.main;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.Bind;
import butterknife.ButterKnife;
import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.presenters.fragments.main.AirPortsPagerPresenter;
import moleculus.isaevkonstantin.skytaxi.components.adapters.viewpager.AirPortViewPagerAdapter;
import moleculus.isaevkonstantin.skytaxi.components.constants.ArgumentTitles;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpActionView;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpView;
import moleculus.isaevkonstantin.skytaxi.components.model.ActivityViewBuilder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AirPortsViewPagerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AirPortsViewPagerFragment extends Fragment implements MvpView {

    @Bind(R.id.air_port_view_pager)
    ViewPager airPortsViewPager;
    @Bind(R.id.air_ports_tabs)
    TabLayout airPortsTabs;

    private AirPortsPagerPresenter presenter;
    private int pathId;
    private boolean forwardFromDetails;
    public AirPortsViewPagerFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AirPortsViewPagerFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AirPortsViewPagerFragment newInstance() {
        AirPortsViewPagerFragment fragment = new AirPortsViewPagerFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            pathId = getArguments().getInt(ArgumentTitles.PATH_ID_TITLE_PARAM);
            forwardFromDetails = getArguments().getBoolean(ArgumentTitles.FORWARD_FROM_ORDER_DETAILS);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_air_ports, container, false);
        ButterKnife.bind(this,view);

        AirPortViewPagerAdapter adapter = new AirPortViewPagerAdapter(getChildFragmentManager(),pathId,forwardFromDetails);
        airPortsViewPager.setAdapter(adapter);
        airPortsTabs.setupWithViewPager(airPortsViewPager);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter = new AirPortsPagerPresenter();
        presenter.setMvpView(this);
        presenter.init();
        presenter.loadStations();
    }

    @Override
    public void onStart() {
        super.onStart();
        updateActionBar();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.destroy();
        presenter = null;
    }

    public void updateActionBar(){
        ActivityViewBuilder activityViewBuilder = ActivityViewBuilder.createNavigation("",ActivityViewBuilder.BACK_NAVIGATION);
        activityViewBuilder.setColor(R.color.transparent_white);
        ((MvpActionView)getActivity()).updateUi(activityViewBuilder);
    }

}
