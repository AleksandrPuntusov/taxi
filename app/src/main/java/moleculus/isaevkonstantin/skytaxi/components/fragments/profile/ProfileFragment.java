package moleculus.isaevkonstantin.skytaxi.components.fragments.profile;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.components.dialogs.LoadImageDialog;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpActionView;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpView;
import moleculus.isaevkonstantin.skytaxi.components.loaders.UserLoader;
import moleculus.isaevkonstantin.skytaxi.components.model.ActivityViewBuilder;
import moleculus.isaevkonstantin.skytaxi.models.User;
import moleculus.isaevkonstantin.skytaxi.presenters.fragments.profile.ProfilePresenter;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment implements MvpView, LoaderManager.LoaderCallbacks {
    @Bind(R.id.user_logo_image_view)
    ImageView userLogoImageView;
    @Bind(R.id.empty_image_placeholder)
    ImageView emptyImagePlaceholder;
    @Bind(R.id.first_name_edit_text)
    EditText firsNameEditText;
    @Bind(R.id.last_name_edit_text)
    EditText lastNameEditText;
    @Bind(R.id.middle_name_edit_text)
    EditText middleNameEditText;
    @Bind(R.id.phone_text_view)
    TextView phoneTextView;
    @Bind(R.id.email_edit_text)
    EditText emailEditText;
    @Bind(R.id.confirm_profile_button)
    Button confirmProfileButton;
    @Bind(R.id.promo_code_first_underline)
    View firstPromoUnderLine;
    @Bind(R.id.promo_code_layout)
    LinearLayout promoCodeLayout;
    @Bind(R.id.credit_card_layout)
    LinearLayout creditCardLayout;
    @Bind(R.id.credit_card_first_underline)
    View creditCardFirstUnderLine;
    @Bind(R.id.promo_code_edit_text)
    EditText promoCodeEditText;

    public static final int LOAD_IMAGE = 0;
    public static final int IMAGE_FROM_CAMERA = 1;
    public static final int IMAGE_FROM_GALLERY = 2;

    public static final int FIRST_SETTINGS_PROFILE = 1;
    public static final int FROM_SLIDE_MENU_SETTINGS_PROFILE = 2;

    public static final String PROFILE_ID_TITLE_PARAM = "profileIdTitle";
    public static final String USER_TITLE_PARAM = "userTitleParam";

    private ProfilePresenter presenter;
    private int profileId;

    public static final int USER_LOADER_ID = 1;
    private User user;
    private boolean firstLoad;
    private Uri photoUri;


    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ProfileFragment.
     */
    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            profileId = getArguments().getInt(PROFILE_ID_TITLE_PARAM);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        if (profileId == FROM_SLIDE_MENU_SETTINGS_PROFILE) {
            firstPromoUnderLine.setVisibility(View.GONE);
            promoCodeLayout.setVisibility(View.GONE);
            creditCardFirstUnderLine.setVisibility(View.GONE);
            creditCardLayout.setVisibility(View.GONE);
            confirmProfileButton.setVisibility(View.GONE);
//            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        } else {
//            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter = new ProfilePresenter();
        presenter.setMvpView(this);
        presenter.init();
        getLoaderManager().initLoader(USER_LOADER_ID, null, this);

    }

    @Override
    public void onStart() {
        super.onStart();
        updateActionBar(profileId);
        presenter.registerSubscriber();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (profileId == FROM_SLIDE_MENU_SETTINGS_PROFILE) {
            saveUserProfile();
        }
        presenter.unRegisterSubscriber();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.destroy();
        presenter = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ProfileFragment.LOAD_IMAGE && resultCode == Activity.RESULT_OK) {
            int photoSourceId = data.getIntExtra(LoadImageDialog.LOAD_IMAGE_SOURCE_TITLE, -1);
            switch (photoSourceId) {
                case IMAGE_FROM_CAMERA:
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, IMAGE_FROM_CAMERA);
                    }
                    break;
                case IMAGE_FROM_GALLERY:
                    Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, IMAGE_FROM_GALLERY);
                    break;
            }

        }
        if (requestCode == IMAGE_FROM_CAMERA && resultCode == Activity.RESULT_OK) {
            Bundle extras = data.getExtras();
            photoUri = data.getData();

            Bitmap imageBitmap = (Bitmap) extras.get("data");
            if (imageBitmap != null) {
                userLogoImageView.setImageBitmap(imageBitmap);
                emptyImagePlaceholder.setVisibility(View.GONE);
                //reading file structure
                File cdCardDir = Environment.getExternalStorageDirectory();
                String mFootballDirPath = "Pictures/";
                File photoDir = new File(cdCardDir, mFootballDirPath);
                boolean isDirExists = photoDir.exists();
                if (!isDirExists) {
                    isDirExists = photoDir.mkdir();
                }

                /** Pattern for file name.*/
                String mFileName = new SimpleDateFormat("yyyyMMdd_ss").format(new Date()) + ".jpg";
                File bitmapFile = new File(photoDir, mFileName);

                if (isDirExists) {
                    //saving bitmap to file
                    try {
                        FileOutputStream out = new FileOutputStream(bitmapFile);
                        imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                        out.flush();
                        out.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                Picasso.with(getContext())
                        .load(bitmapFile)
                        .fit()
                        .noFade()
                        .centerInside()
                        .into(userLogoImageView);
            }

        }
        if (requestCode == IMAGE_FROM_GALLERY && resultCode == Activity.RESULT_OK && data != null) {
            photoUri = data.getData();
            if (photoUri != null) {
                Picasso.with(getContext())
                        .load(photoUri)
                        .fit()
                        .noFade()
                        .centerInside()
                        .into(userLogoImageView);
                emptyImagePlaceholder.setVisibility(View.GONE);
            }
        }
    }

    @OnClick(R.id.user_logo_image_view)
    public void userLogoImageViewClick() {
        closeKeyboard();
        presenter.createUserLogo();
    }

    @OnClick(R.id.credit_card_layout)
    public void setCreaditCardClick() {
        presenter.setCreditCard();
    }

    @OnClick(R.id.confirm_profile_button)
    public void saveUserProfile() {
        closeKeyboard();
        String firstName = firsNameEditText.getText().toString();
        String lastName = lastNameEditText.getText().toString();
        String middleName = middleNameEditText.getText().toString();
        String promoCode = promoCodeEditText.getText().toString();
        if (firstName.isEmpty() || lastName.isEmpty() || middleName.isEmpty()) {
            Toast.makeText(getContext(), "Заполните все поля", Toast.LENGTH_LONG).show();
        } else {
            String email = emailEditText.getText().toString();
            BitmapDrawable bitmapDrawable = ((BitmapDrawable) userLogoImageView.getDrawable());
            String authToken = null;
            if (user != null) {
                authToken = user.getAuthToken();
            }
            if (bitmapDrawable == null) {
                photoUri = null;
            }
            presenter.sendUser(firstName, lastName, middleName, email, photoUri, authToken);
        }
    }

    public void updateUserProfile(User user) {
        this.user = user;
        updateTextField(user.getFirstName(), firsNameEditText);
        updateTextField(user.getLastName(), lastNameEditText);
        updateTextField(user.getMiddleName(), middleNameEditText);
//        updateTextField(user.getPromoCode().getCode(), promoCodeEditText);
        updateTextField(user.getEmail(), emailEditText);
        String phoneNumber = user.getPhoneNumber();
        if (phoneNumber != null) {
//            phoneNumber = "75555555555";
            if (phoneNumber.startsWith("7") && phoneNumber.length() == 11) {
                StringBuilder stringBuilder = new StringBuilder(phoneNumber);
                stringBuilder.insert(1, "(").insert(5, ") ").insert(10, "-").insert(13, "-");
                updateTextField(stringBuilder.toString(), phoneTextView);
            } else {
                updateTextField(phoneNumber, phoneTextView);

            }

        }
//        updateTextField(user.getPhoneNumber(), phoneTextView);
        if (user.getPhotoUrl() != null) {
            Picasso.with(getContext()).load(user.getPhotoUrl()).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    userLogoImageView.setImageBitmap(bitmap);
                    userLogoImageView.setVisibility(View.VISIBLE);
                    emptyImagePlaceholder.setVisibility(View.GONE);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            });

        }
    }

    public void updateActionBar(int profileId) {
        ActivityViewBuilder activityViewBuilder;
        String actionBarTitle;
        if (profileId == ProfileFragment.FIRST_SETTINGS_PROFILE) {
            actionBarTitle = getContext().getResources().getString(R.string.profile_action_bar_title_firts_settings);
            activityViewBuilder = ActivityViewBuilder.createNavigation(actionBarTitle,
                    ActivityViewBuilder.NOT_NAVIGATION);
        } else {
            actionBarTitle = getContext().getResources().getString(R.string.profile_action_bar_title_slide_menu_settings);
            activityViewBuilder = ActivityViewBuilder.createNavigation(actionBarTitle,
                    ActivityViewBuilder.BACK_NAVIGATION);
        }
        activityViewBuilder.setColor(R.color.white);

        ((MvpActionView) getActivity()).updateUi(activityViewBuilder);
    }


    private void updateTextField(String source, EditText editText) {
        if (source != null) {
            editText.setText(source);
            editText.setSelection(source.length());
        }
    }

    private void updateTextField(String source, TextView textView) {
        if (source != null) {
            textView.setText(source);
        }
    }

    private void closeKeyboard() {
        InputMethodManager imm = (InputMethodManager)
                getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isAcceptingText()) {
            imm.hideSoftInputFromWindow(getView().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        Loader loader = null;
        if (id == USER_LOADER_ID) {
            loader = new UserLoader(getContext());
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        User user = (User) data;
        if (user != null) {
            updateUserProfile(user);
            if (!firstLoad) {
                firstLoad = true;
                presenter.syncUser(user);
            }

        }
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }


}
