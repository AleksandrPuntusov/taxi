package moleculus.isaevkonstantin.skytaxi.components.fragments.trip;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.models.Order;
import moleculus.isaevkonstantin.skytaxi.models.OrderOptions;
import moleculus.isaevkonstantin.skytaxi.presenters.fragments.trip.TripSettingsPresenter;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpView;
import moleculus.isaevkonstantin.skytaxi.components.loaders.OrderLoader;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TripSettingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TripSettingsFragment extends Fragment  implements MvpView,LoaderManager.LoaderCallbacks {
    //Views
    @Bind(R.id.bag_counter_text_view)
    TextView bagCounterTextView;
    @Bind(R.id.plus_bag_radio_button)
    RadioButton plusBagRadioButton;
    @Bind(R.id.minus_bag_radio_button)
    RadioButton minusBagRadioButton;
    @Bind(R.id.no_smoking_driver_switch_button)
    SwitchCompat noSomokingDriverSwitch;
    @Bind(R.id.flight_number_edit_text)
    EditText flightNumberEditText;

    public static final int ORDER_LOADER_ID = 1;
    private TripSettingsPresenter presenter;
    private OrderOptions orderOptions;

    public TripSettingsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TripSettingsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TripSettingsFragment newInstance() {
        TripSettingsFragment fragment = new TripSettingsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_trip_settings, container, false);
        ButterKnife.bind(this,view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter = new TripSettingsPresenter();
        presenter.setMvpView(this);
        presenter.init();
        getLoaderManager().initLoader(ORDER_LOADER_ID, getArguments(), this);
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.updateActionBar();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        if(orderOptions!=null){
            orderOptions.setNoSmokingDriver(noSomokingDriverSwitch.isChecked());
            String flighNumber = flightNumberEditText.getText().toString();
            if(flighNumber!=null&&!flighNumber.isEmpty()){
                orderOptions.setFlightNumber(flighNumber);
            }
            presenter.updateOrderOptions(orderOptions);

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.destroy();
    }

    private void updateOrder(OrderOptions orderOptions){
        if(orderOptions!=null){
            this.orderOptions=orderOptions;
        }
        bagCounterTextView.setText(String.valueOf(orderOptions.getBagCount()));
        noSomokingDriverSwitch.setChecked(orderOptions.isNoSmokingDriver());
        flightNumberEditText.setText(orderOptions.getFlightNumber());
    }

    @OnClick(R.id.trip_comment_layout)
    public void addCommentClick(){
        presenter.addComment();
    }

    @OnClick(R.id.plus_bag_radio_button)
    public void plusBagClick(){

        int counter = orderOptions.getBagCount()+1;
        if(counter<8){
            orderOptions.setBagCount(counter);
            plusBagRadioButton.setChecked(false);
            updateOrder(orderOptions);
        }


    }
    @OnClick(R.id.minus_bag_radio_button)
    public void minusBagClick(){
        int counter = orderOptions.getBagCount()-1;
        if(counter>0){
            orderOptions.setBagCount(counter);
            minusBagRadioButton.setChecked(false);
            updateOrder(orderOptions);
        }

    }
    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        Loader loader = null;
        if (id == ORDER_LOADER_ID) {
            loader =  new OrderLoader(getContext(),OrderLoader.LOAD_CURRENT_ORDER);
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        Order order = (Order)data;
        if(order!=null){
            updateOrder(order.getOrderOptions());

        }
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }
}
