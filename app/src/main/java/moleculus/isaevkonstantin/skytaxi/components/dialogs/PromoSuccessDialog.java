package moleculus.isaevkonstantin.skytaxi.components.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import butterknife.ButterKnife;
import moleculus.isaevkonstantin.skytaxi.R;

/**
 * Created by Isaev Konstantin on 10.05.16.
 */
public class PromoSuccessDialog extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View alertDialogView = inflater.inflate(R.layout.dialog_promo_success, null);
        ButterKnife.bind(this, alertDialogView);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(alertDialogView);
        AlertDialog alertDialog = builder.create();
        return alertDialog;
    }
}
