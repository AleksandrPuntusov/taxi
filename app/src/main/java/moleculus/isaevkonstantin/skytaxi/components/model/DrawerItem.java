package moleculus.isaevkonstantin.skytaxi.components.model;

/**
 * Created by Isaev Konstantin on 16.04.16.
 */
public class DrawerItem {

    private String title;
    private int imageId;
    private boolean checkSwitch;

    private RecycleViewHeader recycleViewHeader;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public boolean isCheckSwitch() {
        return checkSwitch;
    }

    public void setCheckSwitch(boolean checkSwitch) {
        this.checkSwitch = checkSwitch;
    }

    public RecycleViewHeader getRecycleViewHeader() {
        return recycleViewHeader;
    }

    public void setRecycleViewHeader(RecycleViewHeader recycleViewHeader) {
        this.recycleViewHeader = recycleViewHeader;
    }
}
