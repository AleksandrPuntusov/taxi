package moleculus.isaevkonstantin.skytaxi.components.fragments.trip;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import moleculus.isaevkonstantin.skytaxi.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TripCommentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TripCommentFragment extends Fragment {


    public TripCommentFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TripCommentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TripCommentFragment newInstance() {
        TripCommentFragment fragment = new TripCommentFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_trip_comment, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

}
