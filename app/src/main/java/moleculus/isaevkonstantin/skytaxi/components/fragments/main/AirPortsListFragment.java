package moleculus.isaevkonstantin.skytaxi.components.fragments.main;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.models.Location;
import moleculus.isaevkonstantin.skytaxi.presenters.fragments.main.AirPortsListPresenter;
import moleculus.isaevkonstantin.skytaxi.components.adapters.recycleview.StationRecyclerViewAdapter;
import moleculus.isaevkonstantin.skytaxi.components.constants.ArgumentTitles;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpView;
import moleculus.isaevkonstantin.skytaxi.components.loaders.LocationLoader;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AirPortsListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AirPortsListFragment extends Fragment implements MvpView,LoaderManager.LoaderCallbacks<List<Location>> {
    @Bind(R.id.stations_recycler_view)
    RecyclerView stationsRecyclerView;
    private int listId;
    private int pathId;
    private StationRecyclerViewAdapter adapter;
    private AirPortsListPresenter presenter;
    public static final int RAILS_STATIONS = 1;
    public static final int AIR_PORTS = 2;

    public static final int PATH_FROM =1;
    public static final int PATH_TO =2;

    public static final int LOCATION_LOADER_ID = 1;

    public AirPortsListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AirPortsListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AirPortsListFragment newInstance(int listId,int pathId,boolean forwardFromDetails) {
        AirPortsListFragment fragment = new AirPortsListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ArgumentTitles.LOCATION_LIST_FOR_AIR_PORTS_PARAM,listId);
        bundle.putInt(ArgumentTitles.PATH_ID_TITLE_PARAM,pathId);
        bundle.putBoolean(ArgumentTitles.FORWARD_FROM_ORDER_DETAILS,forwardFromDetails);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            listId = getArguments().getInt(ArgumentTitles.LOCATION_LIST_FOR_AIR_PORTS_PARAM);
            pathId = getArguments().getInt(ArgumentTitles.PATH_ID_TITLE_PARAM);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_air_ports_list, container, false);
        ButterKnife.bind(this,view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        stationsRecyclerView.setLayoutManager(linearLayoutManager);
        adapter = new StationRecyclerViewAdapter();
        stationsRecyclerView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(LOCATION_LOADER_ID, getArguments(), this);
        presenter = new AirPortsListPresenter();
        presenter.setMvpView(this);
        presenter.init();
        presenter.setPathId(pathId);
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.registerSubscriber();

    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.unRegisterSubscriber();
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        Loader loader = null;
        if (id == LOCATION_LOADER_ID) {
            if(listId==AIR_PORTS){
                loader =  new LocationLoader(getContext(),LocationLoader.LOAD_AIR_PORTS);
            }else if (listId==RAILS_STATIONS){
                loader =  new LocationLoader(getContext(),LocationLoader.LOAD_RAILS_STATION);
            }
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<List<Location>> loader, List<Location> data) {
        if(data!=null){
            adapter.removeAll();
            adapter.addAll(data);
        }
    }


    @Override
    public void onLoaderReset(Loader loader) {

    }
}
