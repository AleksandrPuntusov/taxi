package moleculus.isaevkonstantin.skytaxi.components.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Class change default typeface to another .
 * Created by Isaev Konstantin on 13.04.16.
 */
public class TypefaceUtil {
    public static final int UI_DISPLAY_BLACK = 0;
    public static final int UI_DISPLAY_BOLD = 1;
    public static final int UI_DISPLAY_HEAVY = 2;
    public static final int UI_DISPLAY_LIGHT = 3;
    public static final int UI_DISPLAY_MEDIUM = 4;
    public static final int UI_DISPLAY_REGULAR = 5;
    public static final int UI_DISPLAY_SEMIBOLD = 6;
    public static final int UI_DISPLAY_THIN = 7;
    public static final int UI_DISPLAY_ULTRALIGHT = 8;
    public static final int UI_TEXT_BOLD = 9;
    public static final int UI_TEXT_BOLD_ITALIC = 10;
    public static final int UI_TEXT_HEAVY = 11;
    public static final int UI_TEXT_HEAVY_ITALIC = 12;
    public static final int UI_TEXT_LIGHT = 13;
    public static final int UI_TEXT_LIGHT_ITALIC = 14;
    public static final int UI_TEXT_MEDIUM = 15;
    public static final int UI_TEXT_MEDIUM_ITALIC = 16;
    public static final int UI_TEXT_REGULAR = 17;
    public static final int UI_TEXT_REGULAR_ITALIC = 18;
    public static final int UI_TEXT_SEMI_BOLD = 19;
    public static final int UI_TEXT_SEMI_BOLD_ITALIC = 20;

    public void setTypeface(TextView textView,int fontId){
        Typeface tf = loadFont(textView.getContext(),fontId);
        textView.setTypeface(tf);
    }

    public void setTypeface(EditText editText, int fontId){
        Typeface tf = loadFont(editText.getContext(),fontId);
        editText.setTypeface(tf);
    }

    private Typeface loadFont(Context context,int fontId){
        Typeface tf = null;
        switch (fontId){
            case UI_DISPLAY_BLACK:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/SF-UI-Display-Black.otf");
                break;
            case UI_DISPLAY_BOLD:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/SF-UI-Display-Bold.otf");
                break;
            case UI_DISPLAY_HEAVY:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/SF-UI-Display-Heavy.otf");
                break;
            case UI_DISPLAY_LIGHT:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/SF-UI-Display-Light.otf");
                break;
            case UI_DISPLAY_MEDIUM:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/SF-UI-Display-Medium.otf");
                break;
            case UI_DISPLAY_REGULAR:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/SF-UI-Display-Regular.otf");
                break;
            case UI_DISPLAY_SEMIBOLD:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/SF-UI-Display-Semibold.otf");
                break;
            case UI_DISPLAY_THIN:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/SF-UI-Display-Thin.otf");
                break;
            case UI_DISPLAY_ULTRALIGHT:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/SF-UI-Display-Ultralight.otf");
                break;
            case UI_TEXT_BOLD:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/SF-UI-Text-Bold.otf");
                break;
            case UI_TEXT_BOLD_ITALIC:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/SF-UI-Text-BoldItalic.otf");
                break;
            case UI_TEXT_HEAVY:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/SF-UI-Text-Heavy.otf");
                break;
            case UI_TEXT_HEAVY_ITALIC:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/SF-UI-Text-HeavyItalic.otf");
                break;
            case UI_TEXT_LIGHT:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/SF-UI-Text-Light.otf");
                break;
            case UI_TEXT_LIGHT_ITALIC:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/SF-UI-Text-LightItalic.otf");
                break;
            case UI_TEXT_MEDIUM:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/SF-UI-Text-Medium.otf");
                break;
            case UI_TEXT_MEDIUM_ITALIC:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/SF-UI-Text-MediumItalic.otf");
                break;
            case UI_TEXT_REGULAR:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/SF-UI-Text-Regular.otf");
                break;
            case UI_TEXT_REGULAR_ITALIC:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/SF-UI-Text-RegularItalic.otf");
                break;
            case UI_TEXT_SEMI_BOLD:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/SF-UI-Text-Semibold.otf");
                break;
            case UI_TEXT_SEMI_BOLD_ITALIC:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/SF-UI-Text-SemiboldItalic.otf");
                break;
        }
        return tf;
    }
}
