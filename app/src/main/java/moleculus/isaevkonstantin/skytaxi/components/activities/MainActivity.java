
package moleculus.isaevkonstantin.skytaxi.components.activities;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.NavigationView;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.net.URISyntaxException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.network.NetworkRequestEvent;
import moleculus.isaevkonstantin.skytaxi.models.User;
import moleculus.isaevkonstantin.skytaxi.presenters.activities.MainActivityPresenter;
import moleculus.isaevkonstantin.skytaxi.components.adapters.recycleview.NavigationRecycleViewAdapter;
import moleculus.isaevkonstantin.skytaxi.components.fragments.main.MapFragment;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpActionView;
import moleculus.isaevkonstantin.skytaxi.components.loaders.UserLoader;
import moleculus.isaevkonstantin.skytaxi.components.model.ActivityViewBuilder;
import moleculus.isaevkonstantin.skytaxi.components.model.DrawerItem;
import moleculus.isaevkonstantin.skytaxi.components.model.RecycleViewHeader;
import moleculus.isaevkonstantin.skytaxi.components.service.SkyTaxiService;
import moleculus.isaevkonstantin.skytaxi.components.service.WebSocketClientService;

public class MainActivity extends AppCompatActivity implements MvpActionView,
        LoaderManager.LoaderCallbacks{
    private MainActivityPresenter presenter;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_title)
    TextView toolBarTitleTextView;
    @Bind(R.id.close_nav_drawer_image_view)
    ImageView closeNavDrawerImageView;
    @Bind(R.id.open_profile_image_view)
    ImageView openProfileImageView;
    @Bind(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @Bind(R.id.nav_view)
    NavigationView navigationView;
    @Bind(R.id.nav_recycle_view)
    RecyclerView navRecycleView;
    @Bind(R.id.user_name_nav_text_view)
    TextView userNameNavTextView;
    @Bind(R.id.phone_number_nav_text_view)
    TextView phoneNumberNavTetxView;
    @Bind(R.id.right_menu_button)
    ImageView rightMenuImageView;
    @Bind(R.id.user_logo_image_view)
    ImageView userLogoImageView;
    @Bind(R.id.user_logo_place_holder)
    ImageView userLogoPlcaHolderImageView;
    private ActionBarDrawerToggle toggle;
    private Intent serviceIntent;
    private Intent webSocketIntent;

    private NavigationRecycleViewAdapter adapter;
    private ServiceConnection sConn;

    public static final int USER_LOADER_ID = 0;
    public static final int REQUEST_CHECK_SETTINGS = 1;

    private boolean firstLoad;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        initDrawer(true);
        navigationView.setItemIconTintList(null);
        initNavigationView();
        serviceIntent = new Intent(this, SkyTaxiService.class);
        presenter = new MainActivityPresenter();
        presenter.setMvpView(this);
        presenter.init();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.status_bar_color));
        }
        webSocketIntent = new Intent(this, WebSocketClientService.class);
        sConn = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                WebSocketClientService.WebSocketBinder binder = (WebSocketClientService.WebSocketBinder) service;
                WebSocketClientService socketClientService = binder.getService();
                try {
                    socketClientService.connectToSocket();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {

            }
        };
        if(savedInstanceState==null){
            presenter.clearAddToBackStack();
            presenter.createMap(MapFragment.PATH_FROM);
//            if(presenter.checkOrder()){
//                listenOrderStatus();
//            }
        }
    }

    @Override
    public void updateUi(ActivityViewBuilder activityViewBuilder) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            toolBarTitleTextView.setText(activityViewBuilder.getActionBarTitle());
            switch (activityViewBuilder.getActionBarNavigation()){
                case ActivityViewBuilder.BACK_NAVIGATION:
                    actionBar.setDisplayHomeAsUpEnabled(true);
                    actionBar.setDisplayShowHomeEnabled(true);
                    actionBar.setHomeButtonEnabled(true);
                    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onBackPressed();
                        }
                    });
                    break;
                case ActivityViewBuilder.MENU_NAVIGATION:
                    actionBar.setDisplayShowHomeEnabled(false);
                    actionBar.setHomeButtonEnabled(false);
                    actionBar.setDisplayHomeAsUpEnabled(false);
                    drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                    toggle.syncState();
                    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            drawerLayout.openDrawer(GravityCompat.START);
                        }
                    });
                    break;
                case ActivityViewBuilder.CLOSE_NAVIGATION:

                    break;
            }
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(activityViewBuilder.getColor())));
            if(activityViewBuilder.getMenuDrawableId()>0){
                rightMenuImageView.setVisibility(View.VISIBLE);
                rightMenuImageView.setImageDrawable(getResources().getDrawable(activityViewBuilder.getMenuDrawableId()));
            }else {
                rightMenuImageView.setVisibility(View.INVISIBLE);

            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.registerSubscriber();
        getSupportLoaderManager().initLoader(USER_LOADER_ID, null,this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.unRegisterSubscriber();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.destroy();
    }

    @Override
    public void onBackPressed() {
        if(drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }else {
            if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
                this.finish();
            } else {
                getSupportFragmentManager().popBackStack();
            }
        }

    }



    @OnClick(R.id.right_menu_button)
    public void clickRightMenuButton(){
        presenter.rightMenuClick();
    }


    @OnClick(R.id.close_nav_drawer_image_view)
    public void closeNavDrawer(){
        drawerLayout.closeDrawer(GravityCompat.START);

    }

    @OnClick(R.id.open_profile_image_view)

    public void openProfile(){
        closeNavDrawer();
        presenter.openProfile();

    }

    public void listenOrderStatus(){
        if(webSocketIntent!=null){
            bindService(webSocketIntent, sConn, BIND_AUTO_CREATE);
        }
    }

    public void networkFailded(){
        Toast.makeText(this, "Нет интернет соединения!!",
                Toast.LENGTH_SHORT).show();
    }

    public void loadFromApi(NetworkRequestEvent<Bundle> event) {
        serviceIntent.putExtra(SkyTaxiService.SERVICE_JOB_ID_TITLE, event.getId());
        serviceIntent.putExtras(event.getData());
        startService(serviceIntent);
    }

    public void updateUserProfile(User user){
        String firstName = user.getFirstName();
        String lastName = user.getLastName();
        String phone = user.getPhoneNumber();
        userNameNavTextView.setText(firstName+" "+ lastName);
        phoneNumberNavTetxView.setText(phone);
        if(user.getPhotoUrl()!=null){
            Picasso.with(getApplicationContext()).load(user.getPhotoUrl()).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    userLogoImageView.setImageBitmap(bitmap);
                    userLogoPlcaHolderImageView.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            });
        }
    }

    public void makeToast(String text){
        Toast.makeText(this,text,Toast.LENGTH_LONG).show();
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        Loader loader = null;
        if (id == USER_LOADER_ID) {
            loader =  new UserLoader(this);

        }
        return loader;

    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        User user = (User)data;
        if(user!=null){
            updateUserProfile(user);
            if(!firstLoad){
                firstLoad = true;
                presenter.syncUser(user);
            }

        }
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }

    private void initNavigationView(){
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) navigationView.getLayoutParams();
        params.width = metrics.widthPixels;
        navigationView.setLayoutParams(params);
        adapter = new NavigationRecycleViewAdapter();
        String[] navTitles = getResources().getStringArray(R.array.navigation_view_titles);
        int[] navImageid = {R.drawable.persent_nav_drawer,R.drawable.credit_card_profile,
                R.drawable.man_nav_drawer,R.drawable.car_nav_drawer,R.drawable.make_call_nav_drawer,
                R.drawable.logout_naw_drawer};
        DrawerItem drawerItem;
        RecycleViewHeader header;
        for (int i = 0; i < navImageid.length; i++) {
            drawerItem = new DrawerItem();
            drawerItem.setTitle(navTitles[i]);
            drawerItem.setImageId(navImageid[i]);
            if(i==3){
                header = new RecycleViewHeader();
                header.setId(RecycleViewHeader.NAV_DRAWER_SWITCH_HEADER);
                drawerItem.setRecycleViewHeader(header);
                drawerItem.setCheckSwitch(true);
            }
            adapter.add(drawerItem);
        }
        drawerItem = new DrawerItem();
        header = new RecycleViewHeader();
        header.setId(RecycleViewHeader.NAV_DRAWER_EMPTY_HEADER);
        drawerItem.setRecycleViewHeader(header);
        adapter.insertByPosition(1,drawerItem);
        adapter.insertByPosition(4,drawerItem);
        adapter.insertByPosition(6,drawerItem);
        adapter.insertByPosition(8,drawerItem);
        adapter.insertByPosition(10,drawerItem);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        navRecycleView.setLayoutManager(linearLayoutManager);
        navRecycleView.setAdapter(adapter);
    }

    private void initDrawer(boolean menu){

        toggle = new ActionBarDrawerToggle(
                this, drawerLayout,toolbar,R.string.nav_drawer_open, R.string.nav_drawer_close);
        toggle.setDrawerIndicatorEnabled(menu);

        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        presenter.locationSuccess();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i("", "User chose not to make required location settings changes.");
                        break;
                }
                break;
        }
    }
}
