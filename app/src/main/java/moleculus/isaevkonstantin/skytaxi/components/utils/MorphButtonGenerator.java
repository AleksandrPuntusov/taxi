package moleculus.isaevkonstantin.skytaxi.components.utils;

import android.content.Context;

import com.dd.morphingbutton.MorphingAnimation;
import com.dd.morphingbutton.MorphingButton;

import moleculus.isaevkonstantin.skytaxi.R;

/**
 * Created by Isaev Konstantin on 07.05.16.
 */
public class MorphButtonGenerator {
    public static final int MORPH_ENABLE = 0;
    public static final int MORPH_DISABLE = 1;
    public static final int MORPH_PROGRESS = 2;
    public static final int MORPH_SUCCESS = 3;
    public static final int MORPH_FAILED = 4;

    private void morphButton(int morphState,MorphingAnimation.Listener animationListener,
                             String text,MorphingButton morphingButton){
        Context context = morphingButton.getContext();
        MorphingButton.Params circleRect = null;
        switch (morphState) {
            case MORPH_ENABLE:
                circleRect = MorphingButton.Params.create()
                        .duration(0)
                        .width((int) context.getResources().getDimension(R.dimen.edit_text_registration_width))
                        .height((int) context.getResources().getDimension(R.dimen.confirm_registration_height))
                        .cornerRadius((int) context.getResources().getDimension(R.dimen.mb_height_56))
                        .text(text)
                        .color(context.getResources().getColor(R.color.main_blue));
                // normal state color
                break;
            case MORPH_DISABLE:
                circleRect = MorphingButton.Params.create()
                        .duration(0)
                        .text(text)
                        .width((int) context.getResources().getDimension(R.dimen.edit_text_registration_width))
                        .height((int) context.getResources().getDimension(R.dimen.confirm_registration_height))
                        .cornerRadius((int) context.getResources().getDimension(R.dimen.mb_height_56)) // 56 dp
                        .color(context.getResources().getColor(R.color.main_blue_semi_transparent));
                break;
            case MORPH_PROGRESS:
                circleRect = MorphingButton.Params.create()
                        .duration(300)
                        .width((int) context.getResources().getDimension(R.dimen.mb_height_56))
                        .height((int) context.getResources().getDimension(R.dimen.mb_height_56))
                        .cornerRadius((int) context.getResources().getDimension(R.dimen.mb_height_56))
                        .color(context.getResources().getColor(R.color.main_blue));
                break;
            case MORPH_SUCCESS:
                circleRect = MorphingButton.Params.create()
                        .icon(R.drawable.ic_action_accept)
                        .duration(300)

                        .width((int) context.getResources().getDimension(R.dimen.edit_text_registration_width))
                        .height((int) context.getResources().getDimension(R.dimen.confirm_registration_height))
                        .cornerRadius((int)context. getResources().getDimension(R.dimen.mb_height_56))
                        .color(context.getResources().getColor(R.color.cpb_green));

                break;
            case MORPH_FAILED:
                circleRect = MorphingButton.Params.create()
                        .duration(300)
                        .text(text)
                        .width((int) context.getResources().getDimension(R.dimen.edit_text_registration_width))
                        .height((int) context.getResources().getDimension(R.dimen.confirm_registration_height))
                        .cornerRadius((int)context. getResources().getDimension(R.dimen.mb_height_56))
                        .color(context.getResources().getColor(R.color.cpb_red));

//                circleRect = MorphingButton.Params.create()
//                        .icon(R.drawable.ic_action_cancel)
//                        .duration(300)
//                        .width((int) context.getResources().getDimension(R.dimen.edit_text_registration_height))
//                        .height((int)context.getResources().getDimension(R.dimen.confirm_registration_height))
//                        .cornerRadius((int) context.getResources().getDimension(R.dimen.mb_height_56))
//                        .color(context.getResources().getColor(R.color.cpb_red));
                break;
        }
        if (circleRect != null) {
            if(animationListener!=null){
                circleRect.animationListener(animationListener);
            }
            morphingButton.morph(circleRect);
        }
    }

    public void morphButton(int morphState,String text,MorphingButton morphingButton){
        this.morphButton(morphState,null,text,morphingButton);
    }
}
