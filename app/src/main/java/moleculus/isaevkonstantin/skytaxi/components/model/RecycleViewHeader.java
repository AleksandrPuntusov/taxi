package moleculus.isaevkonstantin.skytaxi.components.model;

/**
 * Created by Isaev Konstantin on 28.03.16.
 */
public class RecycleViewHeader {

    private int id;

    public static final int NAV_DRAWER_SIMPLE_HEADER = 0;
    public static final int NAV_DRAWER_EMPTY_HEADER = 1;
    public static final int NAV_DRAWER_SWITCH_HEADER = 2;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
