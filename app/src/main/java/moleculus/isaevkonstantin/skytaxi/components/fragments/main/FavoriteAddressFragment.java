package moleculus.isaevkonstantin.skytaxi.components.fragments.main;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpActionView;
import moleculus.isaevkonstantin.skytaxi.components.model.ActivityViewBuilder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FavoriteAddressFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FavoriteAddressFragment extends Fragment {


    public FavoriteAddressFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FavoriteAddressFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FavoriteAddressFragment newInstance() {
        FavoriteAddressFragment fragment = new FavoriteAddressFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorite_address, container, false);
        ButterKnife.bind(this,view);
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        updateActionBar();
    }

    public void updateActionBar(){
        String title;
        title = getContext().getResources().getString(R.string.favorites_action_bar_title);
        ActivityViewBuilder activityViewBuilder = ActivityViewBuilder.createNavigation(title,ActivityViewBuilder.BACK_NAVIGATION);
        activityViewBuilder.setColor(R.color.white);
        ((MvpActionView)getActivity()).updateUi(activityViewBuilder);
    }

}
