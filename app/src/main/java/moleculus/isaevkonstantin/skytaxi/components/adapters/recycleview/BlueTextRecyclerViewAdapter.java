package moleculus.isaevkonstantin.skytaxi.components.adapters.recycleview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.eventbus.BusProvider;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.ListItemClickEvent;
import moleculus.isaevkonstantin.skytaxi.components.fragments.trip.BlueListTimePaymentFragment;

/**
 * Created by Isaev Konstantin on 08.05.16.
 */
public class BlueTextRecyclerViewAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private List<String> titles;
    private int listId;

    public BlueTextRecyclerViewAdapter(int listId) {
        titles = new ArrayList<>();
        this.listId = listId;
    }

    public void addAll(List<String> tempTitles){
        titles.addAll(tempTitles);
        notifyDataSetChanged();
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        RecyclerView.ViewHolder viewHolder = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.blue_text_list_item,parent,false);
        viewHolder = new BlueTextListContentHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        String title = titles.get(position);
        ((BlueTextListContentHolder)holder).titleTextView.setText(title);
        if(listId== BlueListTimePaymentFragment.TIME_LIST_FRAGMENT){
            holder.itemView.setOnClickListener(new TimeClickListener(position));
        }else {
            holder.itemView.setOnClickListener(new PaymentClickListener(position));
        }
    }

    @Override
    public int getItemCount() {
        int size = 0;
        if(titles!=null&&!titles.isEmpty()){
            size = titles.size();
        }
        return size;
    }

    static class BlueTextListContentHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.blue_text_list_text_view)
        TextView titleTextView;
        public BlueTextListContentHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    private static class TimeClickListener implements View.OnClickListener{
        private final int position;

        TimeClickListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            ListItemClickEvent listItemClickEvent = new ListItemClickEvent();
            switch (position){
                case 0:
                    listItemClickEvent.setId(ListItemClickEvent.SHORT_TIME_LIST_CLICK);
                    break;
                case 1:
                    listItemClickEvent.setId(ListItemClickEvent.THIRTY_MINUTES_LIST_CLICK);
                    break;
                case 2:
                    listItemClickEvent.setId(ListItemClickEvent.FORTY_FIVE_LIST_CLICK);
                    break;
                case 3:
                    listItemClickEvent.setId(ListItemClickEvent.HOUR_LIST_CLICK);
                    break;
            }
            BusProvider.send(listItemClickEvent);
        }
    }

    private static class PaymentClickListener implements View.OnClickListener{
        private final int position;

        PaymentClickListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            ListItemClickEvent listItemClickEvent = new ListItemClickEvent();
            switch (position){
                case 0:
                    listItemClickEvent.setId(ListItemClickEvent.PAY_BY_CARD_LIST_CLICK);
                    break;
                case 1:
                    listItemClickEvent.setId(ListItemClickEvent.PAY_BY_CASH_LIST_CLICK);
                    break;
                case 2:
                    listItemClickEvent.setId(ListItemClickEvent.PAY_BY_PROMO_LIST_CLICK);
                    break;
            }
            BusProvider.send(listItemClickEvent);
        }
    }
}
