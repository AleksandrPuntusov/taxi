package moleculus.isaevkonstantin.skytaxi.components.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.network.NetworkRequestEvent;
import moleculus.isaevkonstantin.skytaxi.presenters.activities.RegistrationActivityPresenter;
import moleculus.isaevkonstantin.skytaxi.components.constants.ArgumentTitles;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpActionView;
import moleculus.isaevkonstantin.skytaxi.components.model.ActivityViewBuilder;
import moleculus.isaevkonstantin.skytaxi.components.service.SkyTaxiService;

public class RegistrationActivity extends AppCompatActivity implements MvpActionView{
    private Intent serviceIntent;

    private RegistrationActivityPresenter presenter;
    private List<ActivityViewBuilder> activityViewBuilders;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_title)
    TextView toolBarTitleTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.main_blue)));
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });
//        getSupportActionBar().setHomeButtonEnabled(true);
        serviceIntent = new Intent(this, SkyTaxiService.class);
        presenter = new RegistrationActivityPresenter();
        presenter.setMvpView(this);
        presenter.init();
        if(savedInstanceState==null){
            SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("SkyTaxiPrefs", Context.MODE_PRIVATE);
            boolean registrationSuccess = sharedPref.getBoolean(ArgumentTitles.REGISTRATION_SUCCESS_SHAR_PREF,false);
            if(!registrationSuccess) {
                presenter.enterPhoneNumber();
            }else {
                boolean firstSettingSuccess = sharedPref.getBoolean(ArgumentTitles.FIRST_SETTINGS_SUCCESS_SHAR_PREF,false);
                if(!firstSettingSuccess){
                    presenter.enterProfile();
                }else {
                    // TODO: 05.05.16 Move to Splash Activity
                    Intent intent = new Intent(this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                            IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    this.startActivity(intent);
                }
            }
        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            getWindow().setStatusBarColor(getResources().getColor(R.color.status_bar_color));
//        }
    }

    @Override
    public void updateUi(ActivityViewBuilder activityViewBuilder) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            toolBarTitleTextView.setText(activityViewBuilder.getActionBarTitle());
            if(activityViewBuilder.getActionBarNavigation()==ActivityViewBuilder.BACK_NAVIGATION){
                actionBar.setDisplayShowHomeEnabled(true);
                actionBar.setHomeButtonEnabled(true);
                actionBar.setDisplayHomeAsUpEnabled(true);
                toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
            }else if(activityViewBuilder.getActionBarNavigation()==ActivityViewBuilder.NOT_NAVIGATION){
                actionBar.setDisplayShowHomeEnabled(false);
                actionBar.setHomeButtonEnabled(false);
                actionBar.setDisplayHomeAsUpEnabled(false);
            }
            if(activityViewBuilder.getColor()>0){
                actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(activityViewBuilder.getColor())));
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.registerSubscriber();
    }



    @Override
    protected void onStop() {
        super.onStop();
        presenter.unRegisterSubscriber();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.destroy();
        presenter = null;
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            this.finish();
        } else {
            getSupportFragmentManager().popBackStack();
        }

    }

    public void loadFromApi(NetworkRequestEvent<Bundle> event) {
        serviceIntent.putExtra(SkyTaxiService.SERVICE_JOB_ID_TITLE, event.getId());
        serviceIntent.putExtras(event.getData());
        startService(serviceIntent);
    }



    public void makeToast(String text){
        Toast.makeText(this,text,Toast.LENGTH_LONG).show();
    }
}
