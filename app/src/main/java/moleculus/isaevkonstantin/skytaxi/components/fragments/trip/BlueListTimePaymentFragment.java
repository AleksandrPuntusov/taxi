package moleculus.isaevkonstantin.skytaxi.components.fragments.trip;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Arrays;

import butterknife.Bind;
import butterknife.ButterKnife;
import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.presenters.fragments.trip.TimePaymentListPresenter;
import moleculus.isaevkonstantin.skytaxi.components.adapters.recycleview.BlueTextRecyclerViewAdapter;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpView;
import moleculus.isaevkonstantin.skytaxi.components.utils.NotScrollLinearLayoutManager;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BlueListTimePaymentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BlueListTimePaymentFragment extends Fragment implements MvpView{
    @Bind(R.id.blue_text_recycler_view)
    RecyclerView blueTextRecyclerView;

    private TimePaymentListPresenter presenter;
    private BlueTextRecyclerViewAdapter adapter;

    public static final int SHORT_TIME = 0;
    public static final int THIRTY_MINUTES_TIME = 1;
    public static final int FORTY_FIVE_MINUTES_TIME = 2;
    public static final int HOUR_TIME = 3;
    public static final int PAY_BY_CREDIT_CARD = 4;
    public static final int PAY_BY_CASH = 5;
    public static final int PAY_BY_PROMO = 6;

    public static final int TIME_LIST_FRAGMENT = 1;
    public static final int PAYMENT_LIST_FRAGMENT = 2;

    private boolean fromOrderDetails;

    public static final String FRAGMENT_LIST_ID_PARAM = "fragmentListIdParam";
    private int listFragmentId;

    public static final String TIME_TITLE_PARAM = "time";

    public BlueListTimePaymentFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment BlueListTimePaymentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BlueListTimePaymentFragment newInstance() {
        BlueListTimePaymentFragment fragment = new BlueListTimePaymentFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            listFragmentId  = getArguments().getInt(FRAGMENT_LIST_ID_PARAM,-1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_time_payment, container, false);
        ButterKnife.bind(this, view);
        NotScrollLinearLayoutManager linearLayoutManager = new NotScrollLinearLayoutManager(getContext());
        linearLayoutManager.setScrollEnabled(false);
        blueTextRecyclerView.setLayoutManager(linearLayoutManager);
        adapter = new BlueTextRecyclerViewAdapter(listFragmentId);
        fillList();
        blueTextRecyclerView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter = new TimePaymentListPresenter();
        presenter.setMvpView(this);
        presenter.init();
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.updateActionBar(listFragmentId);
        presenter.registerSubscriber();

    }

    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void onStop() {
        super.onStop();
        presenter.unRegisterSubscriber();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.destroy();
    }

    private void fillList(){
        String[] values = null;
        if(listFragmentId==TIME_LIST_FRAGMENT){
            values = getContext().getResources().getStringArray(R.array.time_values_array);
        }else if(listFragmentId==PAYMENT_LIST_FRAGMENT){
            values = getContext().getResources().getStringArray(R.array.payment_values_array);
        }
        if(values!=null){
            adapter.addAll(Arrays.asList(values));
        }
    }

}