package moleculus.isaevkonstantin.skytaxi.components.model;

/**
 * Created by Isaev Konstantin on 01.04.16.
 */
public class ActivityViewBuilder {
    public static final int NOT_NAVIGATION = 0;
    public static final int BACK_NAVIGATION = 1;
    public static final int MENU_NAVIGATION = 2;
    public static final int CLOSE_NAVIGATION = 3;

    private String actionBarTitle;
    private int actionBarNavigation;
    private boolean actionBarOver;
    private int color;
    private int menuDrawableId;
    private boolean hideToolBar;
    private boolean hideRightMenu;


    public static ActivityViewBuilder newInstance(){
        return new ActivityViewBuilder();
    }

    public String getActionBarTitle() {
        return actionBarTitle;
    }

    public void setActionBarTitle(String actionBarTitle) {
        this.actionBarTitle = actionBarTitle;
    }

    public int getActionBarNavigation() {
        return actionBarNavigation;
    }

    public void setActionBarNavigation(int actionBarNavigation) {
        this.actionBarNavigation = actionBarNavigation;
    }

    public boolean isActionBarOver() {
        return actionBarOver;
    }

    public void setActionBarOver(boolean actionBarOver) {
        this.actionBarOver = actionBarOver;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public boolean isHideToolBar() {
        return hideToolBar;
    }

    public void setHideToolBar(boolean hideToolBar) {
        this.hideToolBar = hideToolBar;
    }

    public int getMenuDrawableId() {
        return menuDrawableId;
    }

    public void setMenuDrawableId(int menuDrawableId) {
        this.menuDrawableId = menuDrawableId;
    }



    public static ActivityViewBuilder createNavigation(String title, int navigationId){
        ActivityViewBuilder activityViewBuilder = new ActivityViewBuilder();
        activityViewBuilder.setActionBarTitle(title);
        activityViewBuilder.setActionBarNavigation(navigationId);
        return activityViewBuilder;
    }



}
