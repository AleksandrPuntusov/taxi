package moleculus.isaevkonstantin.skytaxi.components.adapters.recycleview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.eventbus.BusProvider;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.ListItemClickEvent;
import moleculus.isaevkonstantin.skytaxi.models.Location;
import moleculus.isaevkonstantin.skytaxi.components.model.RecycleViewHeader;

/**
 * Created by Isaev Konstantin on 23.05.16.
 */
public class StationRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Location> locationList;

    public StationRecyclerViewAdapter() {
        locationList = new ArrayList<>();
    }

    public void addAll(List<Location> tempLocationList){
        locationList.addAll(tempLocationList);
        notifyDataSetChanged();
    }

    public void removeAll(){
        locationList.clear();
        notifyDataSetChanged();
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        RecyclerView.ViewHolder viewHolder = null;
        if(viewType== RecycleViewHeader.NAV_DRAWER_SIMPLE_HEADER) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.navigation_view_text_item,parent,false);
            viewHolder = new LocationsContentHolder(view);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Location location = locationList.get(position);
        if(location.isStation()){
            switch (location.getStationId()){
                case Location.AIR_PORTS:
                    ((LocationsContentHolder)holder).navItemTitleTextView.setText(location.getName());
                    ((LocationsContentHolder)holder).navItemLogoImageView.setImageResource(R.drawable.air_bus);
                    break;
                case Location.RAILS_STATION:
                    ((LocationsContentHolder)holder).navItemTitleTextView.setText(location.getName()+" вокзал");
                    ((LocationsContentHolder)holder).navItemLogoImageView.setImageResource(R.drawable.raills_icon);
                    break;
            }
        }
        ((LocationsContentHolder)holder).dividerView.setVisibility(View.INVISIBLE);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ListItemClickEvent<Location> listItemClickEvent = new ListItemClickEvent<Location>();
                listItemClickEvent.setId(ListItemClickEvent.LOCATION_LIST_CLICK);
                listItemClickEvent.setData(location);
                BusProvider.send(listItemClickEvent);

            }
        });
    }

    @Override
    public int getItemCount() {
        int size = 0;
        if(locationList !=null&&!locationList.isEmpty()){
            size = locationList.size();
        }
        return size;
    }

    static class LocationsContentHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.nav_item_title_text_view)
        TextView navItemTitleTextView;
        @Bind(R.id.nav_item_logo_image_view)
        ImageView navItemLogoImageView;
        @Bind(R.id.simple_text_divider)
        View dividerView;
        public LocationsContentHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
