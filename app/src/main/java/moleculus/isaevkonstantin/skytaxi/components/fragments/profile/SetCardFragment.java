package moleculus.isaevkonstantin.skytaxi.components.fragments.profile;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import butterknife.ButterKnife;
import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpActionView;
import moleculus.isaevkonstantin.skytaxi.components.model.ActivityViewBuilder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SetCardFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SetCardFragment extends Fragment {


    public SetCardFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SetCardFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SetCardFragment newInstance() {
        SetCardFragment fragment = new SetCardFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_set_card, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        updateActionBar();
    }

    public void updateActionBar(){
        ActivityViewBuilder activityViewBuilder = new ActivityViewBuilder();
        String actionBarTitle;
        actionBarTitle = getContext().getResources().getString(R.string.set_card_action_bar_title);
        activityViewBuilder.setActionBarTitle(actionBarTitle);
        activityViewBuilder.setActionBarNavigation(ActivityViewBuilder.BACK_NAVIGATION);
        activityViewBuilder.setColor(R.color.white);
        ((MvpActionView)getActivity()).updateUi(activityViewBuilder);
    }
}
