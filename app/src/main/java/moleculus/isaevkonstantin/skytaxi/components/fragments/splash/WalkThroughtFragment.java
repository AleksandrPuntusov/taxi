package moleculus.isaevkonstantin.skytaxi.components.fragments.splash;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.viewpagerindicator.CirclePageIndicator;

import java.lang.reflect.Field;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.components.adapters.viewpager.WalkCloudsPagerAdapter;
import moleculus.isaevkonstantin.skytaxi.components.utils.ViewPagerScroller;
import moleculus.isaevkonstantin.skytaxi.presenters.fragments.WalkThroughtPresenter;
import moleculus.isaevkonstantin.skytaxi.components.adapters.viewpager.WalkThroughPagerAdapter;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WalkThroughtFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WalkThroughtFragment extends Fragment implements MvpView{
    private WalkThroughtPresenter presenter;

    @Bind(R.id.walk_through_view_pager)
    ViewPager walkThroughViewPager;
    @Bind(R.id.walk_through_clouds_view_pager)
    ViewPager walkThroughCloudsViewPager;
    @Bind(R.id.call_taxi_layout)
    LinearLayout callTaxiLayout;
    @Bind(R.id.circle_pager_indicator)
    CirclePageIndicator circlePageIndicator;


    private WalkThroughPagerAdapter viewPagerAdapter;
    private WalkCloudsPagerAdapter cloudsPagerAdapter;

    public WalkThroughtFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment WalkThroughtFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static WalkThroughtFragment newInstance() {
        WalkThroughtFragment fragment = new WalkThroughtFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_walk_throught, container, false);
        ButterKnife.bind(this,view);
        viewPagerAdapter = new WalkThroughPagerAdapter(getChildFragmentManager());
        cloudsPagerAdapter = new WalkCloudsPagerAdapter(getChildFragmentManager());
        fillViewPagerAdapter();
        walkThroughViewPager.setAdapter(viewPagerAdapter);
        walkThroughCloudsViewPager.setAdapter(cloudsPagerAdapter);
        walkThroughViewPager.setPageMargin(0);
        circlePageIndicator.setViewPager(walkThroughViewPager);
        walkThroughViewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                walkThroughCloudsViewPager.onTouchEvent(event);
                return false;
            }
        });

        walkThroughCloudsViewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                walkThroughViewPager.onTouchEvent(event);
                return false;
            }
        });
        changePagerScroller();
        return view;
    }



    private void fillViewPagerAdapter(){
        String[] headers = getResources().getStringArray(R.array.walk_headers);
        String[] descriptions = getResources().getStringArray(R.array.walk_descriptions);
        int[] logoImagesId = {R.drawable.hello_baloon_walk,R.drawable.cost_walk,R.drawable.payment_walk};
        int[] backgroundImagesId = {R.drawable.hello_walk_clouds,R.drawable.walk_cost,R.drawable.payment_clouds};
        viewPagerAdapter.init(headers,descriptions,logoImagesId,backgroundImagesId);
        cloudsPagerAdapter.init(backgroundImagesId);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter = new WalkThroughtPresenter();
        presenter.setMvpView(this);
        presenter.init();
    }

    @OnClick(R.id.call_taxi_layout)
    public void showRegistrationActivity(){

        presenter.startRegistrationActivity();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.destroy();
    }

    private void changePagerScroller() {
        try {
            Field mScroller = null;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            ViewPagerScroller scroller = new ViewPagerScroller(walkThroughCloudsViewPager.getContext());
            mScroller.set(walkThroughCloudsViewPager, scroller);
        } catch (Exception e) {
        }
    }
}
