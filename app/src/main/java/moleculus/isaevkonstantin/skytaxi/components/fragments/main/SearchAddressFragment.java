
package moleculus.isaevkonstantin.skytaxi.components.fragments.main;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.models.Location;
import moleculus.isaevkonstantin.skytaxi.presenters.fragments.main.SearchAddressPresenter;
import moleculus.isaevkonstantin.skytaxi.components.adapters.recycleview.AddressesRecycleViewAdapter;
import moleculus.isaevkonstantin.skytaxi.components.constants.ArgumentTitles;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpActionView;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpView;
import moleculus.isaevkonstantin.skytaxi.components.model.ActivityViewBuilder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SearchAddressFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchAddressFragment extends Fragment implements MvpView{
    @Bind(R.id.search_on_map_layout)
    LinearLayout searchOnMapLayout;
    @Bind(R.id.search_in_favorites_layout)
    LinearLayout searchInFavoritesLayout;
    @Bind(R.id.search_in_airports_layout)
    LinearLayout searchInAirportsLayout;
    @Bind(R.id.search_address_edit_text)
    EditText searchAddressEditText;
    @Bind(R.id.address_recycle_view)
    RecyclerView addressRecycleView;
    @Bind(R.id.route_icon_text_view)
    TextView routeTextView;


    public static final int PATH_FROM = 1;
    public static final int PATH_TO = 2;
    private boolean searched;
    private boolean setTitleFromListClick;
    private int searchAddressId;
    private boolean searchAddresFromMap;
    private AddressesRecycleViewAdapter adapter;
    private SearchAddressPresenter presenter;


    public SearchAddressFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SearchAddressFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchAddressFragment newInstance() {
        SearchAddressFragment fragment = new SearchAddressFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            searchAddressId = getArguments().getInt(ArgumentTitles.SEARCH_ADDRESS_ID_PARAM);
            searchAddresFromMap = getArguments().getBoolean(ArgumentTitles.SEARCH_ADDRESS_FROM_MAP);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search_address, container, false);
        ButterKnife.bind(this,view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        addressRecycleView.setLayoutManager(linearLayoutManager);
        adapter = new AddressesRecycleViewAdapter();
        addressRecycleView.setAdapter(adapter);
        searchAddressEditText.requestFocus();
        openKeyBoard(true);
        searchAddressEditText.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        searchAddressEditText.addTextChangedListener(new TextWatcher() {
            int deteTedCount = 0;
            String lastWord = "";
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()==0){
                    if(!searchAddresFromMap){
                        searchOnMapLayout.setVisibility(View.VISIBLE);
                    }
                    searchInFavoritesLayout.setVisibility(View.VISIBLE);
                    searchInAirportsLayout.setVisibility(View.VISIBLE);
                    adapter.removeAll();
                }else {
                    searchOnMapLayout.setVisibility(View.GONE);
                    searchInFavoritesLayout.setVisibility(View.GONE);
                    searchInAirportsLayout.setVisibility(View.GONE);
                    if(s.length()>3){
                        if(!searched){
                            if(!setTitleFromListClick){
                                if(!adapter.isCheckedLocation()){
                                    searched = true;
                                    presenter.searchAddressByTitle(s.toString());
                                }else {
                                    if(deteTedCount>2){
                                        adapter.setCheckedLocation(false);
                                        deteTedCount=0;
                                    }
                                }
                            }else {
                                setTitleFromListClick = false;
                            }
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(lastWord.length()>s.length()){
                    deteTedCount++;
                }
                lastWord = s.toString();

            }
        });
        searchAddressEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    searchAddressEditText.setHintTextColor(getResources().getColor(R.color.gray));
                }else {
                    searchAddressEditText.setHintTextColor(getResources().getColor(R.color.black));

                }
            }
        });
        if(searchAddressId== PATH_FROM){
            routeTextView.setText("A");
            searchAddressEditText.setHint(getResources().getString(R.string.search_address_from_edit_text_hint));
        }else {
            routeTextView.setText("B");
            searchAddressEditText.setHint(getResources().getString(R.string.search_address_to_edit_text_hint));
        }
        if(searchAddresFromMap){
            searchOnMapLayout.setVisibility(View.GONE);
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter = new SearchAddressPresenter();
        presenter.setMvpView(this);
        presenter.setPathId(searchAddressId);
        presenter.init();
        presenter.getAllLocation(0);

    }

    @Override
    public void onStart() {
        super.onStart();
        updateActionBar();
        presenter.registerSubscriber();
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.unRegisterSubscriber();
        openKeyBoard(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.destroy();
    }

    @OnClick(R.id.search_on_map_layout)
    public void searchOnMap(){
        presenter.searchOnMap();
    }
    @OnClick(R.id.search_in_favorites_layout)
    public void searchInFavorites(){
        presenter.searchInFavorites();
    }
    @OnClick(R.id.search_in_airports_layout)
    public void searchInAirports(){
        presenter.searchInAirports();
    }

    public void updateAddressList(List<Location> addressList){
        if (addressList != null) {
            adapter.removeAll();
            adapter.addAll(addressList);
        }
    }

    public void setCheckedLocation(Location location){
        adapter.setCheckedLocation(true);
        String[] address = location.getName().split(",");
        String street = address[address.length-1];
        setTitleFromListClick = true;
        searchAddressEditText.setText(street);
    }

    public void searchAddressSuccess(List<Location> addressList){
        searched = false;
        updateAddressList(addressList);
    }

    private void updateActionBar(){
        String title;
        if(searchAddressId==SearchAddressFragment.PATH_FROM){
            title = getContext().getResources().getString(R.string.map_from_action_bar_title);
        }else {
            title = getContext().getResources().getString(R.string.map_to_action_bar_title);
        }
        ActivityViewBuilder activityViewBuilder = ActivityViewBuilder.createNavigation(title,ActivityViewBuilder.BACK_NAVIGATION);
        activityViewBuilder.setColor(R.color.white);
        activityViewBuilder.setMenuDrawableId(0);
        ((MvpActionView)getActivity()).updateUi(activityViewBuilder);
    }



    public void openKeyBoard(boolean open) {
        InputMethodManager imm = (InputMethodManager)
                getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if(open){
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.SHOW_FORCED);
        }else {
            if(imm.isActive()){
                if(searchAddressEditText!=null){
                    searchAddressEditText.clearFocus();
                }
                imm.hideSoftInputFromWindow(getView().getWindowToken(),0);
            }
        }

    }
}
