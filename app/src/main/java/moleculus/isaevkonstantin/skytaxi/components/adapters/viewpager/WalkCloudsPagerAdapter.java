package moleculus.isaevkonstantin.skytaxi.components.adapters.viewpager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import moleculus.isaevkonstantin.skytaxi.components.fragments.splash.WalkCloudFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.splash.WalkDetailFragment;

/**
 * Created by Isaev Konstantin on 31.05.16.
 */
public class WalkCloudsPagerAdapter extends FragmentPagerAdapter {
    private int[] backgroundImagesid;
    private static final int PAGE_COUNTER = 3;
    public WalkCloudsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void init(int[] backgroundImagesId){
        this.backgroundImagesid = backgroundImagesId;
    }

    @Override
    public Fragment getItem(int position) {
        return WalkCloudFragment.newInstance(backgroundImagesid[position]);
    }

    @Override
    public int getCount() {
        return PAGE_COUNTER;
    }
}
