package moleculus.isaevkonstantin.skytaxi.components.adapters.viewpager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import moleculus.isaevkonstantin.skytaxi.components.fragments.splash.WalkDetailFragment;

/**
 * Created by Isaev Konstantin on 10.04.16.
 */
public class WalkThroughPagerAdapter extends FragmentPagerAdapter{
    private String[] headers;
    private String[] descriptions;
    private int[] logoImagesId;
    private static final int PAGE_COUNTER = 3;
    public WalkThroughPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void init(String[] headers,String[] descriptions,int[] logoImagesId,int[] backgroundImagesId){
        this.headers = headers;
        this.descriptions = descriptions;
        this.logoImagesId = logoImagesId;
    }

    @Override
    public Fragment getItem(int position) {
        return WalkDetailFragment.newInstance(headers[position], descriptions[position], logoImagesId[position]
                );
    }

    @Override
    public int getCount() {
        return PAGE_COUNTER;
    }

}
