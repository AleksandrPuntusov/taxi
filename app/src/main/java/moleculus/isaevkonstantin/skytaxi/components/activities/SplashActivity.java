package moleculus.isaevkonstantin.skytaxi.components.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import butterknife.ButterKnife;
import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.presenters.activities.SplashActivityPresenter;
import moleculus.isaevkonstantin.skytaxi.components.constants.ArgumentTitles;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpActionView;
import moleculus.isaevkonstantin.skytaxi.components.model.ActivityViewBuilder;

public class SplashActivity extends AppCompatActivity implements MvpActionView{
    private SplashActivityPresenter presenter;
    public  static final String WALK_THROUGHT_TITLE = "walkThroughtTitle";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        presenter = new SplashActivityPresenter();
        presenter.setMvpView(this);
        presenter.init();
        if (savedInstanceState == null) {
            presenter.createSplash();
            final Runnable runnable = new Runnable()
            {
                public void run()
                {
                    SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("SkyTaxiPrefs", Context.MODE_PRIVATE);
                    boolean walk = sharedPref.getBoolean(WALK_THROUGHT_TITLE,false);
                    if(!walk) {
                        presenter.createWalkThrought();
                    }else {
                        boolean registrationSuccess = sharedPref.getBoolean(ArgumentTitles.REGISTRATION_SUCCESS_SHAR_PREF,false);
                        boolean firstSettingSuccess = sharedPref.getBoolean(ArgumentTitles.FIRST_SETTINGS_SUCCESS_SHAR_PREF,false);
                        if(registrationSuccess&&firstSettingSuccess) {
                            presenter.startMainActivity();
                        }else {
                            presenter.startRegistrationActivity();

                        }
                    }
                }
            };
            Handler handler = new Handler();
            handler.postDelayed(runnable, 800);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.status_bar_color));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.registerSubscriber();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.unRegisterSubscriber();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.destroy();
    }

    @Override
    public void updateUi(ActivityViewBuilder activityViewBuilder) {

    }

    @Override
    public void makeToast(String text) {

    }
}
