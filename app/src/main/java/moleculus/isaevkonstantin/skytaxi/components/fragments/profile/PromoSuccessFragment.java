package moleculus.isaevkonstantin.skytaxi.components.fragments.profile;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpActionView;
import moleculus.isaevkonstantin.skytaxi.components.model.ActivityViewBuilder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PromoSuccessFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PromoSuccessFragment extends Fragment {
    public PromoSuccessFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PromoSuccessFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PromoSuccessFragment newInstance() {
        PromoSuccessFragment fragment = new PromoSuccessFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_promo_success, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    private void updateActionBar(){
        String title = getResources().getString(R.string.enter_promo_action_bar_title);

        ActivityViewBuilder activityViewBuilder = ActivityViewBuilder.createNavigation(title,ActivityViewBuilder.BACK_NAVIGATION);
        activityViewBuilder.setColor(R.color.white);
        activityViewBuilder.setMenuDrawableId(0);
        ((MvpActionView)getActivity()).updateUi(activityViewBuilder);
    }

    @Override
    public void onStart() {
        super.onStart();
        updateActionBar();
    }
}
