package moleculus.isaevkonstantin.skytaxi.components.fragments.splash;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import moleculus.isaevkonstantin.skytaxi.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WalkDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WalkDetailFragment extends Fragment {
    private String header;
    private String description;
    private int logoImageId;
    private static final String LOGO_IMAGE_ID = "imageId";

    private int backgroundImageId;

    private static final String HEADER = "header";
    private static final String DESCRIPTION = "description";
    @Bind(R.id.walk_header_text_view)
    TextView headerTextView;
    @Bind(R.id.walk_description_text_view)
    TextView descriptionTextView;
    @Bind(R.id.walk_logo_image_view)
    ImageView logoImageView;


    public WalkDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     *
     * @return A new instance of fragment WalkDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static WalkDetailFragment newInstance(String header,String description,
                                                 int imageId) {
        WalkDetailFragment fragment = new WalkDetailFragment();
        Bundle args = new Bundle();
        args.putString(HEADER,header);
        args.putString(DESCRIPTION,description);
        args.putInt(LOGO_IMAGE_ID,imageId);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            header = getArguments().getString(HEADER);
            description = getArguments().getString(DESCRIPTION);
            logoImageId = getArguments().getInt(LOGO_IMAGE_ID);

        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_walk_detail, container, false);
        ButterKnife.bind(this,view);
        headerTextView.setText(header);
        descriptionTextView.setText(description);
        logoImageView.setImageDrawable(getResources().getDrawable(logoImageId));
        return view;
    }

}
