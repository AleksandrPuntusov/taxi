package moleculus.isaevkonstantin.skytaxi.components.fragments.trip;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.models.Order;
import moleculus.isaevkonstantin.skytaxi.presenters.fragments.trip.TripDetailsPresenter;
import moleculus.isaevkonstantin.skytaxi.components.fragments.main.SearchAddressFragment;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpActionView;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpView;
import moleculus.isaevkonstantin.skytaxi.components.model.ActivityViewBuilder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TripDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TripDetailsFragment extends Fragment implements MvpView{
    @Bind(R.id.get_cost_button)
    Button getCostButton;

    @Bind(R.id.order_location_from_text_view)
    TextView orderLocationFromTextView;
    @Bind(R.id.order_location_to_text_view)
    TextView orderLocationToTextView;
    @Bind(R.id.order_time_text_view)
    TextView orderTime;
    private TripDetailsPresenter presenter;
    public TripDetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TripDetailsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TripDetailsFragment newInstance() {
        TripDetailsFragment fragment = new TripDetailsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_trip_details, container, false);
        ButterKnife.bind(this,view);
        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter = new TripDetailsPresenter();
        presenter.setMvpView(this);
        presenter.init();
        presenter.getCurentOrder();
    }

    @Override
    public void onStart() {
        super.onStart();
        updateActionBar();
        presenter.registerSubscriber();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.unRegisterSubscriber();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.destroy();
    }
    public void updateActionBar(){
        String title = getContext().getResources().getString(R.string.trip_details_action_bar_title);
        ActivityViewBuilder activityViewBuilder = ActivityViewBuilder.createNavigation(title,ActivityViewBuilder.MENU_NAVIGATION);
        activityViewBuilder.setColor(R.color.white);
        ((MvpActionView)getActivity()).updateUi(activityViewBuilder);
    }

    @OnClick(R.id.get_cost_button)
    void getCostButtonClick(){
        presenter.getTripCost();
    }

    @OnClick(R.id.trip_settings_text_view)
    void getTripSettingsTetxViewClick(){
        presenter.getTripSettings();
    }

    @OnClick(R.id.order_time_text_view)
    public void orderTimeClick(){
        presenter.setOrderTime();
    }

    @OnClick(R.id.order_location_from_text_view)
    public void orderLocationFromClick(){
        presenter.setLocation(SearchAddressFragment.PATH_FROM);
    }
    @OnClick(R.id.order_location_to_text_view)
    public void orderLocationToClick(){
        presenter.setLocation(SearchAddressFragment.PATH_TO);
    }

    public void updateOrder(Order order){
        if(order!=null){
            if(order.getLocationFrom()!=null){
                updateTextFielt(order.getLocationFrom().getName(),orderLocationFromTextView);
            }
            if(order.getLocationTo()!=null){
                updateTextFielt(order.getLocationTo().getName(),orderLocationToTextView);
            }
            String[] times = getContext().getResources().getStringArray(R.array.time_values_array);
            String time;
            switch (order.getTimeId()){
                case Order.START_SHORT_TIME:
                    time = times[0];
                    break;
                case Order.START_THIRTY_MINUTES:

                    time = times[1];
                    break;
                case Order.START_FORTY_FIVE_MINUTES:
                    time = times[2];
                    break;
                case Order.START_HOUR:
                    time = times[3];
                    break;
                default:
                    time = "";
            }
            updateTextFielt(time,orderTime);
        }else {

        }
    }

    private void updateTextFielt(String source,TextView textView){
        if (source!=null&&!source.isEmpty()){
            textView.setText(source);
        }
    }
}
