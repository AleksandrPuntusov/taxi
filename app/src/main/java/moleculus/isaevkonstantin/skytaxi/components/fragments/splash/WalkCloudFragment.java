package moleculus.isaevkonstantin.skytaxi.components.fragments.splash;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import butterknife.Bind;
import butterknife.ButterKnife;
import moleculus.isaevkonstantin.skytaxi.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WalkCloudFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WalkCloudFragment extends Fragment {
    @Bind(R.id.cloud_image_view)
    ImageView backgroundImageView;
    private static final String BACKGROUND_IMAGE_ID = "backgroundImageId";


    private int backgroundImageId;
    public WalkCloudFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment WalkCloudFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static WalkCloudFragment newInstance(int imageId) {
        WalkCloudFragment fragment = new WalkCloudFragment();
        Bundle args = new Bundle();
        args.putInt(BACKGROUND_IMAGE_ID,imageId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            backgroundImageId = getArguments().getInt(BACKGROUND_IMAGE_ID);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_walk_cloud, container, false);
        ButterKnife.bind(this,view);
        backgroundImageView.setImageDrawable(getResources().getDrawable(backgroundImageId));

        return view;
    }

}
