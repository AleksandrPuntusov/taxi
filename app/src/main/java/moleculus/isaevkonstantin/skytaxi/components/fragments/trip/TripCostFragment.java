package moleculus.isaevkonstantin.skytaxi.components.fragments.trip;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpActionView;
import moleculus.isaevkonstantin.skytaxi.components.model.ActivityViewBuilder;
import moleculus.isaevkonstantin.skytaxi.models.Order;
import moleculus.isaevkonstantin.skytaxi.models.Trip;
import moleculus.isaevkonstantin.skytaxi.presenters.fragments.trip.TripCostPresenter;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpView;
import moleculus.isaevkonstantin.skytaxi.components.loaders.OrderLoader;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TripCostFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TripCostFragment extends Fragment implements MvpView,LoaderManager.LoaderCallbacks {
    @Bind(R.id.tariff_optima_button)
    Button tariffOptimaButton;
    @Bind(R.id.tariff_promo_button)
    Button tariffPromoButton;
    public static final int OPTIMA_TARIFF = 1;
    public static final int PROMO_TARIFF = 2;

    public static final int ORDER_LOADER_ID = 1;


    private TripCostPresenter presenter;
    public TripCostFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TripCostFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TripCostFragment newInstance() {
        TripCostFragment fragment = new TripCostFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_trip_cost, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter = new TripCostPresenter();
        presenter.setMvpView(this);
        presenter.init();
        getLoaderManager().initLoader(ORDER_LOADER_ID, getArguments(), this);
    }

    @Override
    public void onStart() {
        super.onStart();
        updateActionBar();
    }

    public void updateActionBar(){
        String title;
        title = getContext().getResources().getString(R.string.cost_action_bar_title);
        ActivityViewBuilder activityViewBuilder = ActivityViewBuilder.createNavigation(title,ActivityViewBuilder.BACK_NAVIGATION);
        activityViewBuilder.setColor(R.color.white);
        ((MvpActionView)getActivity()).updateUi(activityViewBuilder);
    }
    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.destroy();
        presenter=null;
    }

    @OnClick(R.id.tariff_optima_button)
    public void optimaButtonClick(){
        presenter.setTariff(OPTIMA_TARIFF);
    }
    @OnClick(R.id.tariff_promo_button)
    public void promoButtonClick(){
        presenter.setTariff(PROMO_TARIFF);

    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        Loader loader = null;
        if (id == ORDER_LOADER_ID) {
            loader =  new OrderLoader(getContext(),OrderLoader.LOAD_CURRENT_ORDER);
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        Order order = (Order)data;
        if(order!=null){
            List<Trip> trips = order.getTrips();
            if(trips!=null&&!trips.isEmpty()){
                for (int i = 0; i < trips.size(); i++) {
                    Trip trip = trips.get(i);
                    if(trip.getName().equalsIgnoreCase("Тариф Оптима")){
                        tariffOptimaButton.setText("Поехать за "+ trip.getCost());
                    }else {
                        tariffPromoButton.setText("Поехать за "+ trip.getCost());

                    }
                }
            }
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }
}
