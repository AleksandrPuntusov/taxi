package moleculus.isaevkonstantin.skytaxi.components.adapters.viewpager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import moleculus.isaevkonstantin.skytaxi.components.fragments.main.AirPortsListFragment;

/**
 * Created by Isaev Konstantin on 17.04.16.
 */
public class AirPortViewPagerAdapter extends FragmentPagerAdapter {
    private static final String[] titles = {"Аэропорты","ЖД вокзалы"};
    private static final int PAGE_COUNTER = 2;
    private int pathId;
    private boolean forwardFromDetails;

    public AirPortViewPagerAdapter(FragmentManager fm,int pathId,boolean forwardFromDetails) {
        super(fm);
        this.pathId = pathId;
        this.forwardFromDetails = forwardFromDetails;
    }


    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                fragment = AirPortsListFragment.newInstance(AirPortsListFragment.AIR_PORTS,pathId,forwardFromDetails);
                break;
            case 1:
                fragment = AirPortsListFragment.newInstance(AirPortsListFragment.RAILS_STATIONS,pathId,forwardFromDetails);
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return PAGE_COUNTER;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
