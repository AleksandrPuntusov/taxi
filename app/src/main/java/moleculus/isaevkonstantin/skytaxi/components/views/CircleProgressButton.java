package moleculus.isaevkonstantin.skytaxi.components.views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.util.AttributeSet;

import com.dd.morphingbutton.IProgress;
import com.dd.morphingbutton.MorphingButton;

/**
 * Created by Isaev Konstantin on 05.05.16.
 */
public class CircleProgressButton extends MorphingButton implements IProgress {
    public static final int MAX_PROGRESS = 100;
    public static final int MIN_PROGRESS = 0;
    private int mProgress;
    private int mProgressColor;
    private int mProgressCornerRadius;
    private Paint paint;
    public CircleProgressButton(Context context) {
        super(context);
        init();
    }

    public CircleProgressButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    public CircleProgressButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();

    }



    private void init(){
        paint = new Paint();
        paint.setColor(Color.WHITE);
    }

    public void morphToProgress(int color, int progressColor, int progressCornerRadius, int width, int height, int duration) {
        this.mProgressCornerRadius = progressCornerRadius;
        this.mProgressColor = progressColor;
        Params longRoundedSquare = Params.create().duration(duration).
                cornerRadius(this.mProgressCornerRadius).width(width).height(height).color(color).colorPressed(color);
        this.morph(longRoundedSquare);
    }

    @Override
    public void setProgress(int progress) {
        this.mProgress = progress;
        this.invalidate();
    }

    public void morph(@NonNull Params params) {
        super.morph(params);
        this.mProgress = 0;
        this.paint = null;
    }
}
