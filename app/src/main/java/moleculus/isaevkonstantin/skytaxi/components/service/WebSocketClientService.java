package moleculus.isaevkonstantin.skytaxi.components.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import moleculus.isaevkonstantin.skytaxi.eventbus.BusProvider;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.UpdateUiEvent;
import moleculus.isaevkonstantin.skytaxi.interactors.OrderInteractor;
import moleculus.isaevkonstantin.skytaxi.interactors.UserInteractor;
import moleculus.isaevkonstantin.skytaxi.models.Driver;
import moleculus.isaevkonstantin.skytaxi.models.Order;
import moleculus.isaevkonstantin.skytaxi.models.User;

public class WebSocketClientService extends Service {
    private static final String WS_SERVER = "http://178.62.197.213/api/v1/";
//    private static final String WS_SERVER = "http://139.59.191.245";
    private Socket socket;
    private User user;
    private UserInteractor userInteractor;
    private OrderInteractor orderInteractor;
    public WebSocketClientService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return new WebSocketBinder();
    }

    public class WebSocketBinder extends Binder {
        public WebSocketClientService getService() {
            // Return this instance of LocalService so clients can call public methods
            return WebSocketClientService.this;
        }
    }


    @Override
    public void onCreate() {
        super.onCreate();
        userInteractor = new UserInteractor(this.getApplicationContext());
        orderInteractor = new OrderInteractor(this.getApplicationContext());
        user = userInteractor.getUserFromDb(0l);
    }

    public void connectToSocket() throws URISyntaxException {
        IO.Options opts = new IO.Options();
        opts.forceNew = true;
        opts.secure = false;
        opts.reconnection = true;
        socket = IO.socket(WS_SERVER,opts);
        if(user!=null){
            socket.on("connect", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("id",user.getServerId());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    socket.emit("auth",obj);
                }
            });
            socket.on("drivers.updated", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    JSONObject obj = (JSONObject)args[0];
                    int i = 0;
                    i++;
                }
            });
            socket.on("order.updated", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    for (int i = 0; i < args.length; i++) {
                        JSONObject orderJson = (JSONObject)args[0];
                        String orderId = null;
                        try {
                            orderId = orderJson.getJSONObject("order").getString("id");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(orderId!=null){
                            Order order = orderInteractor.getOrderByServerIdFromDb(orderId);
                            try {
                                int status = orderJson.getJSONObject("order").getInt("status");;
                                if(order.getStatus()!=status){
                                    order.setStatus(status);
                                    orderInteractor.updateOrderInDb(order);
                                    UpdateUiEvent updateUiEvent = new UpdateUiEvent();
                                    updateUiEvent.setId(UpdateUiEvent.UPDATE_ORDER);
                                    BusProvider.send(updateUiEvent);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });
            socket.on("order.driver", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    JSONObject obj = (JSONObject) args[0];
                    Gson gson = new Gson();
                    JsonParser jsonParser = new JsonParser();
                    JsonObject gsonObject = (JsonObject)jsonParser.parse(obj.toString());
                    Driver driver = gson.fromJson(gsonObject.getAsJsonObject("driver"),Driver.class);
                    String  orderId = gsonObject.getAsJsonObject("order").get("id").getAsString();
                    Order order = orderInteractor.getOrderByServerIdFromDb(orderId);
                    if(order!=null){
                        if(driver!=null) {
                            orderInteractor.updateDriverInDb(driver);
                            order.setDriver(driver);
                            order.setDriverFk(driver.getId());
                            orderInteractor.updateOrderInDb(order);
                            UpdateUiEvent updateUiEvent = new UpdateUiEvent();
                            updateUiEvent.setId(UpdateUiEvent.UPDATE_ORDER);
                            BusProvider.send(updateUiEvent);
                        }
                    }
                }
            });
            socket.connect();

        }

    }

    public void disconnectFromSocket(){
        if(socket.connected()){
            socket.disconnect();
        }
    }

}
