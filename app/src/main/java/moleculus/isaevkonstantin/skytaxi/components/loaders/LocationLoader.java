package moleculus.isaevkonstantin.skytaxi.components.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import java.util.List;

import moleculus.isaevkonstantin.skytaxi.interactors.LocationInteractor;
import moleculus.isaevkonstantin.skytaxi.models.Location;

/**
 * Created by Isaev Konstantin on 12.05.16.
 */
public class LocationLoader extends AsyncTaskLoader<List<Location>> {
    public static final int LOAD_RAILS_STATION = 1;
    public static final int LOAD_AIR_PORTS = 2;
    public static final int LOAD_FAVORITES = 3;

    private int jobId;
    private LocationInteractor locationInteractor;

    public LocationLoader(Context context,int jobId) {
        super(context);
        locationInteractor = new LocationInteractor(context);
        this.jobId = jobId;
        onContentChanged();
    }

    @Override
    public void forceLoad() {
        super.forceLoad();
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        if (takeContentChanged())
            forceLoad();
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
    }

    @Override
    public void deliverResult(List<Location> data) {
        super.deliverResult(data);
    }

    @Override
    public List<Location> loadInBackground() {
        switch (jobId){
            case LOAD_RAILS_STATION:
                return locationInteractor.getAllStationsByTypeDb(Location.RAILS_STATION);
            case LOAD_AIR_PORTS:
                return locationInteractor.getAllStationsByTypeDb(Location.AIR_PORTS);
            case LOAD_FAVORITES:
                break;

        }
        return null;
    }
}
