package moleculus.isaevkonstantin.skytaxi.components.constants;

/**
 * Created by Isaev Konstantin on 13.05.16.
 */
public interface ArgumentTitles {

    String AUTH_TOKEN = "AuthTokenParam";
    String PHOTO_URI= "photoUriParam";

    String REGISTRATION_SUCCESS_SHAR_PREF = "RegistrationSuccessParam";
    String FIRST_SETTINGS_SUCCESS_SHAR_PREF = "FirstSettingsParam";

    String ENTER_DATA_TEXT = "enterDataParam";
    String ENTER_DATA_ID = "enterDataIdParam";
    String PHONE = "phoneParam";
    String SMS_CODE = "smsCodeParam";
    String LOCATION_FROM = "locationFromParam";
    String LOCATION_TO = "locationToParam";
    String CURRENT_ORDER = "currentOrderParam";
    String SEARCH_ADDRESS_FROM_MAP = "searchAddressFromMap";
    String SEARCH_ADDRESS_ID_PARAM = "searchAddressTitle";
    String LOCATION_TITLE_PARAM = "locationTitle";
    String LOCATION_LIST_FOR_AIR_PORTS_PARAM = "locationTitle";
    String PATH_ID_TITLE_PARAM = "pathIdTitleParam";

    String FORWARD_FROM_ORDER_DETAILS = "forwardFromOrderDetails";
    String TARIF_ID = "tariffId";
    String STREET_ID = "streetId";
}
