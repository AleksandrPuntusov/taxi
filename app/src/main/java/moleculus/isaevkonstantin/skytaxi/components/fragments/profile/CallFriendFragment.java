package moleculus.isaevkonstantin.skytaxi.components.fragments.profile;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.presenters.fragments.profile.CallFriendPresenter;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CallFriendFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CallFriendFragment extends Fragment implements MvpView{
    private CallFriendPresenter presenter;

    public CallFriendFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CallFriendFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CallFriendFragment newInstance() {
        CallFriendFragment fragment = new CallFriendFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_call_friend, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter = new CallFriendPresenter();
        presenter.setMvpView(this);
        presenter.init();
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.updateActionBar();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.destroy();
    }

    @OnClick(R.id.share_button)
    public void callFrinedButtonClick(){
        presenter.shareInSocial();
    }
}
