package moleculus.isaevkonstantin.skytaxi.components.utils;

import android.text.TextPaint;
import android.text.style.URLSpan;

/**
 * Created by Isaev Konstantin on 13.04.16.
 */
public class UrlSpanNoUnderline extends URLSpan {

    public UrlSpanNoUnderline(String url) {
        super(url);
    }
    @Override public void updateDrawState(TextPaint ds) {
        super.updateDrawState(ds);
        ds.setUnderlineText(false);
    }

}
