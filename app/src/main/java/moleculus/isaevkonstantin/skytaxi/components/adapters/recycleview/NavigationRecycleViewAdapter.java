package moleculus.isaevkonstantin.skytaxi.components.adapters.recycleview;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.eventbus.BusProvider;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.ListItemClickEvent;
import moleculus.isaevkonstantin.skytaxi.components.model.DrawerItem;
import moleculus.isaevkonstantin.skytaxi.components.model.RecycleViewHeader;

/**
 * Created by Isaev Konstantin on 16.04.16.
 */
public class NavigationRecycleViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<DrawerItem> drawerItems;

    public NavigationRecycleViewAdapter() {
        drawerItems = new ArrayList<>();
    }

    public void add(DrawerItem item){
        drawerItems.add(item);
        notifyDataSetChanged();
    }

    public void insertByPosition(int position,DrawerItem item){
        drawerItems.add(position,item);
        notifyDataSetChanged();
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        RecyclerView.ViewHolder viewHolder = null;
        if(viewType==RecycleViewHeader.NAV_DRAWER_SIMPLE_HEADER) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.navigation_view_text_item,parent,false);
            viewHolder = new NavDrawerContentHolder(view);
        }else if(viewType==RecycleViewHeader.NAV_DRAWER_EMPTY_HEADER){
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.navigation_view_empty_item, parent, false);
            viewHolder = new NavDrawerEmptyHolder(view);
        }else if(viewType==RecycleViewHeader.NAV_DRAWER_SWITCH_HEADER){
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.navigation_view_switch_item, parent, false);
            viewHolder = new NavDrawerSwitchHolder(view);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        DrawerItem drawerItem = drawerItems.get(position);
        final ListItemClickEvent event = new ListItemClickEvent();
        if(holder instanceof NavDrawerContentHolder){
            ((NavDrawerContentHolder)holder).navItemTitleTextView.setText(drawerItem.getTitle());
            ((NavDrawerContentHolder)holder).navItemLogoImageView.setImageResource(drawerItem.getImageId());
            holder.itemView.setOnClickListener(new NavTextClickListener(position));
        }else if(holder instanceof NavDrawerSwitchHolder){
            ((NavDrawerSwitchHolder)holder).navItemTitleTextView.setText(drawerItem.getTitle());
            ((NavDrawerSwitchHolder)holder).navItemLogoImageView.setImageResource(drawerItem.getImageId());
            ((NavDrawerSwitchHolder)holder).navItemSwitch.setChecked(drawerItem.isCheckSwitch());
            holder.itemView.setOnClickListener(new NavTextClickListener(position));
            ((NavDrawerSwitchHolder)holder).navItemSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    event.setId(ListItemClickEvent.SHOW_DRIVERS_ON_MAP_SETTINGS_NAV);
                    BusProvider.send(event);
                }
            });
        }else if(holder instanceof NavDrawerEmptyHolder){

        }
    }

    @Override
    public int getItemCount() {
        return drawerItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(drawerItems.get(position).getRecycleViewHeader()!=null){
            return drawerItems.get(position).getRecycleViewHeader().getId();
        }else {
            return RecycleViewHeader.NAV_DRAWER_SIMPLE_HEADER;
        }
    }



    static class NavDrawerContentHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.nav_item_title_text_view)
        TextView navItemTitleTextView;
        @Bind(R.id.nav_item_logo_image_view)
        ImageView navItemLogoImageView;
        public NavDrawerContentHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    static class NavDrawerSwitchHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.nav_item_title_text_view)
        TextView navItemTitleTextView;
        @Bind(R.id.nav_item_logo_image_view)
        ImageView navItemLogoImageView;
        @Bind(R.id.nav_item_switch_button)
        SwitchCompat navItemSwitch;

        public NavDrawerSwitchHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

        }
    }

    private  static class NavDrawerEmptyHolder extends RecyclerView.ViewHolder{
        public NavDrawerEmptyHolder(View itemView) {
            super(itemView);
        }
    }

    private static class NavTextClickListener implements View.OnClickListener{
        private final int position;

        private NavTextClickListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            ListItemClickEvent event = new ListItemClickEvent();
            switch (position){
                case 0:
                    event.setId(ListItemClickEvent.PROMO_NAV_CLICK);
                    break;
                case 2:
                    event.setId(ListItemClickEvent.CREDIT_CARD_NAV_CLICK);
                    break;
                case 3:
                    event.setId(ListItemClickEvent.CALL_FRIEND_NAV_CLICK);
                    break;
                case 5:
                    event.setId(ListItemClickEvent.CARS_NAV_CLICK);
                    break;
                case 7:
                    event.setId(ListItemClickEvent.MAKE_CALL_NAV_CLICK);
                    break;
                case 9:
                    event.setId(ListItemClickEvent.LOGOUT_NAV_CLICK);
                    break;
            }
            BusProvider.send(event);

        }
    }
}
