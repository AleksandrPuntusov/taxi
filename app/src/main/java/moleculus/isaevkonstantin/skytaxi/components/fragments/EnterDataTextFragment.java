package moleculus.isaevkonstantin.skytaxi.components.fragments;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Selection;
import android.text.Spannable;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dd.morphingbutton.impl.IndeterminateProgressButton;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.presenters.fragments.main.EnterDataPresenter;
import moleculus.isaevkonstantin.skytaxi.components.constants.ArgumentTitles;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpActionView;
import moleculus.isaevkonstantin.skytaxi.components.interfaces.MvpView;
import moleculus.isaevkonstantin.skytaxi.components.model.ActivityViewBuilder;
import moleculus.isaevkonstantin.skytaxi.components.utils.MorphButtonGenerator;
import moleculus.isaevkonstantin.skytaxi.components.utils.UrlSpanNoUnderline;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EnterDataTextFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EnterDataTextFragment extends Fragment implements MvpView {
    @Bind(R.id.enter_data_edit_text)
    EditText dataEditText;
    @Bind(R.id.confirm_data_morph_button)
    IndeterminateProgressButton morphingButton;
    @Bind(R.id.send_data_progress_bar)
    ProgressBar sendCodeProgressBar;
    @Bind(R.id.agree_with_contract_text_view)
    TextView agreeTextView;


    private EnterDataPresenter presenter;
    private MorphButtonGenerator morphButtonGenerator;

    private int fragmentDataId;

    private String phoneNumber;
    private String smsCode;
    private String buttonTitle;
    boolean isLoaded;

    public static final int ENTER_PHONE_FRAGMENT = 1;
    public static final int ENTER_SMS_CODE_FRAGMENT = 2;
    public static final int ENTER_ENTRANCE_FROM_FRAGMENT = 3;
    public static final int ENTER_ENTRANCE_TO_FRAGMENT = 4;
    public static final int ENTER_PROMO_FRAGMENT = 5;

    public EnterDataTextFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment EnterDataTextFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EnterDataTextFragment newInstance() {
        return new EnterDataTextFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            fragmentDataId = getArguments().getInt(ArgumentTitles.ENTER_DATA_ID, -1);
            if (fragmentDataId == ENTER_SMS_CODE_FRAGMENT) {
                phoneNumber = getArguments().getString(ArgumentTitles.PHONE);
            }
        }
        isLoaded = true;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_enter_data_text, container, false);
        ButterKnife.bind(this, view);
        morphButtonGenerator = new MorphButtonGenerator();
//        morphingButton.setEnabled(false);
        sendCodeProgressBar.setIndeterminate(true);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextWatcher textWatcher = null;
        String[] buttonTitleArr = getResources().getStringArray(R.array.confirm_data_button_titles);
        String[] dataHintArr = getResources().getStringArray(R.array.enter_data_edit_text_hint);
        String hint;
        switch (fragmentDataId) {
            case ENTER_PHONE_FRAGMENT:
                dataEditText.setText("+7");
                Selection.setSelection(dataEditText.getText(), dataEditText.getText().length());
                textWatcher = new PhoneTextWatcher();
                buttonTitle = buttonTitleArr[0];
                morphButtonGenerator.morphButton(MorphButtonGenerator.MORPH_DISABLE, buttonTitle, morphingButton);
//                dataEditText.requestFocus();
//                openKeyBoard(true);

                break;
            case ENTER_SMS_CODE_FRAGMENT:
                textWatcher = new SimpleLenghtTextWatcher(4);
                buttonTitle = buttonTitleArr[1];
                dataEditText.requestFocus();
                agreeTextView.setVisibility(View.VISIBLE);
                stripUnderlines(agreeTextView);
                hint = dataHintArr[0];
//                dataEditText.setHint(hint);
//                openKeyBoard(true);
                InputFilter filter = new InputFilter() {
                    public CharSequence filter(CharSequence source, int start, int end,
                                               Spanned dest, int dstart, int dend) {
                        for (int i = start; i < end; i++) {
                            if (!Character.isDigit(source.charAt(i))) {
                                return "";
                            }
                        }
                        return null;
                    }
                };
                dataEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(4), filter});
                morphButtonGenerator.morphButton(MorphButtonGenerator.MORPH_DISABLE, buttonTitle, morphingButton);

                break;
            case ENTER_ENTRANCE_FROM_FRAGMENT:
//                textWatcher = new EntranceLenghtTextWatcher(3);
                buttonTitle = buttonTitleArr[1];
                if (isLoaded) {
                    openKeyBoard(true);
                    dataEditText.requestFocus();
                } else {
                    openKeyBoard(false);
                }
                if (!morphingButton.isEnabled()) {
                    morphingButton.setEnabled(true);
                }
                morphButtonGenerator.morphButton(MorphButtonGenerator.MORPH_ENABLE, buttonTitle, morphingButton);

                dataEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(3)});
                break;
            case ENTER_ENTRANCE_TO_FRAGMENT:
//                textWatcher = new EntranceLenghtTextWatcher(3);
                buttonTitle = buttonTitleArr[1];
                openKeyBoard(true);
                dataEditText.requestFocus();
                dataEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(3)});
                if (!morphingButton.isEnabled()) {
                    morphingButton.setEnabled(true);
                }
                morphButtonGenerator.morphButton(MorphButtonGenerator.MORPH_ENABLE, buttonTitle, morphingButton);

                break;
            case ENTER_PROMO_FRAGMENT:
                buttonTitle = buttonTitleArr[1];
                hint = dataHintArr[1];
                dataEditText.setHint(hint);
                dataEditText.requestFocus();
                dataEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
                openKeyBoard(true);
                textWatcher = new SimpleLenghtTextWatcher(2);
                morphButtonGenerator.morphButton(MorphButtonGenerator.MORPH_DISABLE, buttonTitle, morphingButton);

                break;
        }
        if (textWatcher != null) {
            dataEditText.addTextChangedListener(textWatcher);
        }
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter = new EnterDataPresenter();
        presenter.setMvpView(this);
        presenter.init();

    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.registerSubscriber();
        updateActionBar(fragmentDataId);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.unRegisterSubscriber();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.destroy();
        presenter = null;

    }

    @Override
    public void onDestroyView() {
//        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        int count = getActivity().getSupportFragmentManager().getBackStackEntryCount();
        if (count == 1) {
            openKeyBoard(false);
        }
        super.onDestroyView();
        isLoaded = false;

    }

    @OnClick(R.id.confirm_data_morph_button)
    void sendData() {
        String data = dataEditText.getText().toString();
        if (fragmentDataId == ENTER_PHONE_FRAGMENT) {
            data = data.replace("(", "").replace(")", "").replace("-", "").replace(" ", "").replace("+", "").trim();
        }
        if (presenter.validateText(data, fragmentDataId)) {
//            openKeyBoard(false);
            if ((fragmentDataId == ENTER_PHONE_FRAGMENT)
                    || (fragmentDataId == ENTER_SMS_CODE_FRAGMENT)) {
                sendCodeProgressBar.setVisibility(View.VISIBLE);
                morphingButton.setText("");
            }
            if (fragmentDataId == ENTER_SMS_CODE_FRAGMENT) {
                presenter.sendData(data, phoneNumber, fragmentDataId);
            } else {
                presenter.sendData(data, null, fragmentDataId);

            }
        }
    }

    private void openKeyBoard(boolean open) {
        InputMethodManager imm = (InputMethodManager)
                getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (open) {
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.SHOW_FORCED);
        } else {
            if (imm.isActive()) {
                if (dataEditText != null) {
                    dataEditText.clearFocus();
                }
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            }
        }

    }

    /**
     * Method updates ProgressBarButton.
     *
     * @param success if true progressBarButton is true
     * @param message
     */
    public void sendDataResponse(boolean success, String message) {
        if (success) {
//            sendCodeProgressBar.setVisibility(View.INVISIBLE);
//            morphButtonGenerator.morphButton(MorphButtonGenerator.MORPH_SUCCESS,buttonTitle,morphingButton);
            morphingButton.unblockTouch();
        } else {
//            morphButtonGenerator.morphButton(MorphButtonGenerator.MORPH_FAILED,"X",morphingButton);
            sendCodeProgressBar.setVisibility(View.GONE);
            final Runnable runnable = new Runnable() {
                public void run() {
                    morphButtonGenerator.morphButton(MorphButtonGenerator.MORPH_ENABLE, buttonTitle, morphingButton);
                    morphingButton.unblockTouch();
                }
            };
            Handler handler = new Handler();
            handler.postDelayed(runnable, 500);

        }
    }

    private void updateActionBar(int fragmentId) {
        String actionBarTitle = "";
        switch (fragmentId) {
            case ENTER_PHONE_FRAGMENT:
                actionBarTitle = getContext().getResources().getString(R.string.enter_phone_action_bar_title);
                break;
            case ENTER_SMS_CODE_FRAGMENT:
                actionBarTitle = getContext().getResources().getString(R.string.enter_code_action_bar_title);
                break;
            case ENTER_ENTRANCE_FROM_FRAGMENT:
                actionBarTitle = getContext().getResources().getString(R.string.enter_flat_action_bar_title);
                break;
            case ENTER_ENTRANCE_TO_FRAGMENT:
                actionBarTitle = getContext().getResources().getString(R.string.enter_flat_action_bar_title);
                break;
            case ENTER_PROMO_FRAGMENT:
                actionBarTitle = getContext().getResources().getString(R.string.enter_promo_action_bar_title);
                break;
        }
        ActivityViewBuilder activityViewBuilder = ActivityViewBuilder.createNavigation(actionBarTitle, ActivityViewBuilder.BACK_NAVIGATION);
        activityViewBuilder.setColor(R.color.white);
        MvpActionView mvpActionView = (MvpActionView) getActivity();
        mvpActionView.updateUi(activityViewBuilder);
    }

    /**
     * Method removes underline below text view with url.
     *
     * @param textView
     */
    private void stripUnderlines(TextView textView) {
        Spannable s = (Spannable) textView.getText();
        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
        for (URLSpan span : spans) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            span = new UrlSpanNoUnderline(span.getURL());
            s.setSpan(span, start, end, 0);
        }
        textView.setText(s);
    }

    private class PhoneTextWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String data = s.toString();
            if (!data.isEmpty() && before == 0) {
                if (data.startsWith("+7") && data.length() == 3) {
                    dataEditText.getText().insert(2, " (");
                }
                if (data.length() == 7) {
                    dataEditText.getText().insert(7, ") ");
                }
                if (data.length() == 12 || data.length() == 15) {
                    dataEditText.append("-");
                }
            }
            if (data.length() == 18) {
                if (!morphingButton.isEnabled()) {
                    morphingButton.setEnabled(true);
                    morphButtonGenerator.morphButton(MorphButtonGenerator.MORPH_ENABLE, buttonTitle, morphingButton);
                }
            } else {
                if (morphingButton.isEnabled()) {
                    morphingButton.setEnabled(false);
                    morphButtonGenerator.morphButton(MorphButtonGenerator.MORPH_DISABLE, buttonTitle, morphingButton);

                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (!s.toString().startsWith("+7")) {
                dataEditText.setText("+7");
                Selection.setSelection(dataEditText.getText(), dataEditText.getText().length());

            }
        }
    }

    private class SimpleLenghtTextWatcher implements TextWatcher {
        private final int lenght;

        private SimpleLenghtTextWatcher(int lenght) {
            this.lenght = lenght;
        }


        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() == lenght) {
                if (!morphingButton.isEnabled()) {
                    morphingButton.setEnabled(true);
                    morphButtonGenerator.morphButton(MorphButtonGenerator.MORPH_ENABLE, buttonTitle, morphingButton);
                }
            } else {
                if (morphingButton.isEnabled()) {
                    morphingButton.setEnabled(false);
                    morphButtonGenerator.morphButton(MorphButtonGenerator.MORPH_DISABLE, buttonTitle, morphingButton);

                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    private class EntranceLenghtTextWatcher implements TextWatcher {
        private final int lenght;

        private EntranceLenghtTextWatcher(int lenght) {
            this.lenght = lenght;
        }


        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() > 0 && s.length() <= lenght) {
                if (!morphingButton.isEnabled()) {
                    morphingButton.setEnabled(true);
                    morphButtonGenerator.morphButton(MorphButtonGenerator.MORPH_ENABLE, buttonTitle, morphingButton);
                }
            } else {
                if (morphingButton.isEnabled()) {
                    morphingButton.setEnabled(false);
                    morphButtonGenerator.morphButton(MorphButtonGenerator.MORPH_DISABLE, buttonTitle, morphingButton);

                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

}

