package moleculus.isaevkonstantin.skytaxi.components.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.components.fragments.profile.ProfileFragment;

/**
 * Created by Isaev Konstantin on 02.06.16.
 */
public class CancelOrderDialog extends DialogFragment {



    public static final String LOAD_IMAGE_SOURCE_TITLE = "LoadImageSource";
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View alertDialogView = inflater.inflate(R.layout.dialog_cancel_order, null);
        ButterKnife.bind(this, alertDialogView);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(alertDialogView);
        AlertDialog alertDialog = builder.create();
        return alertDialog;
    }

    @OnClick(R.id.cancel_order_dialog_ok_button)
    public void okClick(){
        Intent intent = new Intent();
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
        dismiss();
    }
    @OnClick(R.id.cancel_order_dialog_cancel_button)
    public void cancelClick(){
        dismiss();
    }

}
