package moleculus.isaevkonstantin.skytaxi.components.service;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;

import moleculus.isaevkonstantin.skytaxi.eventbus.BusProvider;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.constants.ApiConstants;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.network.NetworkFailEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.network.NetworkResponseEvent;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.UpdateUiEvent;
import moleculus.isaevkonstantin.skytaxi.interactors.LocationInteractor;
import moleculus.isaevkonstantin.skytaxi.interactors.OrderInteractor;
import moleculus.isaevkonstantin.skytaxi.interactors.UserInteractor;
import moleculus.isaevkonstantin.skytaxi.logger.Logger;
import moleculus.isaevkonstantin.skytaxi.models.Order;
import moleculus.isaevkonstantin.skytaxi.models.Trip;
import moleculus.isaevkonstantin.skytaxi.models.User;
import moleculus.isaevkonstantin.skytaxi.components.constants.ArgumentTitles;
import moleculus.isaevkonstantin.skytaxi.components.fragments.profile.ProfileFragment;
import rx.Subscriber;

public class SkyTaxiService extends Service {
    public static final String SERVICE_JOB_ID_TITLE = "serviceJobId";
    private Subscriber subscriber;
    private LocationInteractor locationInteractor;
    private UserInteractor userInteractor;
    private OrderInteractor orderInteractor;
    private static final String LOGGER_TAG = "SkyTaxiService";
    private int startId;
    public SkyTaxiService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Logger.debug(LOGGER_TAG,"OnCreate");
        subscriber = new Subscriber() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Object o) {
                if (o instanceof NetworkResponseEvent) {
                    NetworkResponseEvent event = (NetworkResponseEvent) o;
                    if(!event.isSucess()) {
                        requestFailed(event);
                    }
                    requestCallBack(event);
                }
            }
        };
        BusProvider.observe().subscribe(subscriber);
        Log.d(LOGGER_TAG,"subscribe");
        userInteractor = new UserInteractor(this);
        locationInteractor = new LocationInteractor(this);
        orderInteractor = new OrderInteractor(this);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Logger.debug(LOGGER_TAG,"onDestroy");
        subscriber.unsubscribe();
        subscriber = null;
        Logger.debug(LOGGER_TAG,"unsubscribe");
        userInteractor = null;
        locationInteractor = null;
        orderInteractor = null;

    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        this.startId = startId;
        int jobId = intent.getIntExtra(SERVICE_JOB_ID_TITLE, -1);

        String phoneNumber;
        String token;
        User user;
        Order order;
        String accessToken;
        switch (jobId){
            case ApiConstants.SEND_PHONE:
                phoneNumber = intent.getStringExtra(ArgumentTitles.ENTER_DATA_TEXT);
                phoneNumber = "380957793596";
                // TODO: 27.07.2016 нужно удалить при релизе и обновлении
                userInteractor.sendPhone(phoneNumber);
                break;
            case ApiConstants.CONFIRM_SMS_CODE:
                phoneNumber = intent.getStringExtra(ArgumentTitles.PHONE);
                String code  = intent.getStringExtra(ArgumentTitles.SMS_CODE);
//                code = "6969";
                userInteractor.sendSmsCode(phoneNumber,code);
                break;
            case ApiConstants.GET_USER:
                token = intent.getStringExtra(ArgumentTitles.AUTH_TOKEN);
                userInteractor.getUserRest(token);
                break;
            case ApiConstants.UPDATE_PROFILE_PHOTO:
                user = intent.getParcelableExtra(ProfileFragment.USER_TITLE_PARAM);
                Uri uri  = Uri.parse(user.getPathToPhoto());
                userInteractor.updateProfilePhoto(user.getAuthToken(),uri);
                break;
            case ApiConstants.DELETE_PROFILE_PHOTO:
                break;
            case ApiConstants.UPDATE_USER:
                user = intent.getParcelableExtra(ProfileFragment.USER_TITLE_PARAM);
                userInteractor.updateUserRest(user);
                if(user.getPathToPhoto()!=null){
                    userInteractor.updateProfilePhoto(user.getAuthToken(),Uri.parse(user.getPathToPhoto()));
                }
                break;
            case ApiConstants.SEARCH_STREET:
                String locationTitle = intent.getStringExtra(ArgumentTitles.LOCATION_TITLE_PARAM);
                locationInteractor.searchAddress(locationTitle);
                break;
            case ApiConstants.LOGOUT:
                accessToken = intent.getStringExtra(ArgumentTitles.AUTH_TOKEN);
                userInteractor.logoutUserRest(accessToken);
                break;
            case ApiConstants.GET_AIR_PORTS:
                user = userInteractor.getUserFromDb(0);
                locationInteractor.getStations(user.getAuthToken());
                break;
            case ApiConstants.GET_ORDER_COST:
                order = orderInteractor.getCurrentOrderFromDb();
                user = userInteractor.getUserFromDb(0);
                orderInteractor.getOrderCost(order,user.getAuthToken());
                break;
            case ApiConstants.SEARCH_HOUSE_BY_STREET_ID:
                String streetId = intent.getStringExtra(ArgumentTitles.STREET_ID);
                locationInteractor.searchHouseByStreetId(streetId);
                break;
            case ApiConstants.CREATE_ORDER:
                order = orderInteractor.getCurrentOrderFromDb();
                user = userInteractor.getUserFromDb(0);
                if(order!=null){
                    Trip chekedTrip = orderInteractor.getCheckedTrip(order.getCheckedTripId(),order.getId());
                    if(chekedTrip!=null){
                        orderInteractor.createOrder(Integer.parseInt(chekedTrip.getServerId()),order.getPaymentId(),user.getAuthToken());
                    }
                }
                break;
            case ApiConstants.CANCEL_ORDER:
                order = orderInteractor.getCurrentOrderFromDb();
                user = userInteractor.getUserFromDb(0);
                if(order!=null){
                    orderInteractor.cancelOrder(user.getAuthToken(),order.getServerId());
                }
                break;

        }
        return START_NOT_STICKY;
    }

    private void requestCallBack(NetworkResponseEvent event){
        UpdateUiEvent updateUiEvent = new UpdateUiEvent();
        updateUiEvent.setSuccess(event.isSucess());
        switch (event.getId()) {
            case ApiConstants.SEND_PHONE:
                updateUiEvent.setId(UpdateUiEvent.ENTER_PHONE_PRESENTER_EVENT);
                updateUiEvent.setData(event.getData());
                break;
            case ApiConstants.CONFIRM_SMS_CODE:
                updateUiEvent.setId(UpdateUiEvent.ENTER_SMS_CODE_PRESENTER_EVENT);
                break;
            case ApiConstants.GET_USER:
                updateUiEvent.setId(UpdateUiEvent.SYNC_PROFILE_PRESENTER_EVENT);
                updateUiEvent.setData(event.getData());
                break;
            case ApiConstants.UPDATE_USER:
                updateUiEvent.setId(UpdateUiEvent.UPDATE_PROFILE_EVENT);
                break;
            case ApiConstants.LOGOUT:
                updateUiEvent.setId(UpdateUiEvent.LOGOUT_EVENT);
                break;
            case ApiConstants.GET_AIR_PORTS:
                updateUiEvent.setId(UpdateUiEvent.GET_AIR_PORTS_EVENT);
                break;
            case ApiConstants.GET_ORDER_COST:
                updateUiEvent.setId(UpdateUiEvent.GET_ORDER_COST_EVENT);
                break;
            case ApiConstants.CREATE_ORDER:
                updateUiEvent.setId(UpdateUiEvent.CREATE_ORDER);
                break;
            case ApiConstants.SEARCH_STREET:
                updateUiEvent.setId(UpdateUiEvent.SEARCH_ADDRESS_BY_TITLE);
                updateUiEvent.setData(event.getData());
                break;
            case ApiConstants.CANCEL_ORDER:
                updateUiEvent.setId(UpdateUiEvent.CANCEL_ORDER);
                break;

        }
        if (updateUiEvent != null) {
            BusProvider.send(updateUiEvent);
        }
        stopSelf(startId);
    }
    private void requestFailed(NetworkResponseEvent event){
        NetworkFailEvent networkFailEvent = new NetworkFailEvent();
        BusProvider.send(networkFailEvent);
        stopSelf(startId);
    }

}


