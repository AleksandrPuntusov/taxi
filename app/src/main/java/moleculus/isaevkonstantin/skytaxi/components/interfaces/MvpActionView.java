package moleculus.isaevkonstantin.skytaxi.components.interfaces;


import moleculus.isaevkonstantin.skytaxi.components.model.ActivityViewBuilder;

/**
 * Created by Isaev Konstantin on 02.04.16.
 */
public interface MvpActionView extends MvpView{

    void updateUi(ActivityViewBuilder activityViewBuilder);

    void makeToast(String text);
}
