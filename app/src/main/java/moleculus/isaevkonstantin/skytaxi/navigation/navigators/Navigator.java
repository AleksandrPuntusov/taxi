package moleculus.isaevkonstantin.skytaxi.navigation.navigators;

import android.content.Intent;

import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.FragmentEvent;

/**
 * Created by Isaev Konstantin on 02.05.16.
 */
public interface Navigator {

    void navigateToFragment(FragmentEvent event);
    void navigateToActivity(Intent intent);
}
