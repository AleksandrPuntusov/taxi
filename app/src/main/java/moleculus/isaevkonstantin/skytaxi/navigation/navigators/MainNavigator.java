package moleculus.isaevkonstantin.skytaxi.navigation.navigators;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.FragmentEvent;
import moleculus.isaevkonstantin.skytaxi.navigation.FragmentDisplayer;

/**
 * Created by Isaev Konstantin on 10.04.16.
 */
public class MainNavigator implements Navigator {
    private FragmentDisplayer fragmentDisplayer;
    private AppCompatActivity activity;

    public AppCompatActivity getActivity() {
        return activity;
    }

    public void setActivity(AppCompatActivity activity) {
        this.activity = activity;
    }

    public void init() {
        fragmentDisplayer = new FragmentDisplayer();
        fragmentDisplayer.setFragmentManager(activity.getSupportFragmentManager());
    }

    public void destroy() {
        activity = null;
        fragmentDisplayer = null;
    }

    @Override
    public void navigateToFragment(FragmentEvent event) {
        if (event.isDialog()) {
            fragmentDisplayer.displayDialog(event.getId(), event.getOwner(), event.getBundle());
        } else {
            fragmentDisplayer.displayFragment(event.getId(), event.isBackStack(), event.getBundle());
        }
    }

    public void clearAddToBackStack() {
        fragmentDisplayer.clearAddToBackStack();
    }


    @Override
    public void navigateToActivity(Intent intent) {
        activity.startActivity(intent);
    }


}
