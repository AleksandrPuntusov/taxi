package moleculus.isaevkonstantin.skytaxi.navigation;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import moleculus.isaevkonstantin.skytaxi.R;
import moleculus.isaevkonstantin.skytaxi.components.dialogs.CancelOrderDialog;
import moleculus.isaevkonstantin.skytaxi.components.fragments.trip.TripSuccessFragment;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.ui.FragmentEvent;
import moleculus.isaevkonstantin.skytaxi.components.dialogs.LoadImageDialog;
import moleculus.isaevkonstantin.skytaxi.components.dialogs.PromoSuccessDialog;
import moleculus.isaevkonstantin.skytaxi.components.fragments.main.AirPortsViewPagerFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.main.FavoriteAddressFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.main.MapFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.main.SearchAddressFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.profile.CallFriendFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.profile.ProfileFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.EnterDataTextFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.profile.PromoSuccessFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.profile.SetCardFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.splash.SplashFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.splash.WalkThroughtFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.trip.BlueListTimePaymentFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.trip.TripCommentFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.trip.TripCostFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.trip.TripDetailsFragment;
import moleculus.isaevkonstantin.skytaxi.components.fragments.trip.TripSettingsFragment;

/**
 * Created by Isaev Konstantin on 13.04.16.
 */
public class FragmentDisplayer {
    private  FragmentManager fragmentManager;


    public static FragmentDisplayer newInstance(){
        return new FragmentDisplayer();
    }

    public FragmentManager getFragmentManager() {
        return fragmentManager;
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    public void displayDialog(int fragmentId, Fragment fragment,Bundle bundle){
        DialogFragment dialogFragment = null;
        switch (fragmentId){
            case FragmentEvent.LOAD_PHOTO_DIALOG:
                dialogFragment = new LoadImageDialog();
                dialogFragment.setTargetFragment(fragment, ProfileFragment.LOAD_IMAGE);
                break;
            case FragmentEvent.CANCEL_ORDER_DIALOG:
                dialogFragment = new CancelOrderDialog();
                dialogFragment.setTargetFragment(fragment, MapFragment.CANCEL_ORDER_DIALOG);
                break;
            case FragmentEvent.PROMO_SUCCESS_DIALOG:
                dialogFragment = new PromoSuccessDialog();
//                dialogFragment.setTargetFragment(fragment, ProfileFragment.LOAD_IMAGE);
                break;
        }
        if (dialogFragment != null) {
            if(bundle!=null){
                dialogFragment.setArguments(bundle);
            }
            dialogFragment.show(getFragmentManager(), null);
        }

    }

    public void clearAddToBackStack() {
        for(int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
            fragmentManager.popBackStack();
        }
    }

    public Fragment displayFragment(int fragmentId, boolean backStack,Bundle bundle){
        Fragment fragment = null;
        switch (fragmentId){
            case FragmentEvent.SPLASH_FRAGMENT:
                fragment = SplashFragment.newInstance();
                break;
            case FragmentEvent.WALKTHROUGHT_FRAGMENT:
                fragment = WalkThroughtFragment.newInstance();
                break;
            case FragmentEvent.CALL_FRIEND_FRAGMENT:
                fragment = CallFriendFragment.newInstance();
                break;
            case FragmentEvent.PROFILE_FRAGMENT:
                fragment = ProfileFragment.newInstance();
                break;
            case FragmentEvent.MAP_FRAGMENT:
                fragment = MapFragment.newInstance();
                break;
            case FragmentEvent.ENTER_DATA_TEXT_FRAGMENT:
                fragment = EnterDataTextFragment.newInstance();
                break;
            case FragmentEvent.SEARCH_ADDRESS_FRAGMENT:
                fragment = SearchAddressFragment.newInstance();
                break;
            case FragmentEvent.SEARCH_IN_FAVORITES_FRAGMENT:
                fragment = FavoriteAddressFragment.newInstance();
                break;
            case FragmentEvent.SEARCH_IN_AIRPORTS_FRAGMENT:
                fragment = AirPortsViewPagerFragment.newInstance();
                break;
            case FragmentEvent.TIME_PAYMENT_LIST_FRAGMENT:
                fragment = BlueListTimePaymentFragment.newInstance();
                break;
            case FragmentEvent.TRIP_DETAIL_FRAGMENT:
                fragment = TripDetailsFragment.newInstance();
                break;
            case FragmentEvent.TRIP_COST_FRAGMENT:
                fragment = TripCostFragment.newInstance();
                break;
            case FragmentEvent.TRIP_SETTINGS_FRAGMENT:
                fragment = TripSettingsFragment.newInstance();
                break;
            case FragmentEvent.TRIP_COMMENT_FRAGMENT:
                fragment = TripCommentFragment.newInstance();
                break;
            case FragmentEvent.PROMO_FRAGMENT:
                fragment = PromoSuccessFragment.newInstance();
                break;
            case FragmentEvent.SET_CARD_FRAGMENT:
                fragment = SetCardFragment.newInstance();
                break;
            case FragmentEvent.TRIP_SUCCESS:
                fragment = TripSuccessFragment.newInstance();
                break;
        }
        if (fragment != null) {
            if(bundle!=null){
                fragment.setArguments(bundle);
            }
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.place_for_fragment,fragment);
            if (backStack){
                transaction.addToBackStack(null);
            }
            transaction.commit();
        }
        return null;
    }
}
