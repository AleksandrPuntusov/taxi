package moleculus.isaevkonstantin.skytaxi.interactors;

import android.content.Context;

import moleculus.isaevkonstantin.skytaxi.database.dao.DriverDao;
import moleculus.isaevkonstantin.skytaxi.database.dao.LocationDao;
import moleculus.isaevkonstantin.skytaxi.database.dao.OrderDao;
import moleculus.isaevkonstantin.skytaxi.database.dao.OrderOptionsDao;
import moleculus.isaevkonstantin.skytaxi.database.dao.PromoCodeDao;
import moleculus.isaevkonstantin.skytaxi.database.dao.TripDao;
import moleculus.isaevkonstantin.skytaxi.database.dao.UserDao;
import moleculus.isaevkonstantin.skytaxi.database.dao.UserStatisticsDao;
import moleculus.isaevkonstantin.skytaxi.database.interfaces.Crud;
import moleculus.isaevkonstantin.skytaxi.rest.RestAdapter;

/**
 * Created by Isaev Konstantin on 22.05.16.
 */
public class BaseInteractor {

    public static final int USER_DAO = 1;
    public static final int ORDER_DAO = 2;
    public static final int PROMOCODE_DAO = 3;
    public static final int USER_STATISTICS_DAO = 4;
    public static final int LOCATION_DAO = 5;
    public static final int ORDER_SETTINGS_DAO = 6;
    public static final int TRIP_DAO = 7;
    public static final int DRIVER_DAO = 8;

    private Context context;
    protected RestAdapter restAdapter;
    public BaseInteractor(Context context) {
        this.context = context;
        restAdapter = new RestAdapter();
    }

    protected Crud initDao(int daoId){
        Crud crud = null;
        switch (daoId){
            case USER_DAO:
                crud = new UserDao(context);
                break;
            case ORDER_DAO:
                crud = new OrderDao(context);
                break;
            case PROMOCODE_DAO:
                crud = new PromoCodeDao(context);
                break;
            case USER_STATISTICS_DAO:
                crud = new UserStatisticsDao(context);
                break;
            case LOCATION_DAO:
                crud = new LocationDao(context);
                break;
            case ORDER_SETTINGS_DAO:
                crud = new OrderOptionsDao(context);
                break;
            case TRIP_DAO:
                crud = new TripDao(context);
                break;
            case DRIVER_DAO:
                crud = new DriverDao(context);
                break;
        }
        return crud;
    }

    protected void destroy(){
        context = null;
        restAdapter = null;
    }

}
