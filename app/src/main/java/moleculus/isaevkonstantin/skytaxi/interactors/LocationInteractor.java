package moleculus.isaevkonstantin.skytaxi.interactors;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import moleculus.isaevkonstantin.skytaxi.database.dao.LocationDao;
import moleculus.isaevkonstantin.skytaxi.eventbus.BusProvider;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.constants.ApiConstants;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.network.NetworkResponseEvent;
import moleculus.isaevkonstantin.skytaxi.models.Location;
import moleculus.isaevkonstantin.skytaxi.models.SearchedLocation;
import moleculus.isaevkonstantin.skytaxi.rest.interfaces.LocationRestService;
import moleculus.isaevkonstantin.skytaxi.rest.models.ResponseResult;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

/**
 * Created by Isaev Konstantin on 09.05.16.
 */
public class LocationInteractor extends BaseInteractor{

    public LocationInteractor(Context context) {
        super(context);
    }

    public void searchAddress(String title){
        restAdapter.init(null,false);
        LocationRestService locationRestService = restAdapter.getRetrofit().create(LocationRestService.class);
        Observable<Response<JsonObject>> locationObservable = locationRestService.
                searchStreet(title,38,55,15);
        locationObservable.subscribeOn(Schedulers.newThread()).subscribe(new Subscriber<Response<JsonObject>>() {
            private NetworkResponseEvent<List<Location>> event;
            private List<Location> locations;
            @Override
            public void onCompleted() {
                if(event!=null){
                    event.setSucess(true);
                }else {
                    event = new NetworkResponseEvent<>();
                    event.setId(ApiConstants.SEARCH_STREET);
                    event.setSucess(false);
                }
                event.setData(locations);
                BusProvider.send(event);
            }

            @Override
            public void onError(Throwable e) {
                event = new NetworkResponseEvent<>();
                event.setId(ApiConstants.SEARCH_STREET);
                event.setSucess(false);
                BusProvider.send(event);
            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if(response.isSuccessful()){
                    event = new NetworkResponseEvent<List<Location>>();
                    event.setId(ApiConstants.SEARCH_STREET);
                    JsonObject jsonObject = response.body().getAsJsonObject();
                    JsonArray locationJson = response.body().getAsJsonArray("list");
                    locations = new ArrayList<Location>();
                    for (int i = 0; i < locationJson.size(); i++) {
                        JsonArray searchLocationJson = locationJson.get(i).getAsJsonArray();
                        SearchedLocation searchedLocation = new SearchedLocation();
                        searchedLocation.setId(searchLocationJson.get(0).getAsString());
                        searchedLocation.setType(searchLocationJson.get(1).getAsString());
                        searchedLocation.setAddress(searchLocationJson.get(2).getAsString());
                        searchedLocation.setLongitude(searchLocationJson.get(3).getAsDouble());
                        searchedLocation.setLatitude(searchLocationJson.get(4).getAsDouble());
                        searchedLocation.setLocationType(searchLocationJson.get(5).getAsString());
                        Location location = Location.toLocation(searchedLocation);
                        if(location!=null){
                            locations.add(location);
                        }
                    }
                }
            }
        });
    }

    public void searchHouseByStreetId(String streetId){
        restAdapter.init(null,false);
        LocationRestService locationRestService = restAdapter.getRetrofit().create(LocationRestService.class);
        Observable<Response<JsonObject>> locationObservable = locationRestService.
                searchhousebyStreetId(streetId);
        locationObservable.subscribeOn(Schedulers.newThread()).subscribe(new Subscriber<Response<JsonObject>>() {
            private NetworkResponseEvent<List<Location>> event;
            private List<Location> locations;
            @Override
            public void onCompleted() {
                if(event!=null){
                    event.setSucess(true);
                }else {
                    event = new NetworkResponseEvent<>();
                    event.setId(ApiConstants.SEARCH_STREET);
                    event.setSucess(false);
                }
                event.setData(locations);
                BusProvider.send(event);
            }

            @Override
            public void onError(Throwable e) {
                event = new NetworkResponseEvent<>();
                event.setId(ApiConstants.SEARCH_STREET);
                event.setSucess(false);
                BusProvider.send(event);
            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if(response.isSuccessful()){
                    event = new NetworkResponseEvent<List<Location>>();
                    event.setId(ApiConstants.SEARCH_STREET);
                    JsonObject jsonObject = response.body().getAsJsonObject();
                    JsonArray locationJson = response.body().getAsJsonArray("list");
                    locations = new ArrayList<Location>();
                    for (int i = 0; i < locationJson.size(); i++) {
                        JsonArray searchLocationJson = locationJson.get(i).getAsJsonArray();
                        SearchedLocation searchedLocation = new SearchedLocation();
                        searchedLocation.setId(searchLocationJson.get(0).getAsString());
                        searchedLocation.setType(searchLocationJson.get(1).getAsString());
                        searchedLocation.setAddress(searchLocationJson.get(2).getAsString());
                        searchedLocation.setLongitude(searchLocationJson.get(3).getAsDouble());
                        searchedLocation.setLatitude(searchLocationJson.get(4).getAsDouble());
                        searchedLocation.setLocationType(searchLocationJson.get(5).getAsString());
                        Location location = Location.toLocation(searchedLocation);
                        if(location!=null){
                            locations.add(location);
                        }
                    }
                }
            }
        });
    }

    public void saveLocationDb(Location location){
        LocationDao locationDao = (LocationDao) initDao(LOCATION_DAO);
        String name = location.getName();
        List<Location> locationList = locationDao.getAllByTitle(location.getName());
        if (locationList == null) {
            location.setId(locationDao.add(location));
        }else {
            location.setId(locationList.get(0).getId());
        }
    }

    public List<Location> getAllFromLocationsDb(){
        LocationDao locationDao = (LocationDao) initDao(LOCATION_DAO);
        return locationDao.getAllByPath(Location.FROM);
    }

    public List<Location> getAllToLocationsDb(){
        LocationDao locationDao = (LocationDao) initDao(LOCATION_DAO);
        return locationDao.getAllByPath(Location.TO);

    }

    public List<Location> getAllStationsByTypeDb(int type){
        LocationDao locationDao = (LocationDao) initDao(LOCATION_DAO);
        List<Location> locationList = locationDao.getAllStationsByType(type);
        return locationList;
    }
    public void getStations(String authToken){
        restAdapter.init(authToken,false);
        LocationRestService locationRestService = restAdapter.getRetrofit().create(LocationRestService.class);
        Observable<Response<JsonObject>> getStationsObservable = locationRestService.getAirPorts();
        getStationsObservable.subscribeOn(Schedulers.newThread()).
                subscribe(new Subscriber<Response<JsonObject>>() {
                    private NetworkResponseEvent<List<Location>> event;
                    private List<Location> locations;
                    @Override
                    public void onCompleted() {
                        if(event!=null){
                            event.setSucess(true);
                        }else {
                            event = new NetworkResponseEvent<>();
                            event.setId(ApiConstants.GET_AIR_PORTS);
                            event.setSucess(false);
                        }
                        if(locations!=null){
                            LocationDao locationDao = (LocationDao) initDao(LOCATION_DAO);
                            List<Location> locs = locationDao.getAll();
                            for (int i = 0; i < locations.size(); i++) {
                                Location location = locationDao.getAllStationsByName(locations.get(i).getName());
                                if(location!=null){
                                    locations.get(i).setId(location.getId());
                                    if(locationDao.update(locations.get(i))){
                                        locations.get(i).setId(location.getId());
                                    }
                                }else {
                                    locationDao.add(locations.get(i));
                                }
                            }
                        }
                        BusProvider.send(event);

                    }

                    @Override
                    public void onError(Throwable e) {
                        event = new NetworkResponseEvent<>();
                        event.setId(ApiConstants.GET_AIR_PORTS);
                        event.setSucess(false);
                        BusProvider.send(event);
                    }

                    @Override
                    public void onNext(Response<JsonObject> response) {
                        if(response.isSuccessful()){
                            locations = new ArrayList<>();
                            JsonObject jsonObject = response.body().getAsJsonObject();
                            Gson gson = new Gson();
                            ResponseResult responseResult = gson.fromJson(jsonObject,ResponseResult.class);
                            event = new NetworkResponseEvent();
                            event.setId(ApiConstants.GET_AIR_PORTS);
                            JsonArray locationJson = response.body().getAsJsonArray("airports");
                            Type listType = new TypeToken<List<Location>>(){}.getType();
                            List<Location> airPortsLocations =  gson.fromJson(locationJson, listType);
                            locationJson = response.body().getAsJsonArray("railstations");
                            List<Location> railsStationsLocations =  gson.fromJson(locationJson, listType);
                            if(airPortsLocations!=null){
                                for (Location location:airPortsLocations) {
                                    location.setStation(true);
                                    location.setStationId(Location.AIR_PORTS);
                                }
                                locations.addAll(airPortsLocations);
                            }
                            if(railsStationsLocations!=null){
                                for (Location location:railsStationsLocations) {
                                    location.setStation(true);
                                    location.setStationId(Location.RAILS_STATION);
                                }
                                locations.addAll(railsStationsLocations);
                            }

                        }
                    }
                });

    }



}
