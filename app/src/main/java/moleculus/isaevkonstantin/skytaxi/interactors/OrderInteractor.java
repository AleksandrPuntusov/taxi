package moleculus.isaevkonstantin.skytaxi.interactors;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import moleculus.isaevkonstantin.skytaxi.database.dao.DriverDao;
import moleculus.isaevkonstantin.skytaxi.database.dao.OrderDao;
import moleculus.isaevkonstantin.skytaxi.database.dao.TripDao;
import moleculus.isaevkonstantin.skytaxi.database.interfaces.Crud;
import moleculus.isaevkonstantin.skytaxi.eventbus.BusProvider;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.constants.ApiConstants;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.network.NetworkResponseEvent;
import moleculus.isaevkonstantin.skytaxi.models.Driver;
import moleculus.isaevkonstantin.skytaxi.models.Location;
import moleculus.isaevkonstantin.skytaxi.models.Order;
import moleculus.isaevkonstantin.skytaxi.models.OrderOptions;
import moleculus.isaevkonstantin.skytaxi.models.Trip;
import moleculus.isaevkonstantin.skytaxi.models.TripLocation;
import moleculus.isaevkonstantin.skytaxi.rest.interfaces.OrderRestService;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

/**
 * Created by Isaev Konstantin on 11.05.16.
 */
public class OrderInteractor extends BaseInteractor{

    public OrderInteractor(Context context) {
        super(context);

    }

    public void addOrderToDb(Order order){
        Crud crud  = initDao(ORDER_DAO);
        order.setId(crud.add(order));
        if(order.getOrderOptions()!=null){
            crud = initDao(ORDER_SETTINGS_DAO);
            long orderOptionsFk = crud.add(order.getOrderOptions());
            order.setOrderOptionFk(orderOptionsFk);
        }
    }

    public void updateOrderInDb(Order order){
        Crud crud  = initDao(ORDER_DAO);
        boolean success = crud.update(order);
        if(!success){
            addOrderToDb(order);
        }
    }

    public void updateDriverInDb(Driver driver){
        DriverDao crud  = (DriverDao) initDao(DRIVER_DAO);
        boolean success = crud.updateByServerId(driver);
        if(!success){
            driver.setId(crud.add(driver));
        }else {
            Driver dbDriver = crud.getByServerId(driver.getServerId());
            driver.setId(dbDriver.getId());
        }
    }

    public void updateOrderOptionInDb(OrderOptions orderOptions){
        Crud crud  = initDao(ORDER_SETTINGS_DAO);
        boolean success = crud.update(orderOptions);
        if(!success){
            orderOptions.setId(crud.add(orderOptions));
        }
    }

    public Order getCurrentOrderFromDb(){
        Crud crud  = initDao(ORDER_DAO);
        Order order = ((OrderDao)crud).getCurrentOrder();
        if (order != null) {
            crud  = initDao(ORDER_SETTINGS_DAO);
            OrderOptions orderOptions = (OrderOptions) crud.getById(order.getOrderOptionFk());
            TripDao tripDao  = (TripDao) initDao(TRIP_DAO);
            List<Trip> trips = tripDao.getAllByOrderFk(order.getId());
            order.setTrips(trips);
            crud  = initDao(LOCATION_DAO);
            order.setLocationFrom((Location) crud.getById(order.getLocationFromId()));
            order.setLocationTo((Location) crud.getById(order.getLocationToId()));
            order.setOrderOptions(orderOptions);
            crud = initDao(DRIVER_DAO);
            Driver driver = (Driver)crud.getById(order.getDriverFk());
            order.setDriver(driver);
        }
        return order;
    }

    public Order getOrderByServerIdFromDb(String orderId){
        Crud crud  = initDao(ORDER_DAO);
        Order order = ((OrderDao)crud).getOrderByServerId(orderId);
        if (order != null) {
            crud  = initDao(ORDER_SETTINGS_DAO);
            OrderOptions orderOptions = (OrderOptions) crud.getById(order.getOrderOptionFk());
            TripDao tripDao  = (TripDao) initDao(TRIP_DAO);
            List<Trip> trips = tripDao.getAllByOrderFk(order.getId());
            order.setTrips(trips);
            crud  = initDao(LOCATION_DAO);
            order.setLocationFrom((Location) crud.getById(order.getLocationFromId()));
            order.setLocationTo((Location) crud.getById(order.getLocationToId()));
            order.setOrderOptions(orderOptions);
            crud = initDao(DRIVER_DAO);
            Driver driver = (Driver)crud.getById(order.getDriverFk());
            order.setDriver(driver);
        }
        return order;
    }

    public void updateTrip(Trip trip){
        TripDao triDao  = (TripDao) initDao(TRIP_DAO);
        boolean success = triDao.updateByOrderId(trip);
        if(!success){
            trip.setId(triDao.add(trip));
        }
    }

    public void deleteTrip(Trip trip){
        TripDao triDao  = (TripDao) initDao(TRIP_DAO);
        boolean success = triDao.deleteById(trip.getId());

    }

    public Trip getCheckedTrip(long tripId,long orderId){
        TripDao triDao  = (TripDao) initDao(TRIP_DAO);
        return triDao.getByIdAndOrderId(tripId,orderId);
    }

    public void createOrder(int tripId,int paymentMethod,String authToken){
        restAdapter.init(authToken,false);
        OrderRestService orderRestService = restAdapter.getRetrofit().create(OrderRestService.class);
        Observable<Response<JsonObject>> createOrder = orderRestService.createOrder(tripId,paymentMethod);
        createOrder.subscribeOn(Schedulers.newThread()).
                subscribe(new Subscriber<Response<JsonObject>>() {
                    private NetworkResponseEvent<List<Location>> event;
                    private String  orderId;
                    @Override
                    public void onCompleted() {
                        if(event!=null){
                            event.setSucess(true);
                        }else {
                            event = new NetworkResponseEvent<>();
                            event.setId(ApiConstants.CREATE_ORDER);
                            event.setSucess(false);
                        }
                        if(orderId!=null&&!orderId.isEmpty()){
                            Order currentOrder = getCurrentOrderFromDb();
                            if(currentOrder!=null){
                                currentOrder.setServerId(orderId);
                                currentOrder.setStatus(Order.DRIVER_SEARCH_STATUS);
                                updateOrderInDb(currentOrder);
                            }
                        }
                        BusProvider.send(event);

                    }

                    @Override
                    public void onError(Throwable e) {
                        event = new NetworkResponseEvent<>();
                        event.setId(ApiConstants.GET_ORDER_COST);
                        event.setSucess(false);
                        BusProvider.send(event);
                    }

                    @Override
                    public void onNext(Response<JsonObject> response) {
                        if(response.isSuccessful()){
                            event = new NetworkResponseEvent<>();
                            event.setId(ApiConstants.CREATE_ORDER);
                            Gson gson = new Gson();
                            JsonObject orderJson = response.body().getAsJsonObject("order");
                            orderId = orderJson.get("id").getAsString();

                        }
                    }
                });

    }

    public void cancelOrder(String authToken,String orderId){
        restAdapter.init(authToken,false);
        OrderRestService orderRestService = restAdapter.getRetrofit().create(OrderRestService.class);
        Observable<Response<JsonObject>> createOrder = orderRestService.cancelOrder(Integer.parseInt(orderId));
        createOrder.subscribeOn(Schedulers.newThread()).
                subscribe(new Subscriber<Response<JsonObject>>() {
                    private NetworkResponseEvent<List<Location>> event;

                    @Override
                    public void onCompleted() {
                        if(event!=null){
                            event.setSucess(true);
                        }else {
                            event = new NetworkResponseEvent<>();
                            event.setId(ApiConstants.CANCEL_ORDER);
                            event.setSucess(false);
                        }
                        Order order = getCurrentOrderFromDb();
                        if(order!=null){
                            order.setServerId("");
                            order.setStatus(Order.NOT_SPECIFIED_STATUS);
                            List<Trip> trips = order.getTrips();
                            if(trips!=null&&!trips.isEmpty()){
                                for (Trip trip:trips) {
                                    deleteTrip(trip);
                                }
                            }
                            updateOrderInDb(order);
                        }
                        BusProvider.send(event);


                    }

                    @Override
                    public void onError(Throwable e) {
                        event = new NetworkResponseEvent<>();
                        event.setId(ApiConstants.CANCEL_ORDER);
                        event.setSucess(false);
                        BusProvider.send(event);
                    }

                    @Override
                    public void onNext(Response<JsonObject> responce) {
                        if(responce.isSuccessful()){
                            event = new NetworkResponseEvent<>();
                            event.setId(ApiConstants.CANCEL_ORDER);
                        }
                    }
                });
    }

    public void getOrderCost(final Order order, String authToken){
        restAdapter.init(authToken,false);
        OrderRestService orderRestService = restAdapter.getRetrofit().create(OrderRestService.class);
        int orderTimeId = order.getTimeId();
        Date date = setOrderTime(orderTimeId);
        String  forMatDate = null;
        if(date!=null){
            SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
            isoFormat.setTimeZone(TimeZone.getDefault());
            forMatDate = isoFormat.format(date);
        }
        NumberFormat formatter = new DecimalFormat("##.######");
        Observable<Response<JsonObject>> orderCost = orderRestService.getOrderCost(
                order.getLocationFrom().getLatitude(),
                order.getLocationFrom().getLongitude(),
                order.getLocationFrom().getName(),
                "",
                order.getLocationTo().getLatitude(),
                order.getLocationTo().getLongitude(),
                order.getLocationTo().getName(),
                "",
                date!=null?date.getTime():0,
                order.getOrderOptions().getBagCount(),
                order.getOrderOptions().getCarClass(),
                0,
                order.getOrderOptions().isNoSmokingDriver()?1:0,
                order.getOrderOptions().getFlightNumber(),
                order.getOrderOptions().getComment(),
                forMatDate);
        orderCost.subscribeOn(Schedulers.newThread()).
                subscribe(new Subscriber<Response<JsonObject>>() {
                    private NetworkResponseEvent<List<Location>> event;
                    private List<Trip> trips;
                    @Override
                    public void onCompleted() {
                        if(event!=null){
                            event.setSucess(true);
                        }else {
                            event = new NetworkResponseEvent<>();
                            event.setId(ApiConstants.GET_ORDER_COST);
                            event.setSucess(false);
                        }
                        if(trips!=null&&!trips.isEmpty()){
                            for (Trip trip:trips) {
                                trip.setOrderId(order.getId());
                                updateTrip(trip);
                            }
                        }
                        BusProvider.send(event);
                    }

                    @Override
                    public void onError(Throwable e) {
                        event = new NetworkResponseEvent<>();
                        event.setId(ApiConstants.GET_ORDER_COST);
                        event.setSucess(false);
                        BusProvider.send(event);
                    }

                    @Override
                    public void onNext(Response<JsonObject> response) {
                        if(response.isSuccessful()){
                            event = new NetworkResponseEvent<>();
                            event.setId(ApiConstants.GET_ORDER_COST);
                            Gson gson = new Gson();
                            JsonArray tripsJson = response.body().getAsJsonArray("trips");
                            Type listType = new TypeToken<List<Trip>>(){}.getType();
                            trips =  gson.fromJson(tripsJson, listType);
                            for (int i = 0; i < tripsJson.size(); i++) {
                                JsonElement jsonElement = tripsJson.get(i);
                                JsonObject locationFromJson = jsonElement.getAsJsonObject().getAsJsonObject("from");
                                TripLocation fromlocation = gson.fromJson(locationFromJson,TripLocation.class);
                                JsonObject locationToJson = jsonElement.getAsJsonObject().getAsJsonObject("to");
                                TripLocation tolocation = gson.fromJson(locationToJson,TripLocation.class);
                                trips.get(i).setLocationFrom(Location.toLocation(fromlocation));
                                trips.get(i).setLocationTo(Location.toLocation(tolocation));
                            }
                        }
                    }
                });

    }

    private Date setOrderTime(int timeId){
        Date now = new Date();
        final long minute = 60*1000;
        Date date  = null;
        switch (timeId){
            case Order.START_SHORT_TIME:
                date = new Date(now.getTime()+(10*minute));
                break;
            case Order.START_THIRTY_MINUTES:
                date = new Date(now.getTime()+(30*minute));
                break;
            case Order.START_FORTY_FIVE_MINUTES:
                date = new Date(now.getTime()+(45*minute));
                break;
            case Order.START_HOUR:
                date = new Date(now.getTime()+(60*minute));
                break;
        }
        return date;
    }

}

