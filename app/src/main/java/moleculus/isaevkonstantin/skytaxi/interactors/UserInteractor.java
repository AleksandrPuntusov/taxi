package moleculus.isaevkonstantin.skytaxi.interactors;

import android.content.Context;
import android.net.Uri;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import moleculus.isaevkonstantin.skytaxi.database.dao.UserDao;
import moleculus.isaevkonstantin.skytaxi.database.interfaces.Crud;
import moleculus.isaevkonstantin.skytaxi.eventbus.BusProvider;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.constants.ApiConstants;
import moleculus.isaevkonstantin.skytaxi.eventbus.events.network.NetworkResponseEvent;
import moleculus.isaevkonstantin.skytaxi.models.PromoCode;
import moleculus.isaevkonstantin.skytaxi.models.User;
import moleculus.isaevkonstantin.skytaxi.models.UserStatistics;
import moleculus.isaevkonstantin.skytaxi.rest.interfaces.UserRestService;
import moleculus.isaevkonstantin.skytaxi.rest.models.ResponseResult;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

/**
 * Created by Isaev Konstantin on 12.04.16.
 */
public class UserInteractor extends BaseInteractor {

    public UserInteractor(Context context) {
        super(context);
    }


//    public void addUserToDatabase(final User entity){
//
//        Observable.just(entity).
//                subscribeOn(Schedulers.newThread())
//                .subscribe(new Subscriber<User>() {
//                    @Override
//                    public void onCompleted() {
//
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//
//                    }
//
//                    @Override
//                    public void onNext(User user) {
//                        UserEvent<User> userEvent = new UserEvent<>();
//                        entity.setId(c.add(entity));
//                        userEvent.setData(entity);
//                        BusProvider.send(userEvent);
//                    }
//                });
//    }

    public User getUserFromDb(final long id){
        Crud crud = initDao(USER_DAO);
        List<User> userList = crud.getAll();
        if (userList != null&&!userList.isEmpty()) {
            User user = userList.get(0);
            crud = initDao(PROMOCODE_DAO);
            user.setPromoCode((PromoCode) crud.getById(user.getPromoCodeFk()));
            crud = initDao(USER_STATISTICS_DAO);
            user.setUserStatistics((UserStatistics)crud.getById(user.getUserStatisticsFk()));
            return user;
        }else {
            return null;
        }
//        Observable.just(id).
//                subscribeOn(Schedulers.newThread())
//                .subscribe(new Action1<Long>() {
//                    @Override
//                    public void call(Long id) {
//                        UserEvent<List<User>> userEvent = new UserEvent<>();
//                        userEvent.setData(userDao.getAll());
//                        BusProvider.send(userEvent);
//                    }
//                });
    }

    public void sendPhone(final String phoneNumber){
        restAdapter.init(null,true);
        UserRestService userRestService = restAdapter.getRetrofit().create(UserRestService.class);
        Observable<Response<JsonObject>> sendSmsCode = userRestService.sendPhone(phoneNumber);
        sendSmsCode.subscribeOn(Schedulers.newThread())
                .subscribe(new Subscriber<Response<JsonObject>>() {
                    private NetworkResponseEvent<String> event;
                    @Override
                    public void onCompleted() {
                        if(event!=null){
                            event.setSucess(true);
                        }else {
                            event = new NetworkResponseEvent<>();
                            event.setId(ApiConstants.SEND_PHONE);
                            event.setSucess(false);
                        }
                        if(event!=null){
                            event.setData(phoneNumber);
                        }
                        BusProvider.send(event);

                    }

                    @Override
                    public void onError(Throwable e) {
                        event = new NetworkResponseEvent<>();
                        event.setId(ApiConstants.SEND_PHONE);
                        event.setSucess(false);
                        BusProvider.send(event);

                    }

                    @Override
                    public void onNext(Response<JsonObject> response) {
                        if(response.isSuccessful()){
                            JsonObject jsonObject = response.body().getAsJsonObject();
                            Gson gson = new Gson();
                            ResponseResult responseResult = gson.fromJson(jsonObject,ResponseResult.class);
                            event = new NetworkResponseEvent();
                            event.setId(ApiConstants.SEND_PHONE);
                        }

                    }
                });

    }
    public void sendSmsCode(final String phoneNumber, final String  smsCode){
        restAdapter.init(null,true);
        final UserRestService userRestService = restAdapter.getRetrofit().create(UserRestService.class);
        Observable<Response<JsonObject>> confirmSmsCode = userRestService.sendSmsCode(phoneNumber,smsCode,
                "2","upAy8oAdxeKWB5iwGkVs");
//                "2","Rne0SimjH0JIrTDCMJqe");
        confirmSmsCode.subscribeOn(Schedulers.newThread())
                .subscribe(new Subscriber<Response<JsonObject>>() {
                    private NetworkResponseEvent<ResponseResult> event;
                    private User user;
                    @Override
                    public void onCompleted() {
                        if(event!=null){
                            event.setSucess(true);
                        }else {
                            event = new NetworkResponseEvent<>();
                            event.setId(ApiConstants.CONFIRM_SMS_CODE);
                            event.setSucess(false);
                        }

                        if(user!=null){
                            updateUserInDb(user);
                        }
                        BusProvider.send(event);
                    }

                    @Override
                    public void onError(Throwable e) {
                        event = new NetworkResponseEvent<>();
                        event.setSucess(false);
                        BusProvider.send(event);
                    }

                    @Override
                    public void onNext(Response<JsonObject> response) {
                        if(response.isSuccessful()){
                            event = new NetworkResponseEvent<>();
                            event.setId(ApiConstants.CONFIRM_SMS_CODE);
                            JsonObject jsonObject = response.body().getAsJsonObject();
                            Gson gson = new Gson();
                            ResponseResult responseResult =  gson.fromJson(jsonObject,ResponseResult.class);
                            if(responseResult !=null){
                                event.setData(responseResult);
                                String token = response.body().get("access_token").getAsString();
                                jsonObject = jsonObject.getAsJsonObject("user");
                                user = gson.fromJson(jsonObject,User.class);
                                if (user != null) {
                                    user.setAuthToken(token);
                                }
                            }
                        }

                    }

                });
    }

    public void getUserRest(final String  authToken){
        restAdapter.init(authToken,false);
        UserRestService userRestService = restAdapter.getRetrofit().create(UserRestService.class);
        Observable <Response<JsonObject>> user = userRestService.getUser();
        user.subscribeOn(Schedulers.newThread()).
                subscribe(new Subscriber<Response<JsonObject>>() {
                    private NetworkResponseEvent<User> event;
                    private User user;
                    @Override
                    public void onCompleted() {
                        if(event!=null){
                            event.setSucess(true);
                        }else {
                            event = new NetworkResponseEvent<>();
                            event.setId(ApiConstants.GET_USER);
                            event.setSucess(false);
                        }
                        if(user!=null){
                            updateUserInDb(user);
                        }
                        BusProvider.send(event);
                    }

                    @Override
                    public void onError(Throwable e) {
                        event = new NetworkResponseEvent<>();
                        event.setId(ApiConstants.GET_USER);
                        event.setSucess(false);
                        BusProvider.send(event);
                    }

                    @Override
                    public void onNext(Response<JsonObject> response) {
                        if(response.isSuccessful()){
                            event = new NetworkResponseEvent<>();
                            event.setId(ApiConstants.GET_USER);
                            Gson gson = new Gson();
                            JsonObject jsonObject = response.body().getAsJsonObject("user");
                            user = gson.fromJson(jsonObject,User.class);
                            if (user != null) {
                                event.setData(user);
                                user.setAuthToken(authToken);
                            }
                        }
                    }
                });
    }
    public void updateUserRest(final User user){
        restAdapter.init(user.getAuthToken(),false);
        UserRestService userRestService = restAdapter.getRetrofit().create(UserRestService.class);
        Map<String,String> params = new HashMap<>();
        fillMap(UserRestService.FIRST_NAME,user.getFirstName(),params);
        fillMap(UserRestService.LAST_NAME,user.getLastName(),params);
        fillMap(UserRestService.MIDDLE_NAME,user.getMiddleName(),params);
        fillMap(UserRestService.EMAIL,user.getEmail(),params);
        final String token = user.getAuthToken();
        final Observable <Response<JsonObject>> userObservable =
                userRestService.updateUser(token,params);
        userObservable.subscribeOn(Schedulers.newThread()).
                subscribe(new Subscriber<Response<JsonObject>>() {
                    private NetworkResponseEvent<User> event;
                    private User userSubs;
                    @Override
                    public void onCompleted() {
                        if(event!=null){
                            event.setSucess(true);
                        }else {
                            event = new NetworkResponseEvent<>();
                            event.setId(ApiConstants.UPDATE_USER);
                            event.setSucess(false);
                        }
                        if(userSubs!=null){
                           updateUserInDb(userSubs);
                        }
                        BusProvider.send(event);
                    }

                    @Override
                    public void onError(Throwable e) {
                        event = new NetworkResponseEvent<>();
                        event.setId(ApiConstants.UPDATE_USER);
                        event.setSucess(false);
                        BusProvider.send(event);
                    }

                    @Override
                    public void onNext(Response<JsonObject> response) {
                        if(response.isSuccessful()){
                            event = new NetworkResponseEvent<>();
                            event.setId(ApiConstants.UPDATE_USER);
                            Gson gson = new Gson();
                            JsonObject jsonObject = response.body().getAsJsonObject("user");
                            userSubs = gson.fromJson(jsonObject,User.class);
                            if (userSubs != null) {
                                userSubs.setAuthToken(token);
                                event.setData(userSubs);

                            }
                        }
                    }
                });
    }

    public void logoutUserRest(String authToken){
        restAdapter.init(authToken,false);
        UserRestService userRestService = restAdapter.getRetrofit().create(UserRestService.class);
        Observable <Response<JsonObject>> logout = userRestService.logout(authToken);
        logout.subscribeOn(Schedulers.newThread()).
                subscribe(new Subscriber<Response<JsonObject>>() {
                    private NetworkResponseEvent event;
                    @Override
                    public void onCompleted() {
                        if(event!=null){
                            event.setSucess(true);
                            Crud crud = initDao(USER_DAO);
                            crud.deleteAll();
                        }else {
                            event = new NetworkResponseEvent<>();
                            event.setId(ApiConstants.GET_USER);
                            event.setSucess(false);
                        }

                        BusProvider.send(event);
                    }

                    @Override
                    public void onError(Throwable e) {
                        event = new NetworkResponseEvent<>();
                        event.setId(ApiConstants.LOGOUT);
                        event.setSucess(false);
                        BusProvider.send(event);
                    }

                    @Override
                    public void onNext(Response<JsonObject> response) {
                        if(response.isSuccessful()){
                            event = new NetworkResponseEvent<>();
                            event.setId(ApiConstants.LOGOUT);
                        }
                    }
                });
    }

    public void updateProfilePhoto(String authToken, Uri uri){
        restAdapter.init(authToken,false);
        File file = new File(uri.getPath());
        if(file!=null){
            RequestBody fbody = RequestBody.create(MediaType.parse("image/*"), file);
            UserRestService userRestService = restAdapter.getRetrofit().create(UserRestService.class);
            final Observable <Response<JsonObject>> photoObservable =
                    userRestService.updatePhoto(fbody);
            photoObservable.subscribeOn(Schedulers.newThread()).
                    subscribe(new Subscriber<Response<JsonObject>>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            int i = 0;
                            i++;
                        }

                        @Override
                        public void onNext(Response<JsonObject> response) {
                            if(response.isSuccessful()){
                                int i = 0;
                                i++;
                            }
                        }
                    });
        }
    }
    private void fillMap(String title,String value,Map<String,String> params){
        if(value!=null&&!value.isEmpty()){
            params.put(title,value);
        }else {
            params.put(title,"");

        }
    }

    private void updateUserInDb(User user){
        UserDao userDao = (UserDao) initDao(USER_DAO);
        Crud crud = null;
        User userDb =  userDao.getByPhoneNumber(user.getPhoneNumber());
        if(userDb!=null){
            crud = initDao(PROMOCODE_DAO);
            user.setUserStatisticsFk(userDb.getUserStatisticsFk());
            user.setPromoCodeFk(userDb.getPromoCodeFk());
            user.getPromoCode().setId(userDb.getPromoCodeFk());
            boolean updatePromo = crud.update(user.getPromoCode());
            if(!updatePromo){
                crud.deleteAll();
                user.setPromoCodeFk(crud.add(user.getPromoCode()));
            }
            crud = initDao(USER_STATISTICS_DAO);
            user.getUserStatistics().setId(userDb.getUserStatisticsFk());
            boolean updateStatistics = crud.update(user.getUserStatistics());
            if(!updateStatistics){
                crud.deleteAll();
                user.setUserStatisticsFk(crud.add(user.getUserStatistics()));
            }
            userDao.updateByServerId(user);
        }else {
            crud = initDao(PROMOCODE_DAO);
            crud.deleteAll();
            user.setPromoCodeFk(crud.add(user.getPromoCode()));
            crud = initDao(USER_STATISTICS_DAO);
            crud.deleteAll();
            user.setUserStatisticsFk(crud.add(user.getUserStatistics()));
            userDao.deleteAll();
            user.setId(userDao.add(user));
        }

    }




}
